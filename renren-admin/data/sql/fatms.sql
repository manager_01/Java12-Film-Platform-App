/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80031
 Source Host           : localhost:3306
 Source Schema         : fatms

 Target Server Type    : MySQL
 Target Server Version : 80031
 File Encoding         : 65001

 Date: 22/11/2022 15:39:35
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for newcomments
-- ----------------------------
DROP TABLE IF EXISTS `newcomments`;
CREATE TABLE `newcomments`  (
  `cid` int NOT NULL,
  `fid` int NOT NULL,
  `uid` int NOT NULL,
  `content` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `createtime` datetime NOT NULL,
  `favoritecomment` int NULL DEFAULT NULL,
  `numberlike` int NULL DEFAULT NULL,
  PRIMARY KEY (`cid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of newcomments
-- ----------------------------
INSERT INTO `newcomments` VALUES (1, 1, 1, '演的真好', '2022-02-10 00:00:00', 1, 66);
INSERT INTO `newcomments` VALUES (2, 2, 2, '1', '2022-02-11 00:00:00', 1, 7);
INSERT INTO `newcomments` VALUES (3, 3, 3, 'zdfasda', '2022-03-12 00:00:00', 1, 1);
INSERT INTO `newcomments` VALUES (4, 4, 2, 'adasdas', '2022-03-12 00:00:00', 1, 13);
INSERT INTO `newcomments` VALUES (5, 5, 3, 'asdadsad', '2022-03-12 00:00:00', 1, 7);
INSERT INTO `newcomments` VALUES (6, 1, 3, 'dasdasd', '2022-03-12 00:00:00', 1, 37);
INSERT INTO `newcomments` VALUES (7, 1, 2, 'sadasdsad', '2022-04-11 00:00:00', 1, 31);

-- ----------------------------
-- Table structure for newfilm
-- ----------------------------
DROP TABLE IF EXISTS `newfilm`;
CREATE TABLE `newfilm`  (
  `filmid` int NOT NULL AUTO_INCREMENT,
  `filmname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `filmupdate` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `filmstar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `filmpictures` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `filmscore` int NULL DEFAULT 4,
  PRIMARY KEY (`filmid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of newfilm
-- ----------------------------
INSERT INTO `newfilm` VALUES (1, '霸王别姬', '1', '1', '1', 3);
INSERT INTO `newfilm` VALUES (2, '拯救大兵瑞恩', '2', '2', '2', 2);
INSERT INTO `newfilm` VALUES (3, '头号玩家', '3', '3', '3', 1);
INSERT INTO `newfilm` VALUES (4, '猪头怪', '4', '4', '4', 3);
INSERT INTO `newfilm` VALUES (5, '皮皮虾', '5', '5', '5', 3);
INSERT INTO `newfilm` VALUES (6, '你在干什么', '6', '6', '6', 2);
INSERT INTO `newfilm` VALUES (7, '7', '7', '7', '7', 2);
INSERT INTO `newfilm` VALUES (8, '8', '8', '8', '8', 2);
INSERT INTO `newfilm` VALUES (9, '9', '9', '9', '9', 4);

-- ----------------------------
-- Table structure for newuser
-- ----------------------------
DROP TABLE IF EXISTS `newuser`;
CREATE TABLE `newuser`  (
  `uid` int NOT NULL,
  `uname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`uid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of newuser
-- ----------------------------
INSERT INTO `newuser` VALUES (1, 'zhangsan');
INSERT INTO `newuser` VALUES (2, 'lisi');
INSERT INTO `newuser` VALUES (3, 'wangwu');

-- ----------------------------
-- Table structure for qrtz_blob_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_blob_triggers`;
CREATE TABLE `qrtz_blob_triggers`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `TRIGGER_NAME` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `TRIGGER_GROUP` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `BLOB_DATA` blob NULL,
  PRIMARY KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) USING BTREE,
  INDEX `SCHED_NAME`(`SCHED_NAME` ASC, `TRIGGER_NAME` ASC, `TRIGGER_GROUP` ASC) USING BTREE,
  CONSTRAINT `qrtz_blob_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of qrtz_blob_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_calendars
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_calendars`;
CREATE TABLE `qrtz_calendars`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `CALENDAR_NAME` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `CALENDAR` blob NOT NULL,
  PRIMARY KEY (`SCHED_NAME`, `CALENDAR_NAME`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of qrtz_calendars
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_cron_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_cron_triggers`;
CREATE TABLE `qrtz_cron_triggers`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `TRIGGER_NAME` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `TRIGGER_GROUP` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `CRON_EXPRESSION` varchar(120) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `TIME_ZONE_ID` varchar(80) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) USING BTREE,
  CONSTRAINT `qrtz_cron_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of qrtz_cron_triggers
-- ----------------------------
INSERT INTO `qrtz_cron_triggers` VALUES ('RenrenScheduler', 'TASK_1067246875800000076', 'DEFAULT', '0 0/30 * * * ?', 'Asia/Shanghai');

-- ----------------------------
-- Table structure for qrtz_fired_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_fired_triggers`;
CREATE TABLE `qrtz_fired_triggers`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `ENTRY_ID` varchar(95) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `TRIGGER_NAME` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `TRIGGER_GROUP` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `INSTANCE_NAME` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `FIRED_TIME` bigint NOT NULL,
  `SCHED_TIME` bigint NOT NULL,
  `PRIORITY` int NOT NULL,
  `STATE` varchar(16) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `JOB_NAME` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `JOB_GROUP` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `IS_NONCONCURRENT` varchar(1) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `REQUESTS_RECOVERY` varchar(1) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`, `ENTRY_ID`) USING BTREE,
  INDEX `IDX_QRTZ_FT_TRIG_INST_NAME`(`SCHED_NAME` ASC, `INSTANCE_NAME` ASC) USING BTREE,
  INDEX `IDX_QRTZ_FT_INST_JOB_REQ_RCVRY`(`SCHED_NAME` ASC, `INSTANCE_NAME` ASC, `REQUESTS_RECOVERY` ASC) USING BTREE,
  INDEX `IDX_QRTZ_FT_J_G`(`SCHED_NAME` ASC, `JOB_NAME` ASC, `JOB_GROUP` ASC) USING BTREE,
  INDEX `IDX_QRTZ_FT_JG`(`SCHED_NAME` ASC, `JOB_GROUP` ASC) USING BTREE,
  INDEX `IDX_QRTZ_FT_T_G`(`SCHED_NAME` ASC, `TRIGGER_NAME` ASC, `TRIGGER_GROUP` ASC) USING BTREE,
  INDEX `IDX_QRTZ_FT_TG`(`SCHED_NAME` ASC, `TRIGGER_GROUP` ASC) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of qrtz_fired_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_job_details
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_job_details`;
CREATE TABLE `qrtz_job_details`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `JOB_NAME` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `JOB_GROUP` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `DESCRIPTION` varchar(250) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `JOB_CLASS_NAME` varchar(250) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `IS_DURABLE` varchar(1) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `IS_NONCONCURRENT` varchar(1) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `IS_UPDATE_DATA` varchar(1) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `REQUESTS_RECOVERY` varchar(1) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `JOB_DATA` blob NULL,
  PRIMARY KEY (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`) USING BTREE,
  INDEX `IDX_QRTZ_J_REQ_RECOVERY`(`SCHED_NAME` ASC, `REQUESTS_RECOVERY` ASC) USING BTREE,
  INDEX `IDX_QRTZ_J_GRP`(`SCHED_NAME` ASC, `JOB_GROUP` ASC) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of qrtz_job_details
-- ----------------------------
INSERT INTO `qrtz_job_details` VALUES ('RenrenScheduler', 'TASK_1067246875800000076', 'DEFAULT', NULL, 'io.renren.modules.job.utils.ScheduleJob', '0', '0', '0', '0', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000174000D4A4F425F504152414D5F4B45597372002E696F2E72656E72656E2E6D6F64756C65732E6A6F622E656E746974792E5363686564756C654A6F62456E7469747900000000000000010200074C00086265616E4E616D657400124C6A6176612F6C616E672F537472696E673B4C000E63726F6E45787072657373696F6E71007E00094C0006706172616D7371007E00094C000672656D61726B71007E00094C00067374617475737400134C6A6176612F6C616E672F496E74656765723B4C000A757064617465446174657400104C6A6176612F7574696C2F446174653B4C0007757064617465727400104C6A6176612F6C616E672F4C6F6E673B78720022696F2E72656E72656E2E636F6D6D6F6E2E656E746974792E42617365456E74697479FB83923222FF87B90200034C000A6372656174654461746571007E000B4C000763726561746F7271007E000C4C0002696471007E000C78707372000E6A6176612E7574696C2E44617465686A81014B597419030000787077080000018476466A08787372000E6A6176612E6C616E672E4C6F6E673B8BE490CC8F23DF0200014A000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B02000078700ECF9F6107B456017371007E00110ECF9F6107B4564C740008746573745461736B74000E3020302F3330202A202A202A203F74000672656E72656E740025E69C89E58F82E6B58BE8AF95EFBC8CE5A49AE4B8AAE58F82E695B0E4BDBFE794A86A736F6E737200116A6176612E6C616E672E496E746567657212E2A0A4F781873802000149000576616C75657871007E0012000000007371007E000F77080000018476466A08787371007E00110ECF9F6107B456017800);

-- ----------------------------
-- Table structure for qrtz_locks
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_locks`;
CREATE TABLE `qrtz_locks`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `LOCK_NAME` varchar(40) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  PRIMARY KEY (`SCHED_NAME`, `LOCK_NAME`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of qrtz_locks
-- ----------------------------
INSERT INTO `qrtz_locks` VALUES ('RenrenScheduler', 'STATE_ACCESS');
INSERT INTO `qrtz_locks` VALUES ('RenrenScheduler', 'TRIGGER_ACCESS');

-- ----------------------------
-- Table structure for qrtz_paused_trigger_grps
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_paused_trigger_grps`;
CREATE TABLE `qrtz_paused_trigger_grps`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `TRIGGER_GROUP` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  PRIMARY KEY (`SCHED_NAME`, `TRIGGER_GROUP`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of qrtz_paused_trigger_grps
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_scheduler_state
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_scheduler_state`;
CREATE TABLE `qrtz_scheduler_state`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `INSTANCE_NAME` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `LAST_CHECKIN_TIME` bigint NOT NULL,
  `CHECKIN_INTERVAL` bigint NOT NULL,
  PRIMARY KEY (`SCHED_NAME`, `INSTANCE_NAME`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of qrtz_scheduler_state
-- ----------------------------
INSERT INTO `qrtz_scheduler_state` VALUES ('RenrenScheduler', 'Dnc-20191119PNT1669095877663', 1669102772458, 15000);

-- ----------------------------
-- Table structure for qrtz_simple_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simple_triggers`;
CREATE TABLE `qrtz_simple_triggers`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `TRIGGER_NAME` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `TRIGGER_GROUP` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `REPEAT_COUNT` bigint NOT NULL,
  `REPEAT_INTERVAL` bigint NOT NULL,
  `TIMES_TRIGGERED` bigint NOT NULL,
  PRIMARY KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) USING BTREE,
  CONSTRAINT `qrtz_simple_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of qrtz_simple_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_simprop_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simprop_triggers`;
CREATE TABLE `qrtz_simprop_triggers`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `TRIGGER_NAME` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `TRIGGER_GROUP` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `STR_PROP_1` varchar(512) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `STR_PROP_2` varchar(512) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `STR_PROP_3` varchar(512) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `INT_PROP_1` int NULL DEFAULT NULL,
  `INT_PROP_2` int NULL DEFAULT NULL,
  `LONG_PROP_1` bigint NULL DEFAULT NULL,
  `LONG_PROP_2` bigint NULL DEFAULT NULL,
  `DEC_PROP_1` decimal(13, 4) NULL DEFAULT NULL,
  `DEC_PROP_2` decimal(13, 4) NULL DEFAULT NULL,
  `BOOL_PROP_1` varchar(1) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `BOOL_PROP_2` varchar(1) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) USING BTREE,
  CONSTRAINT `qrtz_simprop_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of qrtz_simprop_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_triggers`;
CREATE TABLE `qrtz_triggers`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `TRIGGER_NAME` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `TRIGGER_GROUP` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `JOB_NAME` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `JOB_GROUP` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `DESCRIPTION` varchar(250) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `NEXT_FIRE_TIME` bigint NULL DEFAULT NULL,
  `PREV_FIRE_TIME` bigint NULL DEFAULT NULL,
  `PRIORITY` int NULL DEFAULT NULL,
  `TRIGGER_STATE` varchar(16) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `TRIGGER_TYPE` varchar(8) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `START_TIME` bigint NOT NULL,
  `END_TIME` bigint NULL DEFAULT NULL,
  `CALENDAR_NAME` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `MISFIRE_INSTR` smallint NULL DEFAULT NULL,
  `JOB_DATA` blob NULL,
  PRIMARY KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) USING BTREE,
  INDEX `IDX_QRTZ_T_J`(`SCHED_NAME` ASC, `JOB_NAME` ASC, `JOB_GROUP` ASC) USING BTREE,
  INDEX `IDX_QRTZ_T_JG`(`SCHED_NAME` ASC, `JOB_GROUP` ASC) USING BTREE,
  INDEX `IDX_QRTZ_T_C`(`SCHED_NAME` ASC, `CALENDAR_NAME` ASC) USING BTREE,
  INDEX `IDX_QRTZ_T_G`(`SCHED_NAME` ASC, `TRIGGER_GROUP` ASC) USING BTREE,
  INDEX `IDX_QRTZ_T_STATE`(`SCHED_NAME` ASC, `TRIGGER_STATE` ASC) USING BTREE,
  INDEX `IDX_QRTZ_T_N_STATE`(`SCHED_NAME` ASC, `TRIGGER_NAME` ASC, `TRIGGER_GROUP` ASC, `TRIGGER_STATE` ASC) USING BTREE,
  INDEX `IDX_QRTZ_T_N_G_STATE`(`SCHED_NAME` ASC, `TRIGGER_GROUP` ASC, `TRIGGER_STATE` ASC) USING BTREE,
  INDEX `IDX_QRTZ_T_NEXT_FIRE_TIME`(`SCHED_NAME` ASC, `NEXT_FIRE_TIME` ASC) USING BTREE,
  INDEX `IDX_QRTZ_T_NFT_ST`(`SCHED_NAME` ASC, `TRIGGER_STATE` ASC, `NEXT_FIRE_TIME` ASC) USING BTREE,
  INDEX `IDX_QRTZ_T_NFT_MISFIRE`(`SCHED_NAME` ASC, `MISFIRE_INSTR` ASC, `NEXT_FIRE_TIME` ASC) USING BTREE,
  INDEX `IDX_QRTZ_T_NFT_ST_MISFIRE`(`SCHED_NAME` ASC, `MISFIRE_INSTR` ASC, `NEXT_FIRE_TIME` ASC, `TRIGGER_STATE` ASC) USING BTREE,
  INDEX `IDX_QRTZ_T_NFT_ST_MISFIRE_GRP`(`SCHED_NAME` ASC, `MISFIRE_INSTR` ASC, `NEXT_FIRE_TIME` ASC, `TRIGGER_GROUP` ASC, `TRIGGER_STATE` ASC) USING BTREE,
  CONSTRAINT `qrtz_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`) REFERENCES `qrtz_job_details` (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of qrtz_triggers
-- ----------------------------
INSERT INTO `qrtz_triggers` VALUES ('RenrenScheduler', 'TASK_1067246875800000076', 'DEFAULT', 'TASK_1067246875800000076', 'DEFAULT', NULL, 1668432600000, -1, 5, 'PAUSED', 'CRON', 1668432146000, 0, NULL, 2, 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000174000D4A4F425F504152414D5F4B45597372002E696F2E72656E72656E2E6D6F64756C65732E6A6F622E656E746974792E5363686564756C654A6F62456E7469747900000000000000010200074C00086265616E4E616D657400124C6A6176612F6C616E672F537472696E673B4C000E63726F6E45787072657373696F6E71007E00094C0006706172616D7371007E00094C000672656D61726B71007E00094C00067374617475737400134C6A6176612F6C616E672F496E74656765723B4C000A757064617465446174657400104C6A6176612F7574696C2F446174653B4C0007757064617465727400104C6A6176612F6C616E672F4C6F6E673B78720022696F2E72656E72656E2E636F6D6D6F6E2E656E746974792E42617365456E74697479FB83923222FF87B90200034C000A6372656174654461746571007E000B4C000763726561746F7271007E000C4C0002696471007E000C78707372000E6A6176612E7574696C2E44617465686A81014B597419030000787077080000018476466A08787372000E6A6176612E6C616E672E4C6F6E673B8BE490CC8F23DF0200014A000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B02000078700ECF9F6107B456017371007E00110ECF9F6107B4564C740008746573745461736B74000E3020302F3330202A202A202A203F74000672656E72656E740025E69C89E58F82E6B58BE8AF95EFBC8CE5A49AE4B8AAE58F82E695B0E4BDBFE794A86A736F6E737200116A6176612E6C616E672E496E746567657212E2A0A4F781873802000149000576616C75657871007E0012000000007371007E000F77080000018476466A08787371007E00110ECF9F6107B456017800);

-- ----------------------------
-- Table structure for sanswer
-- ----------------------------
DROP TABLE IF EXISTS `sanswer`;
CREATE TABLE `sanswer`  (
  `ansid` int NOT NULL AUTO_INCREMENT,
  `qid` int NULL DEFAULT NULL,
  `uid` int NULL DEFAULT NULL,
  `content` varchar(128) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `dznum` int NOT NULL DEFAULT 0,
  `time` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`ansid`) USING BTREE,
  INDEX `FK_Reference_10`(`qid` ASC) USING BTREE,
  INDEX `FK_Reference_11`(`uid` ASC) USING BTREE,
  CONSTRAINT `FK_Reference_10` FOREIGN KEY (`qid`) REFERENCES `squestion` (`qid`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FK_Reference_11` FOREIGN KEY (`uid`) REFERENCES `suser` (`uid`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sanswer
-- ----------------------------
INSERT INTO `sanswer` VALUES (1, 7, 4, '这个问题要对outline属性进行更改！', 0, '2018-08-09 01:02:45');
INSERT INTO `sanswer` VALUES (2, 7, 6, '将outline的属性设置为none即可，希望采纳', 0, '2018-08-09 01:18:20');
INSERT INTO `sanswer` VALUES (3, 9, 5, '这个问题不好解决', 0, '2018-08-09 01:26:21');
INSERT INTO `sanswer` VALUES (4, 8, 5, '啊啊啊啊啊！！', 0, '2018-08-09 01:26:32');
INSERT INTO `sanswer` VALUES (5, 7, 5, 'ok，楼上正解，感谢！！', 0, '2018-08-09 01:29:53');
INSERT INTO `sanswer` VALUES (6, 7, 6, '嗯 要是还有什么问题可以再问', 0, '2018-08-09 01:35:37');
INSERT INTO `sanswer` VALUES (10, 11, 5, '回复一', 0, '2018-08-09 02:41:06');
INSERT INTO `sanswer` VALUES (11, 11, 6, '回复2', 0, '2018-08-09 02:41:34');

-- ----------------------------
-- Table structure for sarticle
-- ----------------------------
DROP TABLE IF EXISTS `sarticle`;
CREATE TABLE `sarticle`  (
  `arid` int NOT NULL AUTO_INCREMENT,
  `uid` int NULL DEFAULT NULL,
  `title` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `url` varchar(128) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `type` int NOT NULL,
  `ptime` datetime NULL DEFAULT NULL,
  `readnum` int NULL DEFAULT 0,
  `dznum` int NULL DEFAULT 0,
  `plnum` int NULL DEFAULT 0,
  `state` int NULL DEFAULT 0,
  `reason` varchar(32) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `deltag` int NOT NULL DEFAULT 0,
  PRIMARY KEY (`arid`) USING BTREE,
  INDEX `FK_Reference_2`(`uid` ASC) USING BTREE,
  CONSTRAINT `FK_Reference_2` FOREIGN KEY (`uid`) REFERENCES `suser` (`uid`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 511 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sarticle
-- ----------------------------
INSERT INTO `sarticle` VALUES (5, 5, '从零学习人工智能', 'http://localhost:8080/fileResources/htmlFiles/1533745942829.html', 1, '2018-08-09 00:32:23', 24, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (6, 4, '写一个中文聊天机器人', 'http://localhost:8080/fileResources/htmlFiles/1533747729412.html', 1, '2018-08-09 01:02:09', 4, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (7, 6, '深入浅出JS 异步处理', 'http://localhost:8080/fileResources/htmlFiles/1533749058375.html', 4, '2018-08-09 01:24:18', 4, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (8, 5, '云计算test', 'http://localhost:8080/fileResources/htmlFiles/1533745942829.html', 1, '2018-08-09 00:32:23', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (9, 4, '云计算test', 'http://localhost:8080/fileResources/htmlFiles/1533747729412.html', 1, '2018-08-09 01:02:09', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (10, 6, '云计算test', 'http://localhost:8080/fileResources/htmlFiles/1533749058375.html', 1, '2018-08-09 01:24:18', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (11, 5, '云计算test', 'http://localhost:8080/fileResources/htmlFiles/1533745942829.html', 1, '2018-08-09 00:32:23', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (12, 4, '云计算test', 'http://localhost:8080/fileResources/htmlFiles/1533747729412.html', 1, '2018-08-09 01:02:09', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (13, 6, '云计算test', 'http://localhost:8080/fileResources/htmlFiles/1533749058375.html', 1, '2018-08-09 01:24:18', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (14, 5, '云计算test', 'http://localhost:8080/fileResources/htmlFiles/1533745942829.html', 1, '2018-08-09 00:32:23', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (15, 4, '云计算test', 'http://localhost:8080/fileResources/htmlFiles/1533747729412.html', 1, '2018-08-09 01:02:09', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (16, 6, '云计算test', 'http://localhost:8080/fileResources/htmlFiles/1533749058375.html', 1, '2018-08-09 01:24:18', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (18, 5, '云计算test', 'http://localhost:8080/fileResources/htmlFiles/1533745942829.html', 1, '2018-08-09 00:32:23', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (19, 4, '云计算test', 'http://localhost:8080/fileResources/htmlFiles/1533747729412.html', 1, '2018-08-09 01:02:09', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (20, 6, '云计算test', 'http://localhost:8080/fileResources/htmlFiles/1533749058375.html', 1, '2018-08-09 01:24:18', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (21, 5, '云计算test', 'http://localhost:8080/fileResources/htmlFiles/1533745942829.html', 1, '2018-08-09 00:32:23', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (22, 4, '云计算test', 'http://localhost:8080/fileResources/htmlFiles/1533747729412.html', 1, '2018-08-09 01:02:09', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (23, 6, '云计算test', 'http://localhost:8080/fileResources/htmlFiles/1533749058375.html', 1, '2018-08-09 01:24:18', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (24, 5, '云计算test', 'http://localhost:8080/fileResources/htmlFiles/1533745942829.html', 1, '2018-08-09 00:32:23', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (25, 4, '云计算test', 'http://localhost:8080/fileResources/htmlFiles/1533747729412.html', 1, '2018-08-09 01:02:09', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (26, 6, '云计算test', 'http://localhost:8080/fileResources/htmlFiles/1533749058375.html', 1, '2018-08-09 01:24:18', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (27, 5, '云计算test', 'http://localhost:8080/fileResources/htmlFiles/1533745942829.html', 1, '2018-08-09 00:32:23', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (28, 4, '云计算test', 'http://localhost:8080/fileResources/htmlFiles/1533747729412.html', 1, '2018-08-09 01:02:09', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (29, 6, '云计算test', 'http://localhost:8080/fileResources/htmlFiles/1533749058375.html', 1, '2018-08-09 01:24:18', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (33, 5, '云计算test', 'http://localhost:8080/fileResources/htmlFiles/1533745942829.html', 1, '2018-08-09 00:32:23', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (34, 4, '云计算test', 'http://localhost:8080/fileResources/htmlFiles/1533747729412.html', 1, '2018-08-09 01:02:09', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (35, 6, '区块链test', 'http://localhost:8080/fileResources/htmlFiles/1533749058375.html', 2, '2018-08-09 01:24:18', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (36, 5, '区块链test', 'http://localhost:8080/fileResources/htmlFiles/1533745942829.html', 2, '2018-08-09 00:32:23', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (37, 4, '区块链test', 'http://localhost:8080/fileResources/htmlFiles/1533747729412.html', 2, '2018-08-09 01:02:09', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (38, 6, '区块链test', 'http://localhost:8080/fileResources/htmlFiles/1533749058375.html', 2, '2018-08-09 01:24:18', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (39, 5, '区块链test', 'http://localhost:8080/fileResources/htmlFiles/1533745942829.html', 2, '2018-08-09 00:32:23', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (40, 4, '区块链test', 'http://localhost:8080/fileResources/htmlFiles/1533747729412.html', 2, '2018-08-09 01:02:09', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (41, 6, '区块链test', 'http://localhost:8080/fileResources/htmlFiles/1533749058375.html', 2, '2018-08-09 01:24:18', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (42, 5, '区块链test', 'http://localhost:8080/fileResources/htmlFiles/1533745942829.html', 2, '2018-08-09 00:32:23', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (43, 4, '区块链test', 'http://localhost:8080/fileResources/htmlFiles/1533747729412.html', 2, '2018-08-09 01:02:09', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (44, 6, '区块链test', 'http://localhost:8080/fileResources/htmlFiles/1533749058375.html', 2, '2018-08-09 01:24:18', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (45, 5, '区块链test', 'http://localhost:8080/fileResources/htmlFiles/1533745942829.html', 2, '2018-08-09 00:32:23', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (46, 4, '区块链test', 'http://localhost:8080/fileResources/htmlFiles/1533747729412.html', 2, '2018-08-09 01:02:09', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (47, 6, '区块链test', 'http://localhost:8080/fileResources/htmlFiles/1533749058375.html', 2, '2018-08-09 01:24:18', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (48, 5, '区块链test', 'http://localhost:8080/fileResources/htmlFiles/1533745942829.html', 2, '2018-08-09 00:32:23', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (49, 4, '区块链test', 'http://localhost:8080/fileResources/htmlFiles/1533747729412.html', 2, '2018-08-09 01:02:09', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (50, 6, '区块链test', 'http://localhost:8080/fileResources/htmlFiles/1533749058375.html', 2, '2018-08-09 01:24:18', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (51, 5, '区块链test', 'http://localhost:8080/fileResources/htmlFiles/1533745942829.html', 2, '2018-08-09 00:32:23', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (52, 4, '区块链test', 'http://localhost:8080/fileResources/htmlFiles/1533747729412.html', 2, '2018-08-09 01:02:09', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (53, 6, '区块链test', 'http://localhost:8080/fileResources/htmlFiles/1533749058375.html', 2, '2018-08-09 01:24:18', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (54, 5, '区块链test', 'http://localhost:8080/fileResources/htmlFiles/1533745942829.html', 2, '2018-08-09 00:32:23', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (55, 4, '区块链test', 'http://localhost:8080/fileResources/htmlFiles/1533747729412.html', 2, '2018-08-09 01:02:09', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (56, 6, '区块链test', 'http://localhost:8080/fileResources/htmlFiles/1533749058375.html', 2, '2018-08-09 01:24:18', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (64, 5, '区块链test', 'http://localhost:8080/fileResources/htmlFiles/1533745942829.html', 2, '2018-08-09 00:32:23', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (65, 4, '区块链test', 'http://localhost:8080/fileResources/htmlFiles/1533747729412.html', 2, '2018-08-09 01:02:09', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (66, 6, '区块链test', 'http://localhost:8080/fileResources/htmlFiles/1533749058375.html', 2, '2018-08-09 01:24:18', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (67, 5, '区块链test', 'http://localhost:8080/fileResources/htmlFiles/1533745942829.html', 2, '2018-08-09 00:32:23', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (68, 4, '区块链test', 'http://localhost:8080/fileResources/htmlFiles/1533747729412.html', 2, '2018-08-09 01:02:09', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (69, 6, '区块链test', 'http://localhost:8080/fileResources/htmlFiles/1533749058375.html', 2, '2018-08-09 01:24:18', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (70, 5, '区块链test', 'http://localhost:8080/fileResources/htmlFiles/1533745942829.html', 2, '2018-08-09 00:32:23', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (71, 4, '区块链test', 'http://localhost:8080/fileResources/htmlFiles/1533747729412.html', 2, '2018-08-09 01:02:09', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (72, 6, '游戏test', 'http://localhost:8080/fileResources/htmlFiles/1533749058375.html', 3, '2018-08-09 01:24:18', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (73, 5, '游戏test', 'http://localhost:8080/fileResources/htmlFiles/1533745942829.html', 3, '2018-08-09 00:32:23', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (74, 4, '游戏test', 'http://localhost:8080/fileResources/htmlFiles/1533747729412.html', 3, '2018-08-09 01:02:09', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (75, 6, '游戏test', 'http://localhost:8080/fileResources/htmlFiles/1533749058375.html', 3, '2018-08-09 01:24:18', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (76, 5, '游戏test', 'http://localhost:8080/fileResources/htmlFiles/1533745942829.html', 3, '2018-08-09 00:32:23', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (77, 4, '游戏test', 'http://localhost:8080/fileResources/htmlFiles/1533747729412.html', 3, '2018-08-09 01:02:09', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (78, 6, '游戏test', 'http://localhost:8080/fileResources/htmlFiles/1533749058375.html', 3, '2018-08-09 01:24:18', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (79, 5, '游戏test', 'http://localhost:8080/fileResources/htmlFiles/1533745942829.html', 3, '2018-08-09 00:32:23', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (80, 4, '游戏test', 'http://localhost:8080/fileResources/htmlFiles/1533747729412.html', 3, '2018-08-09 01:02:09', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (81, 6, '游戏test', 'http://localhost:8080/fileResources/htmlFiles/1533749058375.html', 3, '2018-08-09 01:24:18', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (82, 5, '游戏test', 'http://localhost:8080/fileResources/htmlFiles/1533745942829.html', 3, '2018-08-09 00:32:23', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (83, 4, '游戏test', 'http://localhost:8080/fileResources/htmlFiles/1533747729412.html', 3, '2018-08-09 01:02:09', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (84, 6, '游戏test', 'http://localhost:8080/fileResources/htmlFiles/1533749058375.html', 3, '2018-08-09 01:24:18', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (85, 5, '游戏test', 'http://localhost:8080/fileResources/htmlFiles/1533745942829.html', 3, '2018-08-09 00:32:23', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (86, 4, '游戏test', 'http://localhost:8080/fileResources/htmlFiles/1533747729412.html', 3, '2018-08-09 01:02:09', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (87, 6, '游戏test', 'http://localhost:8080/fileResources/htmlFiles/1533749058375.html', 3, '2018-08-09 01:24:18', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (88, 5, '游戏test', 'http://localhost:8080/fileResources/htmlFiles/1533745942829.html', 3, '2018-08-09 00:32:23', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (89, 4, '游戏test', 'http://localhost:8080/fileResources/htmlFiles/1533747729412.html', 3, '2018-08-09 01:02:09', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (90, 6, '游戏test', 'http://localhost:8080/fileResources/htmlFiles/1533749058375.html', 3, '2018-08-09 01:24:18', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (91, 5, '游戏test', 'http://localhost:8080/fileResources/htmlFiles/1533745942829.html', 3, '2018-08-09 00:32:23', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (92, 4, '游戏test', 'http://localhost:8080/fileResources/htmlFiles/1533747729412.html', 3, '2018-08-09 01:02:09', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (93, 6, '游戏test', 'http://localhost:8080/fileResources/htmlFiles/1533749058375.html', 3, '2018-08-09 01:24:18', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (94, 5, '游戏test', 'http://localhost:8080/fileResources/htmlFiles/1533745942829.html', 3, '2018-08-09 00:32:23', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (95, 4, '游戏test', 'http://localhost:8080/fileResources/htmlFiles/1533747729412.html', 3, '2018-08-09 01:02:09', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (96, 6, '游戏test', 'http://localhost:8080/fileResources/htmlFiles/1533749058375.html', 3, '2018-08-09 01:24:18', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (97, 5, '游戏test', 'http://localhost:8080/fileResources/htmlFiles/1533745942829.html', 3, '2018-08-09 00:32:23', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (98, 4, '游戏test', 'http://localhost:8080/fileResources/htmlFiles/1533747729412.html', 3, '2018-08-09 01:02:09', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (99, 6, '游戏test', 'http://localhost:8080/fileResources/htmlFiles/1533749058375.html', 3, '2018-08-09 01:24:18', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (100, 5, '游戏test', 'http://localhost:8080/fileResources/htmlFiles/1533745942829.html', 3, '2018-08-09 00:32:23', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (101, 4, '游戏test', 'http://localhost:8080/fileResources/htmlFiles/1533747729412.html', 3, '2018-08-09 01:02:09', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (102, 6, '游戏test', 'http://localhost:8080/fileResources/htmlFiles/1533749058375.html', 3, '2018-08-09 01:24:18', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (103, 5, '游戏test', 'http://localhost:8080/fileResources/htmlFiles/1533745942829.html', 3, '2018-08-09 00:32:23', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (104, 4, '游戏test', 'http://localhost:8080/fileResources/htmlFiles/1533747729412.html', 3, '2018-08-09 01:02:09', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (105, 6, '游戏test', 'http://localhost:8080/fileResources/htmlFiles/1533749058375.html', 3, '2018-08-09 01:24:18', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (106, 5, '游戏test', 'http://localhost:8080/fileResources/htmlFiles/1533745942829.html', 3, '2018-08-09 00:32:23', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (107, 4, '游戏test', 'http://localhost:8080/fileResources/htmlFiles/1533747729412.html', 3, '2018-08-09 01:02:09', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (108, 6, '游戏test', 'http://localhost:8080/fileResources/htmlFiles/1533749058375.html', 3, '2018-08-09 01:24:18', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (109, 5, '游戏test', 'http://localhost:8080/fileResources/htmlFiles/1533745942829.html', 3, '2018-08-09 00:32:23', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (110, 4, '游戏test', 'http://localhost:8080/fileResources/htmlFiles/1533747729412.html', 3, '2018-08-09 01:02:09', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (111, 6, '游戏test', 'http://localhost:8080/fileResources/htmlFiles/1533749058375.html', 3, '2018-08-09 01:24:18', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (127, 5, '游戏test', 'http://localhost:8080/fileResources/htmlFiles/1533745942829.html', 3, '2018-08-09 00:32:23', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (128, 4, '游戏test', 'http://localhost:8080/fileResources/htmlFiles/1533747729412.html', 3, '2018-08-09 01:02:09', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (129, 6, '移动开发test', 'http://localhost:8080/fileResources/htmlFiles/1533749058375.html', 4, '2018-08-09 01:24:18', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (130, 5, '移动开发test', 'http://localhost:8080/fileResources/htmlFiles/1533745942829.html', 4, '2018-08-09 00:32:23', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (131, 4, '移动开发test', 'http://localhost:8080/fileResources/htmlFiles/1533747729412.html', 4, '2018-08-09 01:02:09', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (132, 6, '移动开发test', 'http://localhost:8080/fileResources/htmlFiles/1533749058375.html', 4, '2018-08-09 01:24:18', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (133, 5, '移动开发test', 'http://localhost:8080/fileResources/htmlFiles/1533745942829.html', 4, '2018-08-09 00:32:23', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (134, 4, '移动开发test', 'http://localhost:8080/fileResources/htmlFiles/1533747729412.html', 4, '2018-08-09 01:02:09', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (135, 6, '移动开发test', 'http://localhost:8080/fileResources/htmlFiles/1533749058375.html', 4, '2018-08-09 01:24:18', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (136, 5, '移动开发test', 'http://localhost:8080/fileResources/htmlFiles/1533745942829.html', 4, '2018-08-09 00:32:23', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (137, 4, '移动开发test', 'http://localhost:8080/fileResources/htmlFiles/1533747729412.html', 4, '2018-08-09 01:02:09', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (138, 6, '移动开发test', 'http://localhost:8080/fileResources/htmlFiles/1533749058375.html', 4, '2018-08-09 01:24:18', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (139, 5, '移动开发test', 'http://localhost:8080/fileResources/htmlFiles/1533745942829.html', 4, '2018-08-09 00:32:23', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (140, 4, '移动开发test', 'http://localhost:8080/fileResources/htmlFiles/1533747729412.html', 4, '2018-08-09 01:02:09', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (141, 6, '移动开发test', 'http://localhost:8080/fileResources/htmlFiles/1533749058375.html', 4, '2018-08-09 01:24:18', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (142, 5, '移动开发test', 'http://localhost:8080/fileResources/htmlFiles/1533745942829.html', 4, '2018-08-09 00:32:23', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (143, 4, '移动开发test', 'http://localhost:8080/fileResources/htmlFiles/1533747729412.html', 4, '2018-08-09 01:02:09', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (144, 6, '移动开发test', 'http://localhost:8080/fileResources/htmlFiles/1533749058375.html', 4, '2018-08-09 01:24:18', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (145, 5, '移动开发test', 'http://localhost:8080/fileResources/htmlFiles/1533745942829.html', 4, '2018-08-09 00:32:23', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (146, 4, '移动开发test', 'http://localhost:8080/fileResources/htmlFiles/1533747729412.html', 4, '2018-08-09 01:02:09', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (147, 6, '移动开发test', 'http://localhost:8080/fileResources/htmlFiles/1533749058375.html', 4, '2018-08-09 01:24:18', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (148, 5, '移动开发test', 'http://localhost:8080/fileResources/htmlFiles/1533745942829.html', 4, '2018-08-09 00:32:23', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (149, 4, '移动开发test', 'http://localhost:8080/fileResources/htmlFiles/1533747729412.html', 4, '2018-08-09 01:02:09', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (150, 6, '移动开发test', 'http://localhost:8080/fileResources/htmlFiles/1533749058375.html', 4, '2018-08-09 01:24:18', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (151, 5, '移动开发test', 'http://localhost:8080/fileResources/htmlFiles/1533745942829.html', 4, '2018-08-09 00:32:23', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (152, 4, '移动开发test', 'http://localhost:8080/fileResources/htmlFiles/1533747729412.html', 4, '2018-08-09 01:02:09', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (153, 6, '移动开发test', 'http://localhost:8080/fileResources/htmlFiles/1533749058375.html', 4, '2018-08-09 01:24:18', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (154, 5, '移动开发test', 'http://localhost:8080/fileResources/htmlFiles/1533745942829.html', 4, '2018-08-09 00:32:23', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (155, 4, '移动开发test', 'http://localhost:8080/fileResources/htmlFiles/1533747729412.html', 4, '2018-08-09 01:02:09', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (156, 6, '移动开发test', 'http://localhost:8080/fileResources/htmlFiles/1533749058375.html', 4, '2018-08-09 01:24:18', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (157, 5, '移动开发test', 'http://localhost:8080/fileResources/htmlFiles/1533745942829.html', 4, '2018-08-09 00:32:23', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (158, 4, '移动开发test', 'http://localhost:8080/fileResources/htmlFiles/1533747729412.html', 4, '2018-08-09 01:02:09', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (159, 6, '移动开发test', 'http://localhost:8080/fileResources/htmlFiles/1533749058375.html', 4, '2018-08-09 01:24:18', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (160, 5, '移动开发test', 'http://localhost:8080/fileResources/htmlFiles/1533745942829.html', 4, '2018-08-09 00:32:23', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (161, 4, '移动开发test', 'http://localhost:8080/fileResources/htmlFiles/1533747729412.html', 4, '2018-08-09 01:02:09', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (162, 6, '移动开发test', 'http://localhost:8080/fileResources/htmlFiles/1533749058375.html', 4, '2018-08-09 01:24:18', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (163, 5, '移动开发test', 'http://localhost:8080/fileResources/htmlFiles/1533745942829.html', 4, '2018-08-09 00:32:23', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (164, 4, '移动开发test', 'http://localhost:8080/fileResources/htmlFiles/1533747729412.html', 4, '2018-08-09 01:02:09', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (165, 6, '移动开发test', 'http://localhost:8080/fileResources/htmlFiles/1533749058375.html', 4, '2018-08-09 01:24:18', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (166, 5, '移动开发test', 'http://localhost:8080/fileResources/htmlFiles/1533745942829.html', 4, '2018-08-09 00:32:23', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (167, 4, '移动开发test', 'http://localhost:8080/fileResources/htmlFiles/1533747729412.html', 4, '2018-08-09 01:02:09', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (168, 6, '移动开发test', 'http://localhost:8080/fileResources/htmlFiles/1533749058375.html', 4, '2018-08-09 01:24:18', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (169, 5, '移动开发test', 'http://localhost:8080/fileResources/htmlFiles/1533745942829.html', 4, '2018-08-09 00:32:23', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (170, 4, '移动开发test', 'http://localhost:8080/fileResources/htmlFiles/1533747729412.html', 4, '2018-08-09 01:02:09', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (171, 6, '移动开发test', 'http://localhost:8080/fileResources/htmlFiles/1533749058375.html', 4, '2018-08-09 01:24:18', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (172, 5, '移动开发test', 'http://localhost:8080/fileResources/htmlFiles/1533745942829.html', 4, '2018-08-09 00:32:23', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (173, 4, '移动开发test', 'http://localhost:8080/fileResources/htmlFiles/1533747729412.html', 4, '2018-08-09 01:02:09', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (174, 6, '移动开发test', 'http://localhost:8080/fileResources/htmlFiles/1533749058375.html', 4, '2018-08-09 01:24:18', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (175, 5, '移动开发test', 'http://localhost:8080/fileResources/htmlFiles/1533745942829.html', 4, '2018-08-09 00:32:23', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (176, 4, '移动开发test', 'http://localhost:8080/fileResources/htmlFiles/1533747729412.html', 4, '2018-08-09 01:02:09', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (177, 6, '移动开发test', 'http://localhost:8080/fileResources/htmlFiles/1533749058375.html', 4, '2018-08-09 01:24:18', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (178, 5, '移动开发test', 'http://localhost:8080/fileResources/htmlFiles/1533745942829.html', 4, '2018-08-09 00:32:23', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (179, 4, '移动开发test', 'http://localhost:8080/fileResources/htmlFiles/1533747729412.html', 4, '2018-08-09 01:02:09', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (180, 6, '移动开发test', 'http://localhost:8080/fileResources/htmlFiles/1533749058375.html', 4, '2018-08-09 01:24:18', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (181, 5, '移动开发test', 'http://localhost:8080/fileResources/htmlFiles/1533745942829.html', 4, '2018-08-09 00:32:23', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (182, 4, '移动开发test', 'http://localhost:8080/fileResources/htmlFiles/1533747729412.html', 4, '2018-08-09 01:02:09', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (183, 6, '移动开发test', 'http://localhost:8080/fileResources/htmlFiles/1533749058375.html', 4, '2018-08-09 01:24:18', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (184, 5, '移动开发test', 'http://localhost:8080/fileResources/htmlFiles/1533745942829.html', 4, '2018-08-09 00:32:23', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (185, 4, '移动开发test', 'http://localhost:8080/fileResources/htmlFiles/1533747729412.html', 4, '2018-08-09 01:02:09', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (186, 6, '移动开发test', 'http://localhost:8080/fileResources/htmlFiles/1533749058375.html', 4, '2018-08-09 01:24:18', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (187, 5, '移动开发test', 'http://localhost:8080/fileResources/htmlFiles/1533745942829.html', 4, '2018-08-09 00:32:23', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (188, 4, '移动开发test', 'http://localhost:8080/fileResources/htmlFiles/1533747729412.html', 4, '2018-08-09 01:02:09', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (189, 6, '移动开发test', 'http://localhost:8080/fileResources/htmlFiles/1533749058375.html', 4, '2018-08-09 01:24:18', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (190, 5, '移动开发test', 'http://localhost:8080/fileResources/htmlFiles/1533745942829.html', 4, '2018-08-09 00:32:23', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (191, 4, '移动开发test', 'http://localhost:8080/fileResources/htmlFiles/1533747729412.html', 4, '2018-08-09 01:02:09', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (192, 6, '移动开发test', 'http://localhost:8080/fileResources/htmlFiles/1533749058375.html', 4, '2018-08-09 01:24:18', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (193, 5, '移动开发test', 'http://localhost:8080/fileResources/htmlFiles/1533745942829.html', 4, '2018-08-09 00:32:23', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (194, 4, '移动开发test', 'http://localhost:8080/fileResources/htmlFiles/1533747729412.html', 4, '2018-08-09 01:02:09', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (195, 6, '移动开发test', 'http://localhost:8080/fileResources/htmlFiles/1533749058375.html', 4, '2018-08-09 01:24:18', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (196, 5, '移动开发test', 'http://localhost:8080/fileResources/htmlFiles/1533745942829.html', 4, '2018-08-09 00:32:23', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (197, 4, '移动开发test', 'http://localhost:8080/fileResources/htmlFiles/1533747729412.html', 4, '2018-08-09 01:02:09', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (198, 6, '移动开发test', 'http://localhost:8080/fileResources/htmlFiles/1533749058375.html', 4, '2018-08-09 01:24:18', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (199, 5, '移动开发test', 'http://localhost:8080/fileResources/htmlFiles/1533745942829.html', 4, '2018-08-09 00:32:23', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (200, 4, '移动开发test', 'http://localhost:8080/fileResources/htmlFiles/1533747729412.html', 4, '2018-08-09 01:02:09', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (201, 6, '移动开发test', 'http://localhost:8080/fileResources/htmlFiles/1533749058375.html', 4, '2018-08-09 01:24:18', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (202, 5, '移动开发test', 'http://localhost:8080/fileResources/htmlFiles/1533745942829.html', 4, '2018-08-09 00:32:23', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (203, 4, '移动开发test', 'http://localhost:8080/fileResources/htmlFiles/1533747729412.html', 4, '2018-08-09 01:02:09', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (204, 6, '移动开发test', 'http://localhost:8080/fileResources/htmlFiles/1533749058375.html', 4, '2018-08-09 01:24:18', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (205, 5, '移动开发test', 'http://localhost:8080/fileResources/htmlFiles/1533745942829.html', 4, '2018-08-09 00:32:23', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (206, 4, '移动开发test', 'http://localhost:8080/fileResources/htmlFiles/1533747729412.html', 4, '2018-08-09 01:02:09', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (207, 6, '移动开发test', 'http://localhost:8080/fileResources/htmlFiles/1533749058375.html', 4, '2018-08-09 01:24:18', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (208, 5, '移动开发test', 'http://localhost:8080/fileResources/htmlFiles/1533745942829.html', 4, '2018-08-09 00:32:23', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (209, 4, '移动开发test', 'http://localhost:8080/fileResources/htmlFiles/1533747729412.html', 4, '2018-08-09 01:02:09', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (210, 6, '移动开发test', 'http://localhost:8080/fileResources/htmlFiles/1533749058375.html', 4, '2018-08-09 01:24:18', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (211, 5, '大数据', 'http://localhost:8080/fileResources/htmlFiles/1533745942829.html', 5, '2018-08-09 00:32:23', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (212, 4, '大数据', 'http://localhost:8080/fileResources/htmlFiles/1533747729412.html', 5, '2018-08-09 01:02:09', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (213, 6, '大数据', 'http://localhost:8080/fileResources/htmlFiles/1533749058375.html', 5, '2018-08-09 01:24:18', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (214, 5, '大数据', 'http://localhost:8080/fileResources/htmlFiles/1533745942829.html', 5, '2018-08-09 00:32:23', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (215, 4, '大数据', 'http://localhost:8080/fileResources/htmlFiles/1533747729412.html', 5, '2018-08-09 01:02:09', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (216, 6, '大数据', 'http://localhost:8080/fileResources/htmlFiles/1533749058375.html', 5, '2018-08-09 01:24:18', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (217, 5, '大数据', 'http://localhost:8080/fileResources/htmlFiles/1533745942829.html', 5, '2018-08-09 00:32:23', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (218, 4, '大数据', 'http://localhost:8080/fileResources/htmlFiles/1533747729412.html', 5, '2018-08-09 01:02:09', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (219, 6, '大数据', 'http://localhost:8080/fileResources/htmlFiles/1533749058375.html', 5, '2018-08-09 01:24:18', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (220, 5, '大数据', 'http://localhost:8080/fileResources/htmlFiles/1533745942829.html', 5, '2018-08-09 00:32:23', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (221, 4, '大数据', 'http://localhost:8080/fileResources/htmlFiles/1533747729412.html', 5, '2018-08-09 01:02:09', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (222, 6, '大数据', 'http://localhost:8080/fileResources/htmlFiles/1533749058375.html', 5, '2018-08-09 01:24:18', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (254, 5, '大数据', 'http://localhost:8080/fileResources/htmlFiles/1533745942829.html', 5, '2018-08-09 00:32:23', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (255, 4, '大数据', 'http://localhost:8080/fileResources/htmlFiles/1533747729412.html', 5, '2018-08-09 01:02:09', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (256, 6, '大数据', 'http://localhost:8080/fileResources/htmlFiles/1533749058375.html', 5, '2018-08-09 01:24:18', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (257, 5, '大数据', 'http://localhost:8080/fileResources/htmlFiles/1533745942829.html', 5, '2018-08-09 00:32:23', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (258, 4, '大数据', 'http://localhost:8080/fileResources/htmlFiles/1533747729412.html', 5, '2018-08-09 01:02:09', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (259, 6, '大数据', 'http://localhost:8080/fileResources/htmlFiles/1533749058375.html', 5, '2018-08-09 01:24:18', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (260, 5, '大数据', 'http://localhost:8080/fileResources/htmlFiles/1533745942829.html', 5, '2018-08-09 00:32:23', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (261, 4, '大数据', 'http://localhost:8080/fileResources/htmlFiles/1533747729412.html', 5, '2018-08-09 01:02:09', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (262, 6, '大数据', 'http://localhost:8080/fileResources/htmlFiles/1533749058375.html', 5, '2018-08-09 01:24:18', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (263, 5, '大数据', 'http://localhost:8080/fileResources/htmlFiles/1533745942829.html', 5, '2018-08-09 00:32:23', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (264, 4, '大数据', 'http://localhost:8080/fileResources/htmlFiles/1533747729412.html', 5, '2018-08-09 01:02:09', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (265, 6, '大数据', 'http://localhost:8080/fileResources/htmlFiles/1533749058375.html', 5, '2018-08-09 01:24:18', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (266, 5, '大数据', 'http://localhost:8080/fileResources/htmlFiles/1533745942829.html', 5, '2018-08-09 00:32:23', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (267, 4, '大数据', 'http://localhost:8080/fileResources/htmlFiles/1533747729412.html', 5, '2018-08-09 01:02:09', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (268, 6, '大数据', 'http://localhost:8080/fileResources/htmlFiles/1533749058375.html', 5, '2018-08-09 01:24:18', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (269, 5, '大数据', 'http://localhost:8080/fileResources/htmlFiles/1533745942829.html', 5, '2018-08-09 00:32:23', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (270, 4, '大数据', 'http://localhost:8080/fileResources/htmlFiles/1533747729412.html', 5, '2018-08-09 01:02:09', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (271, 6, '大数据', 'http://localhost:8080/fileResources/htmlFiles/1533749058375.html', 5, '2018-08-09 01:24:18', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (272, 5, '大数据', 'http://localhost:8080/fileResources/htmlFiles/1533745942829.html', 5, '2018-08-09 00:32:23', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (273, 4, '大数据', 'http://localhost:8080/fileResources/htmlFiles/1533747729412.html', 5, '2018-08-09 01:02:09', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (274, 6, '大数据', 'http://localhost:8080/fileResources/htmlFiles/1533749058375.html', 5, '2018-08-09 01:24:18', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (275, 5, '大数据', 'http://localhost:8080/fileResources/htmlFiles/1533745942829.html', 5, '2018-08-09 00:32:23', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (276, 4, '大数据', 'http://localhost:8080/fileResources/htmlFiles/1533747729412.html', 5, '2018-08-09 01:02:09', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (277, 6, '大数据', 'http://localhost:8080/fileResources/htmlFiles/1533749058375.html', 5, '2018-08-09 01:24:18', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (278, 5, '物联网test', 'http://localhost:8080/fileResources/htmlFiles/1533745942829.html', 6, '2018-08-09 00:32:23', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (279, 4, '物联网test', 'http://localhost:8080/fileResources/htmlFiles/1533747729412.html', 6, '2018-08-09 01:02:09', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (280, 6, '物联网test', 'http://localhost:8080/fileResources/htmlFiles/1533749058375.html', 6, '2018-08-09 01:24:18', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (281, 5, '物联网test', 'http://localhost:8080/fileResources/htmlFiles/1533745942829.html', 6, '2018-08-09 00:32:23', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (282, 4, '物联网test', 'http://localhost:8080/fileResources/htmlFiles/1533747729412.html', 6, '2018-08-09 01:02:09', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (283, 6, '物联网test', 'http://localhost:8080/fileResources/htmlFiles/1533749058375.html', 6, '2018-08-09 01:24:18', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (284, 5, '物联网test', 'http://localhost:8080/fileResources/htmlFiles/1533745942829.html', 6, '2018-08-09 00:32:23', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (285, 4, '物联网test', 'http://localhost:8080/fileResources/htmlFiles/1533747729412.html', 6, '2018-08-09 01:02:09', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (286, 6, '物联网test', 'http://localhost:8080/fileResources/htmlFiles/1533749058375.html', 6, '2018-08-09 01:24:18', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (287, 5, '物联网test', 'http://localhost:8080/fileResources/htmlFiles/1533745942829.html', 6, '2018-08-09 00:32:23', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (288, 4, '物联网test', 'http://localhost:8080/fileResources/htmlFiles/1533747729412.html', 6, '2018-08-09 01:02:09', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (289, 6, '物联网test', 'http://localhost:8080/fileResources/htmlFiles/1533749058375.html', 6, '2018-08-09 01:24:18', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (290, 5, '物联网test', 'http://localhost:8080/fileResources/htmlFiles/1533745942829.html', 6, '2018-08-09 00:32:23', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (291, 4, '物联网test', 'http://localhost:8080/fileResources/htmlFiles/1533747729412.html', 6, '2018-08-09 01:02:09', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (292, 6, '物联网test', 'http://localhost:8080/fileResources/htmlFiles/1533749058375.html', 6, '2018-08-09 01:24:18', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (293, 5, '物联网test', 'http://localhost:8080/fileResources/htmlFiles/1533745942829.html', 6, '2018-08-09 00:32:23', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (294, 4, '物联网test', 'http://localhost:8080/fileResources/htmlFiles/1533747729412.html', 6, '2018-08-09 01:02:09', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (295, 6, '物联网test', 'http://localhost:8080/fileResources/htmlFiles/1533749058375.html', 6, '2018-08-09 01:24:18', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (296, 5, '物联网test', 'http://localhost:8080/fileResources/htmlFiles/1533745942829.html', 6, '2018-08-09 00:32:23', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (297, 4, '物联网test', 'http://localhost:8080/fileResources/htmlFiles/1533747729412.html', 6, '2018-08-09 01:02:09', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (298, 6, '物联网test', 'http://localhost:8080/fileResources/htmlFiles/1533749058375.html', 6, '2018-08-09 01:24:18', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (299, 5, '物联网test', 'http://localhost:8080/fileResources/htmlFiles/1533745942829.html', 6, '2018-08-09 00:32:23', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (300, 4, '物联网test', 'http://localhost:8080/fileResources/htmlFiles/1533747729412.html', 6, '2018-08-09 01:02:09', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (301, 6, '物联网test', 'http://localhost:8080/fileResources/htmlFiles/1533749058375.html', 6, '2018-08-09 01:24:18', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (302, 5, '物联网test', 'http://localhost:8080/fileResources/htmlFiles/1533745942829.html', 6, '2018-08-09 00:32:23', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (303, 4, '物联网test', 'http://localhost:8080/fileResources/htmlFiles/1533747729412.html', 6, '2018-08-09 01:02:09', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (304, 6, '物联网test', 'http://localhost:8080/fileResources/htmlFiles/1533749058375.html', 6, '2018-08-09 01:24:18', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (305, 5, '物联网test', 'http://localhost:8080/fileResources/htmlFiles/1533745942829.html', 6, '2018-08-09 00:32:23', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (306, 4, '物联网test', 'http://localhost:8080/fileResources/htmlFiles/1533747729412.html', 6, '2018-08-09 01:02:09', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (307, 6, '物联网test', 'http://localhost:8080/fileResources/htmlFiles/1533749058375.html', 6, '2018-08-09 01:24:18', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (308, 5, '物联网test', 'http://localhost:8080/fileResources/htmlFiles/1533745942829.html', 6, '2018-08-09 00:32:23', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (309, 4, '物联网test', 'http://localhost:8080/fileResources/htmlFiles/1533747729412.html', 6, '2018-08-09 01:02:09', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (310, 6, '物联网test', 'http://localhost:8080/fileResources/htmlFiles/1533749058375.html', 6, '2018-08-09 01:24:18', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (311, 5, '物联网test', 'http://localhost:8080/fileResources/htmlFiles/1533745942829.html', 6, '2018-08-09 00:32:23', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (312, 4, '物联网test', 'http://localhost:8080/fileResources/htmlFiles/1533747729412.html', 6, '2018-08-09 01:02:09', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (313, 6, '物联网test', 'http://localhost:8080/fileResources/htmlFiles/1533749058375.html', 6, '2018-08-09 01:24:18', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (314, 5, '物联网test', 'http://localhost:8080/fileResources/htmlFiles/1533745942829.html', 6, '2018-08-09 00:32:23', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (315, 4, '物联网test', 'http://localhost:8080/fileResources/htmlFiles/1533747729412.html', 6, '2018-08-09 01:02:09', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (316, 6, '物联网test', 'http://localhost:8080/fileResources/htmlFiles/1533749058375.html', 6, '2018-08-09 01:24:18', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (317, 5, '物联网test', 'http://localhost:8080/fileResources/htmlFiles/1533745942829.html', 6, '2018-08-09 00:32:23', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (318, 4, '物联网test', 'http://localhost:8080/fileResources/htmlFiles/1533747729412.html', 6, '2018-08-09 01:02:09', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (319, 6, '物联网test', 'http://localhost:8080/fileResources/htmlFiles/1533749058375.html', 6, '2018-08-09 01:24:18', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (320, 5, '物联网test', 'http://localhost:8080/fileResources/htmlFiles/1533745942829.html', 6, '2018-08-09 00:32:23', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (321, 4, '物联网test', 'http://localhost:8080/fileResources/htmlFiles/1533747729412.html', 6, '2018-08-09 01:02:09', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (322, 6, '物联网test', 'http://localhost:8080/fileResources/htmlFiles/1533749058375.html', 6, '2018-08-09 01:24:18', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (323, 5, '物联网test', 'http://localhost:8080/fileResources/htmlFiles/1533745942829.html', 6, '2018-08-09 00:32:23', 0, 0, 0, 1, NULL, 0);
INSERT INTO `sarticle` VALUES (511, 9, '博客编辑测试', 'http://localhost:8080/fileResources/htmlFiles/1533753474533.html', 1, '2018-08-09 02:37:55', 2, 0, 0, 1, NULL, 0);

-- ----------------------------
-- Table structure for sbonusrecord
-- ----------------------------
DROP TABLE IF EXISTS `sbonusrecord`;
CREATE TABLE `sbonusrecord`  (
  `brid` int NOT NULL AUTO_INCREMENT,
  `uid` int NULL DEFAULT NULL,
  `sourceId` int NULL DEFAULT NULL,
  `type` int NULL DEFAULT NULL,
  `state` int NULL DEFAULT NULL,
  `bonus` int NULL DEFAULT NULL,
  `descr` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `time` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`brid`) USING BTREE,
  INDEX `FK_Reference_8`(`uid` ASC) USING BTREE,
  CONSTRAINT `FK_Reference_8` FOREIGN KEY (`uid`) REFERENCES `suser` (`uid`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 30 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sbonusrecord
-- ----------------------------
INSERT INTO `sbonusrecord` VALUES (5, 5, 7, 2, 1, -5, '提问:去除chrome谷歌浏览器input自带边框？', NULL);
INSERT INTO `sbonusrecord` VALUES (6, 1, 7, 2, 0, 5, '暂存提问积分,问题:去除chrome谷歌浏览器input自带边框？', NULL);
INSERT INTO `sbonusrecord` VALUES (7, 4, 4, 1, 1, -3, '下载资源:1533747064130_tx2.png', NULL);
INSERT INTO `sbonusrecord` VALUES (8, 5, 4, 1, 0, 3, '资源被下载:1533747064130_tx2.png', '2018-08-09 01:03:46');
INSERT INTO `sbonusrecord` VALUES (9, 4, 8, 2, 1, -5, '提问:程序员问题', NULL);
INSERT INTO `sbonusrecord` VALUES (10, 1, 8, 2, 0, 5, '暂存提问积分,问题:程序员问题', NULL);
INSERT INTO `sbonusrecord` VALUES (11, 6, 3, 1, 1, -3, '下载资源:1533746446367_8-3业务逻辑.txt', NULL);
INSERT INTO `sbonusrecord` VALUES (12, 5, 3, 1, 0, 3, '资源被下载:1533746446367_8-3业务逻辑.txt', '2018-08-09 01:16:32');
INSERT INTO `sbonusrecord` VALUES (13, 6, 4, 1, 1, -3, '下载资源:1533747064130_tx2.png', NULL);
INSERT INTO `sbonusrecord` VALUES (14, 5, 4, 1, 0, 3, '资源被下载:1533747064130_tx2.png', '2018-08-09 01:17:05');
INSERT INTO `sbonusrecord` VALUES (15, 6, 9, 2, 1, -20, '提问:关于mac安装homebrew报错！急急急！求大佬帮助！！！', NULL);
INSERT INTO `sbonusrecord` VALUES (16, 1, 9, 2, 0, 20, '暂存提问积分,问题:关于mac安装homebrew报错！急急急！求大佬帮助！！！', NULL);
INSERT INTO `sbonusrecord` VALUES (17, 1, 7, 2, 1, -5, '积分奖励,提问被解决', NULL);
INSERT INTO `sbonusrecord` VALUES (18, 5, 7, 2, 0, 5, '解决提问，最佳答案', NULL);
INSERT INTO `sbonusrecord` VALUES (22, 6, 7, 1, 0, 3, '资源被下载:1533748573883_需求731.txt', '2018-08-09 02:20:26');
INSERT INTO `sbonusrecord` VALUES (24, 1, 10, 2, 0, 10, '暂存提问积分,问题:问题测试', NULL);
INSERT INTO `sbonusrecord` VALUES (25, 9, 7, 1, 1, -3, '下载资源:1533748573883_需求731.txt', NULL);
INSERT INTO `sbonusrecord` VALUES (26, 6, 7, 1, 0, 3, '资源被下载:1533748573883_需求731.txt', '2018-08-09 02:38:44');
INSERT INTO `sbonusrecord` VALUES (27, 9, 11, 2, 1, -2, '提问:问题测试', NULL);
INSERT INTO `sbonusrecord` VALUES (28, 1, 11, 2, 0, 2, '暂存提问积分,问题:问题测试', NULL);
INSERT INTO `sbonusrecord` VALUES (29, 1, 11, 2, 1, -2, '积分奖励,提问被解决', NULL);
INSERT INTO `sbonusrecord` VALUES (30, 9, 11, 2, 0, 2, '解决提问，最佳答案', NULL);

-- ----------------------------
-- Table structure for schedule_job
-- ----------------------------
DROP TABLE IF EXISTS `schedule_job`;
CREATE TABLE `schedule_job`  (
  `id` bigint NOT NULL COMMENT 'id',
  `bean_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT 'spring bean名称',
  `params` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '参数',
  `cron_expression` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT 'cron表达式',
  `status` tinyint UNSIGNED NULL DEFAULT NULL COMMENT '任务状态  0：暂停  1：正常',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
  `creator` bigint NULL DEFAULT NULL COMMENT '创建者',
  `create_date` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `updater` bigint NULL DEFAULT NULL COMMENT '更新者',
  `update_date` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_create_date`(`create_date` ASC) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '定时任务' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of schedule_job
-- ----------------------------
INSERT INTO `schedule_job` VALUES (1067246875800000076, 'testTask', 'renren', '0 0/30 * * * ?', 0, '有参测试，多个参数使用json', 1067246875800000001, '2022-11-14 21:13:57', 1067246875800000001, '2022-11-14 21:13:57');

-- ----------------------------
-- Table structure for schedule_job_log
-- ----------------------------
DROP TABLE IF EXISTS `schedule_job_log`;
CREATE TABLE `schedule_job_log`  (
  `id` bigint NOT NULL COMMENT 'id',
  `job_id` bigint NOT NULL COMMENT '任务id',
  `bean_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT 'spring bean名称',
  `params` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '参数',
  `status` tinyint UNSIGNED NOT NULL COMMENT '任务状态    0：失败    1：成功',
  `error` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '失败信息',
  `times` int NOT NULL COMMENT '耗时(单位：毫秒)',
  `create_date` datetime NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_job_id`(`job_id` ASC) USING BTREE,
  INDEX `idx_create_date`(`create_date` ASC) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '定时任务日志' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of schedule_job_log
-- ----------------------------

-- ----------------------------
-- Table structure for scollection
-- ----------------------------
DROP TABLE IF EXISTS `scollection`;
CREATE TABLE `scollection`  (
  `coid` int NOT NULL AUTO_INCREMENT,
  `uid` int NULL DEFAULT NULL,
  `arid` int NULL DEFAULT NULL,
  `time` datetime NULL DEFAULT NULL,
  `deltag` int NOT NULL DEFAULT 0,
  PRIMARY KEY (`coid`) USING BTREE,
  INDEX `FK_Reference_5`(`uid` ASC) USING BTREE,
  INDEX `FK_Reference_6`(`arid` ASC) USING BTREE,
  CONSTRAINT `FK_Reference_5` FOREIGN KEY (`uid`) REFERENCES `suser` (`uid`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FK_Reference_6` FOREIGN KEY (`arid`) REFERENCES `sarticle` (`arid`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of scollection
-- ----------------------------

-- ----------------------------
-- Table structure for scomment
-- ----------------------------
DROP TABLE IF EXISTS `scomment`;
CREATE TABLE `scomment`  (
  `cid` int NOT NULL AUTO_INCREMENT,
  `arid` int NULL DEFAULT NULL,
  `uid` int NULL DEFAULT NULL,
  `content` varchar(128) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `dznum` int NOT NULL DEFAULT 0,
  `time` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`cid`) USING BTREE,
  INDEX `FK_Reference_3`(`arid` ASC) USING BTREE,
  INDEX `FK_Reference_4`(`uid` ASC) USING BTREE,
  CONSTRAINT `FK_Reference_3` FOREIGN KEY (`arid`) REFERENCES `sarticle` (`arid`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FK_Reference_4` FOREIGN KEY (`uid`) REFERENCES `suser` (`uid`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of scomment
-- ----------------------------
INSERT INTO `scomment` VALUES (2, 5, 4, '这篇文章写得很好！', 0, '2018-08-09 00:58:14');
INSERT INTO `scomment` VALUES (3, 5, 6, '楼上说的很对，我同意。', 0, '2018-08-09 01:15:10');
INSERT INTO `scomment` VALUES (4, 7, 6, '关于这篇文章，大家有什么想说的吗？', 0, '2018-08-09 01:25:03');
INSERT INTO `scomment` VALUES (5, 7, 5, '我觉得还有一些需要补充，不够全面', 0, '2018-08-09 01:25:51');
INSERT INTO `scomment` VALUES (8, 5, 9, '回复测试', 0, '2018-08-09 02:37:17');

-- ----------------------------
-- Table structure for sdelrecord
-- ----------------------------
DROP TABLE IF EXISTS `sdelrecord`;
CREATE TABLE `sdelrecord`  (
  `deid` int NOT NULL AUTO_INCREMENT,
  `type` int NULL DEFAULT NULL,
  `keyid` int NULL DEFAULT NULL,
  `reason` varchar(32) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '审核通过',
  `time` datetime NULL DEFAULT NULL,
  `descr` varchar(32) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`deid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 33 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sdelrecord
-- ----------------------------
INSERT INTO `sdelrecord` VALUES (12, 1, 1, '', '2018-08-09 00:35:07', NULL);
INSERT INTO `sdelrecord` VALUES (13, 0, 5, '审核通过!', '2018-08-09 00:35:14', NULL);
INSERT INTO `sdelrecord` VALUES (14, 2, 7, '审核通过!', '2018-08-09 00:35:27', NULL);
INSERT INTO `sdelrecord` VALUES (15, 1, 1, '', '2018-08-09 00:55:55', NULL);
INSERT INTO `sdelrecord` VALUES (16, 1, 3, '审核通过!', '2018-08-09 00:56:03', NULL);
INSERT INTO `sdelrecord` VALUES (17, 1, 4, '审核通过!', '2018-08-09 00:56:26', NULL);
INSERT INTO `sdelrecord` VALUES (18, 1, 5, '审核通过!', '2018-08-09 00:56:37', NULL);
INSERT INTO `sdelrecord` VALUES (19, 0, 6, '审核通过!', '2018-08-09 01:05:48', NULL);
INSERT INTO `sdelrecord` VALUES (20, 2, 8, '审核通过!', '2018-08-09 01:05:53', NULL);
INSERT INTO `sdelrecord` VALUES (21, 1, 6, '审核通过!', '2018-08-09 01:05:57', NULL);
INSERT INTO `sdelrecord` VALUES (22, 1, 6, '审核通过!', '2018-08-09 01:21:00', NULL);
INSERT INTO `sdelrecord` VALUES (23, 2, 9, '审核通过!', '2018-08-09 01:21:09', NULL);
INSERT INTO `sdelrecord` VALUES (24, 1, 7, '审核通过!', '2018-08-09 01:21:13', NULL);
INSERT INTO `sdelrecord` VALUES (25, 1, 7, '审核通过!', '2018-08-09 01:24:25', NULL);
INSERT INTO `sdelrecord` VALUES (26, 0, 7, '审核通过!', '2018-08-09 01:24:32', NULL);
INSERT INTO `sdelrecord` VALUES (27, 0, 7, '审核通过!', '2018-08-09 01:42:11', NULL);
INSERT INTO `sdelrecord` VALUES (28, 0, 510, '审核通过!', '2018-08-09 02:18:13', NULL);
INSERT INTO `sdelrecord` VALUES (29, 1, 8, '审核通过!', '2018-08-09 02:19:55', NULL);
INSERT INTO `sdelrecord` VALUES (30, 2, 10, '审核通过!', '2018-08-09 02:21:36', NULL);
INSERT INTO `sdelrecord` VALUES (31, 0, 511, '审核通过!', '2018-08-09 02:38:23', NULL);
INSERT INTO `sdelrecord` VALUES (32, 1, 9, '审核通过!', '2018-08-09 02:39:44', NULL);
INSERT INTO `sdelrecord` VALUES (33, 2, 11, '审核通过!', '2018-08-09 02:40:37', NULL);

-- ----------------------------
-- Table structure for sdictionary
-- ----------------------------
DROP TABLE IF EXISTS `sdictionary`;
CREATE TABLE `sdictionary`  (
  `fid` int NOT NULL AUTO_INCREMENT,
  `fname` varchar(16) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `fcode` int NULL DEFAULT NULL,
  `fvalue` varchar(16) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `pcode` int NULL DEFAULT NULL,
  PRIMARY KEY (`fid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sdictionary
-- ----------------------------
INSERT INTO `sdictionary` VALUES (1, 'state', 0, '未审核', 0);
INSERT INTO `sdictionary` VALUES (2, 'state', 0, '审核通过', 1);
INSERT INTO `sdictionary` VALUES (3, 'state', 0, '审核未通过', 2);
INSERT INTO `sdictionary` VALUES (4, 'type', 0, '博客', 0);
INSERT INTO `sdictionary` VALUES (5, 'type', 0, '资源', 1);
INSERT INTO `sdictionary` VALUES (6, 'type', 0, '问题', 2);
INSERT INTO `sdictionary` VALUES (7, 'deltag', 0, '默认', 0);
INSERT INTO `sdictionary` VALUES (8, 'deltag', 0, '删除', 1);

-- ----------------------------
-- Table structure for sdownload
-- ----------------------------
DROP TABLE IF EXISTS `sdownload`;
CREATE TABLE `sdownload`  (
  `dlid` int NOT NULL AUTO_INCREMENT,
  `uid` int NULL DEFAULT NULL,
  `filename` varchar(128) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `time` datetime NULL DEFAULT NULL,
  `bonus` int NULL DEFAULT NULL,
  `type` int NULL DEFAULT NULL,
  `descr` varchar(128) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `dstate` int NULL DEFAULT NULL,
  `url` varchar(128) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `state` int NULL DEFAULT 0,
  `reason` varchar(32) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `dnum` int NULL DEFAULT 0,
  `deltag` int NOT NULL DEFAULT 0,
  PRIMARY KEY (`dlid`) USING BTREE,
  INDEX `FK_Reference_7`(`uid` ASC) USING BTREE,
  CONSTRAINT `FK_Reference_7` FOREIGN KEY (`uid`) REFERENCES `suser` (`uid`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sdownload
-- ----------------------------
INSERT INTO `sdownload` VALUES (3, 5, '1533746446367_8-3业务逻辑.txt', '2018-08-09 00:40:46', 3, 2, '这是关于业务逻辑的笔记', 0, 'D:\\itts\\target\\ittsGp\\fileResources\\fileUpload\\1533746446367_8-3业务逻辑.txt', 1, NULL, 1, 0);
INSERT INTO `sdownload` VALUES (4, 5, '1533747064130_tx2.png', '2018-08-09 00:51:04', 3, 4, '图图图', 0, 'D:\\itts\\target\\ittsGp\\fileResources\\fileUpload\\1533747064130_tx2.png', 1, NULL, 14, 0);
INSERT INTO `sdownload` VALUES (5, 5, '1533747311433_flag.jpg', '2018-08-09 00:55:11', 4, 1, '这是个flag！', 0, 'D:\\itts\\target\\ittsGp\\fileResources\\fileUpload\\1533747311433_flag.jpg', 1, NULL, 8, 0);
INSERT INTO `sdownload` VALUES (6, 4, '1533747861597_域名备案.png', '2018-08-09 01:04:22', 5, 1, '域名问题', 0, 'D:\\itts\\target\\ittsGp\\fileResources\\fileUpload\\1533747861597_域名备案.png', 1, NULL, 6, 0);
INSERT INTO `sdownload` VALUES (7, 6, '1533748573883_需求731.txt', '2018-08-09 01:16:14', 3, 3, '需求文档，记录设计需求', 0, 'D:\\itts\\target\\ittsGp\\fileResources\\fileUpload\\1533748573883_需求731.txt', 1, NULL, 7, 0);
INSERT INTO `sdownload` VALUES (9, 9, '1533753561665_数据库字段说明.txt', '2018-08-09 02:39:22', 2, 3, '上传测试', 0, 'D:\\itts\\target\\ittsGp\\fileResources\\fileUpload\\1533753561665_数据库字段说明.txt', 1, NULL, 0, 0);

-- ----------------------------
-- Table structure for squestion
-- ----------------------------
DROP TABLE IF EXISTS `squestion`;
CREATE TABLE `squestion`  (
  `qid` int NOT NULL AUTO_INCREMENT,
  `uid` int NULL DEFAULT NULL,
  `title` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `content` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `bonus` int NULL DEFAULT 0,
  `time` datetime NULL DEFAULT NULL,
  `readnum` int NULL DEFAULT 0,
  `dstate` int NULL DEFAULT 0,
  `state` int NULL DEFAULT 0,
  `reason` varchar(32) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `deltag` int NULL DEFAULT 0,
  PRIMARY KEY (`qid`) USING BTREE,
  INDEX `FK_Reference_9`(`uid` ASC) USING BTREE,
  CONSTRAINT `FK_Reference_9` FOREIGN KEY (`uid`) REFERENCES `suser` (`uid`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of squestion
-- ----------------------------
INSERT INTO `squestion` VALUES (7, 5, '去除chrome谷歌浏览器input自带边框？', '去除chrome谷歌浏览器input自带边框\r\nchrome谷歌浏览器input会自带边框，这个很讨厌，你可以更改这个边框样式，也可以直接出去掉，哈有什么方法', 5, '2018-08-09 00:34:28', 0, 2, 1, NULL, 0);
INSERT INTO `squestion` VALUES (8, 4, '程序员问题', '程序员为什么一定要加班？', 5, '2018-08-09 01:05:16', 0, 0, 1, NULL, 0);
INSERT INTO `squestion` VALUES (9, 6, '关于mac安装homebrew报错！急急急！求大佬帮助！！！', 'mac安装homebrew报错，，， 这个问题要怎么解决，实在是无能为力了，求帮助', 20, '2018-08-09 01:20:43', 0, 0, 1, NULL, 0);
INSERT INTO `squestion` VALUES (11, 9, '问题测试', '问题内容', 2, '2018-08-09 02:40:16', 0, 11, 1, NULL, 0);

-- ----------------------------
-- Table structure for stcoin
-- ----------------------------
DROP TABLE IF EXISTS `stcoin`;
CREATE TABLE `stcoin`  (
  `tid` int NOT NULL AUTO_INCREMENT,
  `pid` int NULL DEFAULT NULL,
  `time` datetime NULL DEFAULT NULL,
  `money` int NULL DEFAULT 0,
  PRIMARY KEY (`tid`) USING BTREE,
  INDEX `FK_Reference_12`(`pid` ASC) USING BTREE,
  CONSTRAINT `FK_Reference_12` FOREIGN KEY (`pid`) REFERENCES `suserinfo` (`pid`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of stcoin
-- ----------------------------

-- ----------------------------
-- Table structure for suser
-- ----------------------------
DROP TABLE IF EXISTS `suser`;
CREATE TABLE `suser`  (
  `uid` int NOT NULL AUTO_INCREMENT,
  `user` varchar(32) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `password` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `type` int NOT NULL DEFAULT 0,
  PRIMARY KEY (`uid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of suser
-- ----------------------------
INSERT INTO `suser` VALUES (1, '中间商', '202cb962ac59075b964b07152d234b70', 0);
INSERT INTO `suser` VALUES (4, '627695620@qq.com', 'e10adc3949ba59abbe56e057f20f883e', 0);
INSERT INTO `suser` VALUES (5, '291972921@qq.com', 'e10adc3949ba59abbe56e057f20f883e', 0);
INSERT INTO `suser` VALUES (6, '1327987604@qq.com', 'e10adc3949ba59abbe56e057f20f883e', 0);
INSERT INTO `suser` VALUES (9, '790612865@qq.com', 'e10adc3949ba59abbe56e057f20f883e', 0);

-- ----------------------------
-- Table structure for suserinfo
-- ----------------------------
DROP TABLE IF EXISTS `suserinfo`;
CREATE TABLE `suserinfo`  (
  `pid` int NOT NULL AUTO_INCREMENT,
  `uid` int NULL DEFAULT NULL,
  `nickname` varchar(16) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `sex` int NULL DEFAULT NULL,
  `age` int NULL DEFAULT NULL,
  `email` varchar(32) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `birthday` date NULL DEFAULT NULL,
  `descr` varchar(32) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `picurl` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT 'http://localhost:8080/resources/img/tx2.png',
  `pbonus` int NOT NULL DEFAULT 200,
  `vip` int NOT NULL DEFAULT 0,
  `T` int NOT NULL DEFAULT 0,
  PRIMARY KEY (`pid`) USING BTREE,
  INDEX `FK_Reference_1`(`uid` ASC) USING BTREE,
  CONSTRAINT `FK_Reference_1` FOREIGN KEY (`uid`) REFERENCES `suser` (`uid`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of suserinfo
-- ----------------------------
INSERT INTO `suserinfo` VALUES (1, 1, '中间商', NULL, NULL, NULL, NULL, NULL, 'http://139.196.136.237/resources/img/tx2.png', 250, 0, 0);
INSERT INTO `suserinfo` VALUES (4, 4, '冰山淡影', 0, 19, '627695620@qq.com', NULL, '把爱曲折、凉城旧梦、懂我别走、', 'http://139.196.136.237/resources/img/tx1.png', 192, 0, 0);
INSERT INTO `suserinfo` VALUES (5, 5, '清风桴栀', 0, 18, '291972921@qq.com', NULL, '路要自己走，关要自己闯', 'http://139.196.136.237/resources/img/tx6.png', 204, 0, 0);
INSERT INTO `suserinfo` VALUES (6, 6, '越仲夏', 1, 19, '1566655@qq.com', NULL, '我没有说明', 'http://139.196.136.237/resources/img/tx3.png', 180, 0, 0);
INSERT INTO `suserinfo` VALUES (9, 9, '昵称', 0, 20, '79@qq.com', NULL, '描述', 'http://139.196.136.237/resources/img/tx2.png', 197, 0, 0);

-- ----------------------------
-- Table structure for sys_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept`  (
  `id` bigint NOT NULL COMMENT 'id',
  `pid` bigint NULL DEFAULT NULL COMMENT '上级ID',
  `pids` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '所有上级ID，用逗号分开',
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '部门名称',
  `sort` int UNSIGNED NULL DEFAULT NULL COMMENT '排序',
  `creator` bigint NULL DEFAULT NULL COMMENT '创建者',
  `create_date` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `updater` bigint NULL DEFAULT NULL COMMENT '更新者',
  `update_date` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_pid`(`pid` ASC) USING BTREE,
  INDEX `idx_sort`(`sort` ASC) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '部门管理' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
INSERT INTO `sys_dept` VALUES (1067246875800000062, 1067246875800000063, '1067246875800000066,1067246875800000063', '技术部', 2, 1067246875800000001, '2022-11-14 21:13:57', 1067246875800000001, '2022-11-14 21:13:57');
INSERT INTO `sys_dept` VALUES (1067246875800000063, 1067246875800000066, '1067246875800000066', '长沙分公司', 1, 1067246875800000001, '2022-11-14 21:13:57', 1067246875800000001, '2022-11-14 21:13:57');
INSERT INTO `sys_dept` VALUES (1067246875800000064, 1067246875800000066, '1067246875800000066', '上海分公司', 0, 1067246875800000001, '2022-11-14 21:13:57', 1067246875800000001, '2022-11-14 21:13:57');
INSERT INTO `sys_dept` VALUES (1067246875800000065, 1067246875800000064, '1067246875800000066,1067246875800000064', '市场部', 0, 1067246875800000001, '2022-11-14 21:13:57', 1067246875800000001, '2022-11-14 21:13:57');
INSERT INTO `sys_dept` VALUES (1067246875800000066, 0, '0', '人人开源集团', 0, 1067246875800000001, '2022-11-14 21:13:57', 1067246875800000001, '2022-11-14 21:13:57');
INSERT INTO `sys_dept` VALUES (1067246875800000067, 1067246875800000064, '1067246875800000066,1067246875800000064', '销售部', 0, 1067246875800000001, '2022-11-14 21:13:57', 1067246875800000001, '2022-11-14 21:13:57');
INSERT INTO `sys_dept` VALUES (1067246875800000068, 1067246875800000063, '1067246875800000066,1067246875800000063', '产品部', 1, 1067246875800000001, '2022-11-14 21:13:57', 1067246875800000001, '2022-11-14 21:13:57');

-- ----------------------------
-- Table structure for sys_dict_data
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_data`;
CREATE TABLE `sys_dict_data`  (
  `id` bigint NOT NULL COMMENT 'id',
  `dict_type_id` bigint NOT NULL COMMENT '字典类型ID',
  `dict_label` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '字典标签',
  `dict_value` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '字典值',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
  `sort` int UNSIGNED NULL DEFAULT NULL COMMENT '排序',
  `creator` bigint NULL DEFAULT NULL COMMENT '创建者',
  `create_date` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `updater` bigint NULL DEFAULT NULL COMMENT '更新者',
  `update_date` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_dict_type_value`(`dict_type_id` ASC, `dict_value` ASC) USING BTREE,
  INDEX `idx_sort`(`sort` ASC) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '字典数据' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_dict_data
-- ----------------------------
INSERT INTO `sys_dict_data` VALUES (1160061112075464705, 1160061077912858625, '男', '0', '', 0, 1067246875800000001, '2022-11-14 21:13:57', 1067246875800000001, '2022-11-14 21:13:57');
INSERT INTO `sys_dict_data` VALUES (1160061146967879681, 1160061077912858625, '女', '1', '', 1, 1067246875800000001, '2022-11-14 21:13:57', 1067246875800000001, '2022-11-14 21:13:57');
INSERT INTO `sys_dict_data` VALUES (1160061190127267841, 1160061077912858625, '保密', '2', '', 2, 1067246875800000001, '2022-11-14 21:13:57', 1067246875800000001, '2022-11-14 21:13:57');
INSERT INTO `sys_dict_data` VALUES (1225814069634195457, 1225813644059140097, '公告', '0', '', 0, 1067246875800000001, '2022-11-14 21:13:57', 1067246875800000001, '2022-11-14 21:13:57');
INSERT INTO `sys_dict_data` VALUES (1225814107559092225, 1225813644059140097, '会议', '1', '', 1, 1067246875800000001, '2022-11-14 21:13:57', 1067246875800000001, '2022-11-14 21:13:57');
INSERT INTO `sys_dict_data` VALUES (1225814271879340034, 1225813644059140097, '其他', '2', '', 2, 1067246875800000001, '2022-11-14 21:13:57', 1067246875800000001, '2022-11-14 21:13:57');

-- ----------------------------
-- Table structure for sys_dict_type
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_type`;
CREATE TABLE `sys_dict_type`  (
  `id` bigint NOT NULL COMMENT 'id',
  `dict_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '字典类型',
  `dict_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '字典名称',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
  `sort` int UNSIGNED NULL DEFAULT NULL COMMENT '排序',
  `creator` bigint NULL DEFAULT NULL COMMENT '创建者',
  `create_date` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `updater` bigint NULL DEFAULT NULL COMMENT '更新者',
  `update_date` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `dict_type`(`dict_type` ASC) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '字典类型' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_dict_type
-- ----------------------------
INSERT INTO `sys_dict_type` VALUES (1160061077912858625, 'gender', '性别', '', 0, 1067246875800000001, '2022-11-14 21:13:57', 1067246875800000001, '2022-11-14 21:13:57');
INSERT INTO `sys_dict_type` VALUES (1225813644059140097, 'notice_type', '站内通知-类型', '', 1, 1067246875800000001, '2022-11-14 21:13:57', 1067246875800000001, '2022-11-14 21:13:57');

-- ----------------------------
-- Table structure for sys_log_error
-- ----------------------------
DROP TABLE IF EXISTS `sys_log_error`;
CREATE TABLE `sys_log_error`  (
  `id` bigint NOT NULL COMMENT 'id',
  `request_uri` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '请求URI',
  `request_method` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '请求方式',
  `request_params` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '请求参数',
  `user_agent` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '用户代理',
  `ip` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '操作IP',
  `error_info` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '异常信息',
  `creator` bigint NULL DEFAULT NULL COMMENT '创建者',
  `create_date` datetime NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_create_date`(`create_date` ASC) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '异常日志' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_log_error
-- ----------------------------
INSERT INTO `sys_log_error` VALUES (1592768697380982785, '/renren-admin/newfilm/pageinfo', 'GET', '{\"input\":\"2\",\"pageNo\":\"1\",\"pageSize\":\"10\"}', 'PostmanRuntime/7.28.0', '0:0:0:0:0:0:0:1', 'org.springframework.web.method.annotation.MethodArgumentTypeMismatchException: Failed to convert value of type \'java.lang.String\' to required type \'java.lang.Long\'; nested exception is java.lang.NumberFormatException: For input string: \"pageinfo\"\r\n	at org.springframework.web.method.annotation.AbstractNamedValueMethodArgumentResolver.resolveArgument(AbstractNamedValueMethodArgumentResolver.java:133)\r\n	at org.springframework.web.method.support.HandlerMethodArgumentResolverComposite.resolveArgument(HandlerMethodArgumentResolverComposite.java:122)\r\n	at org.springframework.web.method.support.InvocableHandlerMethod.getMethodArgumentValues(InvocableHandlerMethod.java:179)\r\n	at org.springframework.web.method.support.InvocableHandlerMethod.invokeForRequest(InvocableHandlerMethod.java:146)\r\n	at org.springframework.web.servlet.mvc.method.annotation.ServletInvocableHandlerMethod.invokeAndHandle(ServletInvocableHandlerMethod.java:117)\r\n	at org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter.invokeHandlerMethod(RequestMappingHandlerAdapter.java:895)\r\n	at org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter.handleInternal(RequestMappingHandlerAdapter.java:808)\r\n	at org.springframework.web.servlet.mvc.method.AbstractHandlerMethodAdapter.handle(AbstractHandlerMethodAdapter.java:87)\r\n	at org.springframework.web.servlet.DispatcherServlet.doDispatch(DispatcherServlet.java:1071)\r\n	at org.springframework.web.servlet.DispatcherServlet.doService(DispatcherServlet.java:964)\r\n	at org.springframework.web.servlet.FrameworkServlet.processRequest(FrameworkServlet.java:1006)\r\n	at org.springframework.web.servlet.FrameworkServlet.doGet(FrameworkServlet.java:898)\r\n	at javax.servlet.http.HttpServlet.service(HttpServlet.java:670)\r\n	at org.springframework.web.servlet.FrameworkServlet.service(FrameworkServlet.java:883)\r\n	at javax.servlet.http.HttpServlet.service(HttpServlet.java:779)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:227)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:162)\r\n	at org.apache.tomcat.websocket.server.WsFilter.doFilter(WsFilter.java:53)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:189)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:162)\r\n	at org.apache.shiro.web.servlet.ProxiedFilterChain.doFilter(ProxiedFilterChain.java:61)\r\n	at org.apache.shiro.web.servlet.AdviceFilter.executeChain(AdviceFilter.java:108)\r\n	at org.apache.shiro.web.servlet.AdviceFilter.doFilterInternal(AdviceFilter.java:137)\r\n	at org.apache.shiro.web.servlet.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:154)\r\n	at org.apache.shiro.web.servlet.ProxiedFilterChain.doFilter(ProxiedFilterChain.java:66)\r\n	at org.apache.shiro.web.servlet.AdviceFilter.executeChain(AdviceFilter.java:108)\r\n	at org.apache.shiro.web.servlet.AdviceFilter.doFilterInternal(AdviceFilter.java:137)\r\n	at org.apache.shiro.web.servlet.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:154)\r\n	at org.apache.shiro.web.servlet.ProxiedFilterChain.doFilter(ProxiedFilterChain.java:66)\r\n	at org.apache.shiro.web.servlet.AbstractShiroFilter.executeChain(AbstractShiroFilter.java:458)\r\n	at org.apache.shiro.web.servlet.AbstractShiroFilter$1.call(AbstractShiroFilter.java:373)\r\n	at org.apache.shiro.subject.support.SubjectCallable.doCall(SubjectCallable.java:90)\r\n	at org.apache.shiro.subject.support.SubjectCallable.call(SubjectCallable.java:83)\r\n	at org.apache.shiro.subject.support.DelegatingSubject.execute(DelegatingSubject.java:387)\r\n	at org.apache.shiro.web.servlet.AbstractShiroFilter.doFilterInternal(AbstractShiroFilter.java:370)\r\n	at org.apache.shiro.web.servlet.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:154)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:189)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:162)\r\n	at io.renren.common.xss.XssFilter.doFilter(XssFilter.java:30)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:189)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:162)\r\n	at org.apache.shiro.web.servlet.ProxiedFilterChain.doFilter(ProxiedFilterChain.java:61)\r\n	at org.apache.shiro.web.servlet.AdviceFilter.executeChain(AdviceFilter.java:108)\r\n	at org.apache.shiro.web.servlet.AdviceFilter.doFilterInternal(AdviceFilter.java:137)\r\n	at org.apache.shiro.web.servlet.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:154)\r\n	at org.apache.shiro.web.servlet.ProxiedFilterChain.doFilter(ProxiedFilterChain.java:66)\r\n	at org.apache.shiro.web.servlet.AdviceFilter.executeChain(AdviceFilter.java:108)\r\n	at org.apache.shiro.web.servlet.AdviceFilter.doFilterInternal(AdviceFilter.java:137)\r\n	at org.apache.shiro.web.servlet.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:154)\r\n	at org.apache.shiro.web.servlet.ProxiedFilterChain.doFilter(ProxiedFilterChain.java:66)\r\n	at org.apache.shiro.web.servlet.AbstractShiroFilter.executeChain(AbstractShiroFilter.java:458)\r\n	at org.apache.shiro.web.servlet.AbstractShiroFilter$1.call(AbstractShiroFilter.java:373)\r\n	at org.apache.shiro.subject.support.SubjectCallable.doCall(SubjectCallable.java:90)\r\n	at org.apache.shiro.subject.support.SubjectCallable.call(SubjectCallable.java:83)\r\n	at org.apache.shiro.subject.support.DelegatingSubject.execute(DelegatingSubject.java:387)\r\n	at org.apache.shiro.web.servlet.AbstractShiroFilter.doFilterInternal(AbstractShiroFilter.java:370)\r\n	at org.apache.shiro.web.servlet.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:154)\r\n	at org.springframework.web.filter.DelegatingFilterProxy.invokeDelegate(DelegatingFilterProxy.java:354)\r\n	at org.springframework.web.filter.DelegatingFilterProxy.doFilter(DelegatingFilterProxy.java:267)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:189)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:162)\r\n	at org.springframework.web.filter.RequestContextFilter.doFilterInternal(RequestContextFilter.java:100)\r\n	at org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:117)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:189)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:162)\r\n	at org.springframework.web.filter.FormContentFilter.doFilterInternal(FormContentFilter.java:93)\r\n	at org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:117)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:189)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:162)\r\n	at org.springframework.web.filter.CharacterEncodingFilter.doFilterInternal(CharacterEncodingFilter.java:201)\r\n	at org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:117)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:189)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:162)\r\n	at org.apache.catalina.core.StandardWrapperValve.invoke(StandardWrapperValve.java:197)\r\n	at org.apache.catalina.core.StandardContextValve.invoke(StandardContextValve.java:97)\r\n	at org.apache.catalina.authenticator.AuthenticatorBase.invoke(AuthenticatorBase.java:541)\r\n	at org.apache.catalina.core.StandardHostValve.invoke(StandardHostValve.java:135)\r\n	at org.apache.catalina.valves.ErrorReportValve.invoke(ErrorReportValve.java:92)\r\n	at org.apache.catalina.core.StandardEngineValve.invoke(StandardEngineValve.java:78)\r\n	at org.apache.catalina.connector.CoyoteAdapter.service(CoyoteAdapter.java:360)\r\n	at org.apache.coyote.http11.Http11Processor.service(Http11Processor.java:399)\r\n	at org.apache.coyote.AbstractProcessorLight.process(AbstractProcessorLight.java:65)\r\n	at org.apache.coyote.AbstractProtocol$ConnectionHandler.process(AbstractProtocol.java:893)\r\n	at org.apache.tomcat.util.net.NioEndpoint$SocketProcessor.doRun(NioEndpoint.java:1789)\r\n	at org.apache.tomcat.util.net.SocketProcessorBase.run(SocketProcessorBase.java:49)\r\n	at org.apache.tomcat.util.threads.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1191)\r\n	at org.apache.tomcat.util.threads.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:659)\r\n	at org.apache.tomcat.util.threads.TaskThread$WrappingRunnable.run(TaskThread.java:61)\r\n	at java.lang.Thread.run(Thread.java:745)\r\nCaused by: java.lang.NumberFormatException: For input string: \"pageinfo\"\r\n	at java.lang.NumberFormatException.forInputString(NumberFormatException.java:65)\r\n	at java.lang.Long.parseLong(Long.java:589)\r\n	at java.lang.Long.valueOf(Long.java:803)\r\n	at org.springframework.util.NumberUtils.parseNumber(NumberUtils.java:214)\r\n	at org.springframework.beans.propertyeditors.CustomNumberEditor.setAsText(CustomNumberEditor.java:115)\r\n	at org.springframework.beans.TypeConverterDelegate.doConvertTextValue(TypeConverterDelegate.java:429)\r\n	at org.springframework.beans.TypeConverterDelegate.doConvertValue(TypeConverterDelegate.java:402)\r\n	at org.springframework.beans.TypeConverterDelegate.convertIfNecessary(TypeConverterDelegate.java:155)\r\n	at org.springframework.beans.TypeConverterSupport.convertIfNecessary(TypeConverterSupport.java:73)\r\n	at org.springframework.beans.TypeConverterSupport.convertIfNecessary(TypeConverterSupport.java:53)\r\n	at org.springframework.validation.DataBinder.convertIfNecessary(DataBinder.java:729)\r\n	at org.springframework.web.method.annotation.AbstractNamedValueMethodArgumentResolver.resolveArgument(AbstractNamedValueMethodArgumentResolver.java:125)\r\n	... 88 more\r\n', 1067246875800000001, '2022-11-16 14:37:04');
INSERT INTO `sys_log_error` VALUES (1592769550842155009, '/renren-admin/newfilm/pageinfo', 'GET', '{\"input\":\"2\",\"pageNo\":\"1\",\"pageSize\":\"10\"}', 'PostmanRuntime/7.28.0', '0:0:0:0:0:0:0:1', 'org.springframework.web.method.annotation.MethodArgumentTypeMismatchException: Failed to convert value of type \'java.lang.String\' to required type \'java.lang.Long\'; nested exception is java.lang.NumberFormatException: For input string: \"pageinfo\"\r\n	at org.springframework.web.method.annotation.AbstractNamedValueMethodArgumentResolver.resolveArgument(AbstractNamedValueMethodArgumentResolver.java:133)\r\n	at org.springframework.web.method.support.HandlerMethodArgumentResolverComposite.resolveArgument(HandlerMethodArgumentResolverComposite.java:122)\r\n	at org.springframework.web.method.support.InvocableHandlerMethod.getMethodArgumentValues(InvocableHandlerMethod.java:179)\r\n	at org.springframework.web.method.support.InvocableHandlerMethod.invokeForRequest(InvocableHandlerMethod.java:146)\r\n	at org.springframework.web.servlet.mvc.method.annotation.ServletInvocableHandlerMethod.invokeAndHandle(ServletInvocableHandlerMethod.java:117)\r\n	at org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter.invokeHandlerMethod(RequestMappingHandlerAdapter.java:895)\r\n	at org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter.handleInternal(RequestMappingHandlerAdapter.java:808)\r\n	at org.springframework.web.servlet.mvc.method.AbstractHandlerMethodAdapter.handle(AbstractHandlerMethodAdapter.java:87)\r\n	at org.springframework.web.servlet.DispatcherServlet.doDispatch(DispatcherServlet.java:1071)\r\n	at org.springframework.web.servlet.DispatcherServlet.doService(DispatcherServlet.java:964)\r\n	at org.springframework.web.servlet.FrameworkServlet.processRequest(FrameworkServlet.java:1006)\r\n	at org.springframework.web.servlet.FrameworkServlet.doGet(FrameworkServlet.java:898)\r\n	at javax.servlet.http.HttpServlet.service(HttpServlet.java:670)\r\n	at org.springframework.web.servlet.FrameworkServlet.service(FrameworkServlet.java:883)\r\n	at javax.servlet.http.HttpServlet.service(HttpServlet.java:779)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:227)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:162)\r\n	at org.apache.tomcat.websocket.server.WsFilter.doFilter(WsFilter.java:53)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:189)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:162)\r\n	at org.apache.shiro.web.servlet.ProxiedFilterChain.doFilter(ProxiedFilterChain.java:61)\r\n	at org.apache.shiro.web.servlet.AdviceFilter.executeChain(AdviceFilter.java:108)\r\n	at org.apache.shiro.web.servlet.AdviceFilter.doFilterInternal(AdviceFilter.java:137)\r\n	at org.apache.shiro.web.servlet.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:154)\r\n	at org.apache.shiro.web.servlet.ProxiedFilterChain.doFilter(ProxiedFilterChain.java:66)\r\n	at org.apache.shiro.web.servlet.AdviceFilter.executeChain(AdviceFilter.java:108)\r\n	at org.apache.shiro.web.servlet.AdviceFilter.doFilterInternal(AdviceFilter.java:137)\r\n	at org.apache.shiro.web.servlet.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:154)\r\n	at org.apache.shiro.web.servlet.ProxiedFilterChain.doFilter(ProxiedFilterChain.java:66)\r\n	at org.apache.shiro.web.servlet.AbstractShiroFilter.executeChain(AbstractShiroFilter.java:458)\r\n	at org.apache.shiro.web.servlet.AbstractShiroFilter$1.call(AbstractShiroFilter.java:373)\r\n	at org.apache.shiro.subject.support.SubjectCallable.doCall(SubjectCallable.java:90)\r\n	at org.apache.shiro.subject.support.SubjectCallable.call(SubjectCallable.java:83)\r\n	at org.apache.shiro.subject.support.DelegatingSubject.execute(DelegatingSubject.java:387)\r\n	at org.apache.shiro.web.servlet.AbstractShiroFilter.doFilterInternal(AbstractShiroFilter.java:370)\r\n	at org.apache.shiro.web.servlet.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:154)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:189)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:162)\r\n	at io.renren.common.xss.XssFilter.doFilter(XssFilter.java:30)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:189)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:162)\r\n	at org.apache.shiro.web.servlet.ProxiedFilterChain.doFilter(ProxiedFilterChain.java:61)\r\n	at org.apache.shiro.web.servlet.AdviceFilter.executeChain(AdviceFilter.java:108)\r\n	at org.apache.shiro.web.servlet.AdviceFilter.doFilterInternal(AdviceFilter.java:137)\r\n	at org.apache.shiro.web.servlet.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:154)\r\n	at org.apache.shiro.web.servlet.ProxiedFilterChain.doFilter(ProxiedFilterChain.java:66)\r\n	at org.apache.shiro.web.servlet.AdviceFilter.executeChain(AdviceFilter.java:108)\r\n	at org.apache.shiro.web.servlet.AdviceFilter.doFilterInternal(AdviceFilter.java:137)\r\n	at org.apache.shiro.web.servlet.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:154)\r\n	at org.apache.shiro.web.servlet.ProxiedFilterChain.doFilter(ProxiedFilterChain.java:66)\r\n	at org.apache.shiro.web.servlet.AbstractShiroFilter.executeChain(AbstractShiroFilter.java:458)\r\n	at org.apache.shiro.web.servlet.AbstractShiroFilter$1.call(AbstractShiroFilter.java:373)\r\n	at org.apache.shiro.subject.support.SubjectCallable.doCall(SubjectCallable.java:90)\r\n	at org.apache.shiro.subject.support.SubjectCallable.call(SubjectCallable.java:83)\r\n	at org.apache.shiro.subject.support.DelegatingSubject.execute(DelegatingSubject.java:387)\r\n	at org.apache.shiro.web.servlet.AbstractShiroFilter.doFilterInternal(AbstractShiroFilter.java:370)\r\n	at org.apache.shiro.web.servlet.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:154)\r\n	at org.springframework.web.filter.DelegatingFilterProxy.invokeDelegate(DelegatingFilterProxy.java:354)\r\n	at org.springframework.web.filter.DelegatingFilterProxy.doFilter(DelegatingFilterProxy.java:267)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:189)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:162)\r\n	at org.springframework.web.filter.RequestContextFilter.doFilterInternal(RequestContextFilter.java:100)\r\n	at org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:117)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:189)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:162)\r\n	at org.springframework.web.filter.FormContentFilter.doFilterInternal(FormContentFilter.java:93)\r\n	at org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:117)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:189)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:162)\r\n	at org.springframework.web.filter.CharacterEncodingFilter.doFilterInternal(CharacterEncodingFilter.java:201)\r\n	at org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:117)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:189)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:162)\r\n	at org.apache.catalina.core.StandardWrapperValve.invoke(StandardWrapperValve.java:197)\r\n	at org.apache.catalina.core.StandardContextValve.invoke(StandardContextValve.java:97)\r\n	at org.apache.catalina.authenticator.AuthenticatorBase.invoke(AuthenticatorBase.java:541)\r\n	at org.apache.catalina.core.StandardHostValve.invoke(StandardHostValve.java:135)\r\n	at org.apache.catalina.valves.ErrorReportValve.invoke(ErrorReportValve.java:92)\r\n	at org.apache.catalina.core.StandardEngineValve.invoke(StandardEngineValve.java:78)\r\n	at org.apache.catalina.connector.CoyoteAdapter.service(CoyoteAdapter.java:360)\r\n	at org.apache.coyote.http11.Http11Processor.service(Http11Processor.java:399)\r\n	at org.apache.coyote.AbstractProcessorLight.process(AbstractProcessorLight.java:65)\r\n	at org.apache.coyote.AbstractProtocol$ConnectionHandler.process(AbstractProtocol.java:893)\r\n	at org.apache.tomcat.util.net.NioEndpoint$SocketProcessor.doRun(NioEndpoint.java:1789)\r\n	at org.apache.tomcat.util.net.SocketProcessorBase.run(SocketProcessorBase.java:49)\r\n	at org.apache.tomcat.util.threads.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1191)\r\n	at org.apache.tomcat.util.threads.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:659)\r\n	at org.apache.tomcat.util.threads.TaskThread$WrappingRunnable.run(TaskThread.java:61)\r\n	at java.lang.Thread.run(Thread.java:745)\r\nCaused by: java.lang.NumberFormatException: For input string: \"pageinfo\"\r\n	at java.lang.NumberFormatException.forInputString(NumberFormatException.java:65)\r\n	at java.lang.Long.parseLong(Long.java:589)\r\n	at java.lang.Long.valueOf(Long.java:803)\r\n	at org.springframework.util.NumberUtils.parseNumber(NumberUtils.java:214)\r\n	at org.springframework.beans.propertyeditors.CustomNumberEditor.setAsText(CustomNumberEditor.java:115)\r\n	at org.springframework.beans.TypeConverterDelegate.doConvertTextValue(TypeConverterDelegate.java:429)\r\n	at org.springframework.beans.TypeConverterDelegate.doConvertValue(TypeConverterDelegate.java:402)\r\n	at org.springframework.beans.TypeConverterDelegate.convertIfNecessary(TypeConverterDelegate.java:155)\r\n	at org.springframework.beans.TypeConverterSupport.convertIfNecessary(TypeConverterSupport.java:73)\r\n	at org.springframework.beans.TypeConverterSupport.convertIfNecessary(TypeConverterSupport.java:53)\r\n	at org.springframework.validation.DataBinder.convertIfNecessary(DataBinder.java:729)\r\n	at org.springframework.web.method.annotation.AbstractNamedValueMethodArgumentResolver.resolveArgument(AbstractNamedValueMethodArgumentResolver.java:125)\r\n	... 88 more\r\n', 1067246875800000001, '2022-11-16 14:40:28');
INSERT INTO `sys_log_error` VALUES (1592769694534782978, '/renren-admin/newfilm/pageinfo', 'GET', '{\"input\":\"2\",\"pageNo\":\"1\",\"pageSize\":\"10\"}', 'PostmanRuntime/7.28.0', '0:0:0:0:0:0:0:1', 'org.springframework.web.method.annotation.MethodArgumentTypeMismatchException: Failed to convert value of type \'java.lang.String\' to required type \'java.lang.Long\'; nested exception is java.lang.NumberFormatException: For input string: \"pageinfo\"\r\n	at org.springframework.web.method.annotation.AbstractNamedValueMethodArgumentResolver.resolveArgument(AbstractNamedValueMethodArgumentResolver.java:133)\r\n	at org.springframework.web.method.support.HandlerMethodArgumentResolverComposite.resolveArgument(HandlerMethodArgumentResolverComposite.java:122)\r\n	at org.springframework.web.method.support.InvocableHandlerMethod.getMethodArgumentValues(InvocableHandlerMethod.java:179)\r\n	at org.springframework.web.method.support.InvocableHandlerMethod.invokeForRequest(InvocableHandlerMethod.java:146)\r\n	at org.springframework.web.servlet.mvc.method.annotation.ServletInvocableHandlerMethod.invokeAndHandle(ServletInvocableHandlerMethod.java:117)\r\n	at org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter.invokeHandlerMethod(RequestMappingHandlerAdapter.java:895)\r\n	at org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter.handleInternal(RequestMappingHandlerAdapter.java:808)\r\n	at org.springframework.web.servlet.mvc.method.AbstractHandlerMethodAdapter.handle(AbstractHandlerMethodAdapter.java:87)\r\n	at org.springframework.web.servlet.DispatcherServlet.doDispatch(DispatcherServlet.java:1071)\r\n	at org.springframework.web.servlet.DispatcherServlet.doService(DispatcherServlet.java:964)\r\n	at org.springframework.web.servlet.FrameworkServlet.processRequest(FrameworkServlet.java:1006)\r\n	at org.springframework.web.servlet.FrameworkServlet.doGet(FrameworkServlet.java:898)\r\n	at javax.servlet.http.HttpServlet.service(HttpServlet.java:670)\r\n	at org.springframework.web.servlet.FrameworkServlet.service(FrameworkServlet.java:883)\r\n	at javax.servlet.http.HttpServlet.service(HttpServlet.java:779)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:227)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:162)\r\n	at org.apache.tomcat.websocket.server.WsFilter.doFilter(WsFilter.java:53)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:189)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:162)\r\n	at org.apache.shiro.web.servlet.ProxiedFilterChain.doFilter(ProxiedFilterChain.java:61)\r\n	at org.apache.shiro.web.servlet.AdviceFilter.executeChain(AdviceFilter.java:108)\r\n	at org.apache.shiro.web.servlet.AdviceFilter.doFilterInternal(AdviceFilter.java:137)\r\n	at org.apache.shiro.web.servlet.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:154)\r\n	at org.apache.shiro.web.servlet.ProxiedFilterChain.doFilter(ProxiedFilterChain.java:66)\r\n	at org.apache.shiro.web.servlet.AdviceFilter.executeChain(AdviceFilter.java:108)\r\n	at org.apache.shiro.web.servlet.AdviceFilter.doFilterInternal(AdviceFilter.java:137)\r\n	at org.apache.shiro.web.servlet.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:154)\r\n	at org.apache.shiro.web.servlet.ProxiedFilterChain.doFilter(ProxiedFilterChain.java:66)\r\n	at org.apache.shiro.web.servlet.AbstractShiroFilter.executeChain(AbstractShiroFilter.java:458)\r\n	at org.apache.shiro.web.servlet.AbstractShiroFilter$1.call(AbstractShiroFilter.java:373)\r\n	at org.apache.shiro.subject.support.SubjectCallable.doCall(SubjectCallable.java:90)\r\n	at org.apache.shiro.subject.support.SubjectCallable.call(SubjectCallable.java:83)\r\n	at org.apache.shiro.subject.support.DelegatingSubject.execute(DelegatingSubject.java:387)\r\n	at org.apache.shiro.web.servlet.AbstractShiroFilter.doFilterInternal(AbstractShiroFilter.java:370)\r\n	at org.apache.shiro.web.servlet.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:154)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:189)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:162)\r\n	at io.renren.common.xss.XssFilter.doFilter(XssFilter.java:30)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:189)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:162)\r\n	at org.apache.shiro.web.servlet.ProxiedFilterChain.doFilter(ProxiedFilterChain.java:61)\r\n	at org.apache.shiro.web.servlet.AdviceFilter.executeChain(AdviceFilter.java:108)\r\n	at org.apache.shiro.web.servlet.AdviceFilter.doFilterInternal(AdviceFilter.java:137)\r\n	at org.apache.shiro.web.servlet.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:154)\r\n	at org.apache.shiro.web.servlet.ProxiedFilterChain.doFilter(ProxiedFilterChain.java:66)\r\n	at org.apache.shiro.web.servlet.AdviceFilter.executeChain(AdviceFilter.java:108)\r\n	at org.apache.shiro.web.servlet.AdviceFilter.doFilterInternal(AdviceFilter.java:137)\r\n	at org.apache.shiro.web.servlet.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:154)\r\n	at org.apache.shiro.web.servlet.ProxiedFilterChain.doFilter(ProxiedFilterChain.java:66)\r\n	at org.apache.shiro.web.servlet.AbstractShiroFilter.executeChain(AbstractShiroFilter.java:458)\r\n	at org.apache.shiro.web.servlet.AbstractShiroFilter$1.call(AbstractShiroFilter.java:373)\r\n	at org.apache.shiro.subject.support.SubjectCallable.doCall(SubjectCallable.java:90)\r\n	at org.apache.shiro.subject.support.SubjectCallable.call(SubjectCallable.java:83)\r\n	at org.apache.shiro.subject.support.DelegatingSubject.execute(DelegatingSubject.java:387)\r\n	at org.apache.shiro.web.servlet.AbstractShiroFilter.doFilterInternal(AbstractShiroFilter.java:370)\r\n	at org.apache.shiro.web.servlet.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:154)\r\n	at org.springframework.web.filter.DelegatingFilterProxy.invokeDelegate(DelegatingFilterProxy.java:354)\r\n	at org.springframework.web.filter.DelegatingFilterProxy.doFilter(DelegatingFilterProxy.java:267)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:189)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:162)\r\n	at org.springframework.web.filter.RequestContextFilter.doFilterInternal(RequestContextFilter.java:100)\r\n	at org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:117)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:189)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:162)\r\n	at org.springframework.web.filter.FormContentFilter.doFilterInternal(FormContentFilter.java:93)\r\n	at org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:117)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:189)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:162)\r\n	at org.springframework.web.filter.CharacterEncodingFilter.doFilterInternal(CharacterEncodingFilter.java:201)\r\n	at org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:117)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:189)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:162)\r\n	at org.apache.catalina.core.StandardWrapperValve.invoke(StandardWrapperValve.java:197)\r\n	at org.apache.catalina.core.StandardContextValve.invoke(StandardContextValve.java:97)\r\n	at org.apache.catalina.authenticator.AuthenticatorBase.invoke(AuthenticatorBase.java:541)\r\n	at org.apache.catalina.core.StandardHostValve.invoke(StandardHostValve.java:135)\r\n	at org.apache.catalina.valves.ErrorReportValve.invoke(ErrorReportValve.java:92)\r\n	at org.apache.catalina.core.StandardEngineValve.invoke(StandardEngineValve.java:78)\r\n	at org.apache.catalina.connector.CoyoteAdapter.service(CoyoteAdapter.java:360)\r\n	at org.apache.coyote.http11.Http11Processor.service(Http11Processor.java:399)\r\n	at org.apache.coyote.AbstractProcessorLight.process(AbstractProcessorLight.java:65)\r\n	at org.apache.coyote.AbstractProtocol$ConnectionHandler.process(AbstractProtocol.java:893)\r\n	at org.apache.tomcat.util.net.NioEndpoint$SocketProcessor.doRun(NioEndpoint.java:1789)\r\n	at org.apache.tomcat.util.net.SocketProcessorBase.run(SocketProcessorBase.java:49)\r\n	at org.apache.tomcat.util.threads.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1191)\r\n	at org.apache.tomcat.util.threads.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:659)\r\n	at org.apache.tomcat.util.threads.TaskThread$WrappingRunnable.run(TaskThread.java:61)\r\n	at java.lang.Thread.run(Thread.java:745)\r\nCaused by: java.lang.NumberFormatException: For input string: \"pageinfo\"\r\n	at java.lang.NumberFormatException.forInputString(NumberFormatException.java:65)\r\n	at java.lang.Long.parseLong(Long.java:589)\r\n	at java.lang.Long.valueOf(Long.java:803)\r\n	at org.springframework.util.NumberUtils.parseNumber(NumberUtils.java:214)\r\n	at org.springframework.beans.propertyeditors.CustomNumberEditor.setAsText(CustomNumberEditor.java:115)\r\n	at org.springframework.beans.TypeConverterDelegate.doConvertTextValue(TypeConverterDelegate.java:429)\r\n	at org.springframework.beans.TypeConverterDelegate.doConvertValue(TypeConverterDelegate.java:402)\r\n	at org.springframework.beans.TypeConverterDelegate.convertIfNecessary(TypeConverterDelegate.java:155)\r\n	at org.springframework.beans.TypeConverterSupport.convertIfNecessary(TypeConverterSupport.java:73)\r\n	at org.springframework.beans.TypeConverterSupport.convertIfNecessary(TypeConverterSupport.java:53)\r\n	at org.springframework.validation.DataBinder.convertIfNecessary(DataBinder.java:729)\r\n	at org.springframework.web.method.annotation.AbstractNamedValueMethodArgumentResolver.resolveArgument(AbstractNamedValueMethodArgumentResolver.java:125)\r\n	... 88 more\r\n', 1067246875800000001, '2022-11-16 14:41:02');
INSERT INTO `sys_log_error` VALUES (1592770652404781058, '/renren-admin/newfilm/pageinfo', 'GET', '{\"input\":\"2\",\"pageNo\":\"1\",\"pageSize\":\"10\"}', 'PostmanRuntime/7.28.0', '0:0:0:0:0:0:0:1', 'org.springframework.web.method.annotation.MethodArgumentTypeMismatchException: Failed to convert value of type \'java.lang.String\' to required type \'java.lang.Long\'; nested exception is java.lang.NumberFormatException: For input string: \"pageinfo\"\r\n	at org.springframework.web.method.annotation.AbstractNamedValueMethodArgumentResolver.resolveArgument(AbstractNamedValueMethodArgumentResolver.java:133)\r\n	at org.springframework.web.method.support.HandlerMethodArgumentResolverComposite.resolveArgument(HandlerMethodArgumentResolverComposite.java:122)\r\n	at org.springframework.web.method.support.InvocableHandlerMethod.getMethodArgumentValues(InvocableHandlerMethod.java:179)\r\n	at org.springframework.web.method.support.InvocableHandlerMethod.invokeForRequest(InvocableHandlerMethod.java:146)\r\n	at org.springframework.web.servlet.mvc.method.annotation.ServletInvocableHandlerMethod.invokeAndHandle(ServletInvocableHandlerMethod.java:117)\r\n	at org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter.invokeHandlerMethod(RequestMappingHandlerAdapter.java:895)\r\n	at org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter.handleInternal(RequestMappingHandlerAdapter.java:808)\r\n	at org.springframework.web.servlet.mvc.method.AbstractHandlerMethodAdapter.handle(AbstractHandlerMethodAdapter.java:87)\r\n	at org.springframework.web.servlet.DispatcherServlet.doDispatch(DispatcherServlet.java:1071)\r\n	at org.springframework.web.servlet.DispatcherServlet.doService(DispatcherServlet.java:964)\r\n	at org.springframework.web.servlet.FrameworkServlet.processRequest(FrameworkServlet.java:1006)\r\n	at org.springframework.web.servlet.FrameworkServlet.doGet(FrameworkServlet.java:898)\r\n	at javax.servlet.http.HttpServlet.service(HttpServlet.java:670)\r\n	at org.springframework.web.servlet.FrameworkServlet.service(FrameworkServlet.java:883)\r\n	at javax.servlet.http.HttpServlet.service(HttpServlet.java:779)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:227)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:162)\r\n	at org.apache.tomcat.websocket.server.WsFilter.doFilter(WsFilter.java:53)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:189)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:162)\r\n	at org.apache.shiro.web.servlet.ProxiedFilterChain.doFilter(ProxiedFilterChain.java:61)\r\n	at org.apache.shiro.web.servlet.AdviceFilter.executeChain(AdviceFilter.java:108)\r\n	at org.apache.shiro.web.servlet.AdviceFilter.doFilterInternal(AdviceFilter.java:137)\r\n	at org.apache.shiro.web.servlet.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:154)\r\n	at org.apache.shiro.web.servlet.ProxiedFilterChain.doFilter(ProxiedFilterChain.java:66)\r\n	at org.apache.shiro.web.servlet.AdviceFilter.executeChain(AdviceFilter.java:108)\r\n	at org.apache.shiro.web.servlet.AdviceFilter.doFilterInternal(AdviceFilter.java:137)\r\n	at org.apache.shiro.web.servlet.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:154)\r\n	at org.apache.shiro.web.servlet.ProxiedFilterChain.doFilter(ProxiedFilterChain.java:66)\r\n	at org.apache.shiro.web.servlet.AbstractShiroFilter.executeChain(AbstractShiroFilter.java:458)\r\n	at org.apache.shiro.web.servlet.AbstractShiroFilter$1.call(AbstractShiroFilter.java:373)\r\n	at org.apache.shiro.subject.support.SubjectCallable.doCall(SubjectCallable.java:90)\r\n	at org.apache.shiro.subject.support.SubjectCallable.call(SubjectCallable.java:83)\r\n	at org.apache.shiro.subject.support.DelegatingSubject.execute(DelegatingSubject.java:387)\r\n	at org.apache.shiro.web.servlet.AbstractShiroFilter.doFilterInternal(AbstractShiroFilter.java:370)\r\n	at org.apache.shiro.web.servlet.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:154)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:189)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:162)\r\n	at io.renren.common.xss.XssFilter.doFilter(XssFilter.java:30)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:189)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:162)\r\n	at org.apache.shiro.web.servlet.ProxiedFilterChain.doFilter(ProxiedFilterChain.java:61)\r\n	at org.apache.shiro.web.servlet.AdviceFilter.executeChain(AdviceFilter.java:108)\r\n	at org.apache.shiro.web.servlet.AdviceFilter.doFilterInternal(AdviceFilter.java:137)\r\n	at org.apache.shiro.web.servlet.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:154)\r\n	at org.apache.shiro.web.servlet.ProxiedFilterChain.doFilter(ProxiedFilterChain.java:66)\r\n	at org.apache.shiro.web.servlet.AdviceFilter.executeChain(AdviceFilter.java:108)\r\n	at org.apache.shiro.web.servlet.AdviceFilter.doFilterInternal(AdviceFilter.java:137)\r\n	at org.apache.shiro.web.servlet.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:154)\r\n	at org.apache.shiro.web.servlet.ProxiedFilterChain.doFilter(ProxiedFilterChain.java:66)\r\n	at org.apache.shiro.web.servlet.AbstractShiroFilter.executeChain(AbstractShiroFilter.java:458)\r\n	at org.apache.shiro.web.servlet.AbstractShiroFilter$1.call(AbstractShiroFilter.java:373)\r\n	at org.apache.shiro.subject.support.SubjectCallable.doCall(SubjectCallable.java:90)\r\n	at org.apache.shiro.subject.support.SubjectCallable.call(SubjectCallable.java:83)\r\n	at org.apache.shiro.subject.support.DelegatingSubject.execute(DelegatingSubject.java:387)\r\n	at org.apache.shiro.web.servlet.AbstractShiroFilter.doFilterInternal(AbstractShiroFilter.java:370)\r\n	at org.apache.shiro.web.servlet.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:154)\r\n	at org.springframework.web.filter.DelegatingFilterProxy.invokeDelegate(DelegatingFilterProxy.java:354)\r\n	at org.springframework.web.filter.DelegatingFilterProxy.doFilter(DelegatingFilterProxy.java:267)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:189)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:162)\r\n	at org.springframework.web.filter.RequestContextFilter.doFilterInternal(RequestContextFilter.java:100)\r\n	at org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:117)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:189)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:162)\r\n	at org.springframework.web.filter.FormContentFilter.doFilterInternal(FormContentFilter.java:93)\r\n	at org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:117)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:189)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:162)\r\n	at org.springframework.web.filter.CharacterEncodingFilter.doFilterInternal(CharacterEncodingFilter.java:201)\r\n	at org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:117)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:189)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:162)\r\n	at org.apache.catalina.core.StandardWrapperValve.invoke(StandardWrapperValve.java:197)\r\n	at org.apache.catalina.core.StandardContextValve.invoke(StandardContextValve.java:97)\r\n	at org.apache.catalina.authenticator.AuthenticatorBase.invoke(AuthenticatorBase.java:541)\r\n	at org.apache.catalina.core.StandardHostValve.invoke(StandardHostValve.java:135)\r\n	at org.apache.catalina.valves.ErrorReportValve.invoke(ErrorReportValve.java:92)\r\n	at org.apache.catalina.core.StandardEngineValve.invoke(StandardEngineValve.java:78)\r\n	at org.apache.catalina.connector.CoyoteAdapter.service(CoyoteAdapter.java:360)\r\n	at org.apache.coyote.http11.Http11Processor.service(Http11Processor.java:399)\r\n	at org.apache.coyote.AbstractProcessorLight.process(AbstractProcessorLight.java:65)\r\n	at org.apache.coyote.AbstractProtocol$ConnectionHandler.process(AbstractProtocol.java:893)\r\n	at org.apache.tomcat.util.net.NioEndpoint$SocketProcessor.doRun(NioEndpoint.java:1789)\r\n	at org.apache.tomcat.util.net.SocketProcessorBase.run(SocketProcessorBase.java:49)\r\n	at org.apache.tomcat.util.threads.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1191)\r\n	at org.apache.tomcat.util.threads.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:659)\r\n	at org.apache.tomcat.util.threads.TaskThread$WrappingRunnable.run(TaskThread.java:61)\r\n	at java.lang.Thread.run(Thread.java:745)\r\nCaused by: java.lang.NumberFormatException: For input string: \"pageinfo\"\r\n	at java.lang.NumberFormatException.forInputString(NumberFormatException.java:65)\r\n	at java.lang.Long.parseLong(Long.java:589)\r\n	at java.lang.Long.valueOf(Long.java:803)\r\n	at org.springframework.util.NumberUtils.parseNumber(NumberUtils.java:214)\r\n	at org.springframework.beans.propertyeditors.CustomNumberEditor.setAsText(CustomNumberEditor.java:115)\r\n	at org.springframework.beans.TypeConverterDelegate.doConvertTextValue(TypeConverterDelegate.java:429)\r\n	at org.springframework.beans.TypeConverterDelegate.doConvertValue(TypeConverterDelegate.java:402)\r\n	at org.springframework.beans.TypeConverterDelegate.convertIfNecessary(TypeConverterDelegate.java:155)\r\n	at org.springframework.beans.TypeConverterSupport.convertIfNecessary(TypeConverterSupport.java:73)\r\n	at org.springframework.beans.TypeConverterSupport.convertIfNecessary(TypeConverterSupport.java:53)\r\n	at org.springframework.validation.DataBinder.convertIfNecessary(DataBinder.java:729)\r\n	at org.springframework.web.method.annotation.AbstractNamedValueMethodArgumentResolver.resolveArgument(AbstractNamedValueMethodArgumentResolver.java:125)\r\n	... 88 more\r\n', 1067246875800000001, '2022-11-16 14:44:50');
INSERT INTO `sys_log_error` VALUES (1592771163648479233, '/renren-admin/newfilm/pageinfo', 'GET', '{\"input\":\"2\",\"pageNo\":\"1\",\"pageSize\":\"10\"}', 'PostmanRuntime/7.28.0', '0:0:0:0:0:0:0:1', 'org.springframework.web.method.annotation.MethodArgumentTypeMismatchException: Failed to convert value of type \'java.lang.String\' to required type \'java.lang.Long\'; nested exception is java.lang.NumberFormatException: For input string: \"pageinfo\"\r\n	at org.springframework.web.method.annotation.AbstractNamedValueMethodArgumentResolver.resolveArgument(AbstractNamedValueMethodArgumentResolver.java:133)\r\n	at org.springframework.web.method.support.HandlerMethodArgumentResolverComposite.resolveArgument(HandlerMethodArgumentResolverComposite.java:122)\r\n	at org.springframework.web.method.support.InvocableHandlerMethod.getMethodArgumentValues(InvocableHandlerMethod.java:179)\r\n	at org.springframework.web.method.support.InvocableHandlerMethod.invokeForRequest(InvocableHandlerMethod.java:146)\r\n	at org.springframework.web.servlet.mvc.method.annotation.ServletInvocableHandlerMethod.invokeAndHandle(ServletInvocableHandlerMethod.java:117)\r\n	at org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter.invokeHandlerMethod(RequestMappingHandlerAdapter.java:895)\r\n	at org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter.handleInternal(RequestMappingHandlerAdapter.java:808)\r\n	at org.springframework.web.servlet.mvc.method.AbstractHandlerMethodAdapter.handle(AbstractHandlerMethodAdapter.java:87)\r\n	at org.springframework.web.servlet.DispatcherServlet.doDispatch(DispatcherServlet.java:1071)\r\n	at org.springframework.web.servlet.DispatcherServlet.doService(DispatcherServlet.java:964)\r\n	at org.springframework.web.servlet.FrameworkServlet.processRequest(FrameworkServlet.java:1006)\r\n	at org.springframework.web.servlet.FrameworkServlet.doGet(FrameworkServlet.java:898)\r\n	at javax.servlet.http.HttpServlet.service(HttpServlet.java:670)\r\n	at org.springframework.web.servlet.FrameworkServlet.service(FrameworkServlet.java:883)\r\n	at javax.servlet.http.HttpServlet.service(HttpServlet.java:779)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:227)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:162)\r\n	at org.apache.tomcat.websocket.server.WsFilter.doFilter(WsFilter.java:53)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:189)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:162)\r\n	at org.apache.shiro.web.servlet.ProxiedFilterChain.doFilter(ProxiedFilterChain.java:61)\r\n	at org.apache.shiro.web.servlet.AdviceFilter.executeChain(AdviceFilter.java:108)\r\n	at org.apache.shiro.web.servlet.AdviceFilter.doFilterInternal(AdviceFilter.java:137)\r\n	at org.apache.shiro.web.servlet.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:154)\r\n	at org.apache.shiro.web.servlet.ProxiedFilterChain.doFilter(ProxiedFilterChain.java:66)\r\n	at org.apache.shiro.web.servlet.AdviceFilter.executeChain(AdviceFilter.java:108)\r\n	at org.apache.shiro.web.servlet.AdviceFilter.doFilterInternal(AdviceFilter.java:137)\r\n	at org.apache.shiro.web.servlet.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:154)\r\n	at org.apache.shiro.web.servlet.ProxiedFilterChain.doFilter(ProxiedFilterChain.java:66)\r\n	at org.apache.shiro.web.servlet.AbstractShiroFilter.executeChain(AbstractShiroFilter.java:458)\r\n	at org.apache.shiro.web.servlet.AbstractShiroFilter$1.call(AbstractShiroFilter.java:373)\r\n	at org.apache.shiro.subject.support.SubjectCallable.doCall(SubjectCallable.java:90)\r\n	at org.apache.shiro.subject.support.SubjectCallable.call(SubjectCallable.java:83)\r\n	at org.apache.shiro.subject.support.DelegatingSubject.execute(DelegatingSubject.java:387)\r\n	at org.apache.shiro.web.servlet.AbstractShiroFilter.doFilterInternal(AbstractShiroFilter.java:370)\r\n	at org.apache.shiro.web.servlet.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:154)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:189)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:162)\r\n	at io.renren.common.xss.XssFilter.doFilter(XssFilter.java:30)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:189)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:162)\r\n	at org.apache.shiro.web.servlet.ProxiedFilterChain.doFilter(ProxiedFilterChain.java:61)\r\n	at org.apache.shiro.web.servlet.AdviceFilter.executeChain(AdviceFilter.java:108)\r\n	at org.apache.shiro.web.servlet.AdviceFilter.doFilterInternal(AdviceFilter.java:137)\r\n	at org.apache.shiro.web.servlet.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:154)\r\n	at org.apache.shiro.web.servlet.ProxiedFilterChain.doFilter(ProxiedFilterChain.java:66)\r\n	at org.apache.shiro.web.servlet.AdviceFilter.executeChain(AdviceFilter.java:108)\r\n	at org.apache.shiro.web.servlet.AdviceFilter.doFilterInternal(AdviceFilter.java:137)\r\n	at org.apache.shiro.web.servlet.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:154)\r\n	at org.apache.shiro.web.servlet.ProxiedFilterChain.doFilter(ProxiedFilterChain.java:66)\r\n	at org.apache.shiro.web.servlet.AbstractShiroFilter.executeChain(AbstractShiroFilter.java:458)\r\n	at org.apache.shiro.web.servlet.AbstractShiroFilter$1.call(AbstractShiroFilter.java:373)\r\n	at org.apache.shiro.subject.support.SubjectCallable.doCall(SubjectCallable.java:90)\r\n	at org.apache.shiro.subject.support.SubjectCallable.call(SubjectCallable.java:83)\r\n	at org.apache.shiro.subject.support.DelegatingSubject.execute(DelegatingSubject.java:387)\r\n	at org.apache.shiro.web.servlet.AbstractShiroFilter.doFilterInternal(AbstractShiroFilter.java:370)\r\n	at org.apache.shiro.web.servlet.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:154)\r\n	at org.springframework.web.filter.DelegatingFilterProxy.invokeDelegate(DelegatingFilterProxy.java:354)\r\n	at org.springframework.web.filter.DelegatingFilterProxy.doFilter(DelegatingFilterProxy.java:267)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:189)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:162)\r\n	at org.springframework.web.filter.RequestContextFilter.doFilterInternal(RequestContextFilter.java:100)\r\n	at org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:117)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:189)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:162)\r\n	at org.springframework.web.filter.FormContentFilter.doFilterInternal(FormContentFilter.java:93)\r\n	at org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:117)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:189)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:162)\r\n	at org.springframework.web.filter.CharacterEncodingFilter.doFilterInternal(CharacterEncodingFilter.java:201)\r\n	at org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:117)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:189)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:162)\r\n	at org.apache.catalina.core.StandardWrapperValve.invoke(StandardWrapperValve.java:197)\r\n	at org.apache.catalina.core.StandardContextValve.invoke(StandardContextValve.java:97)\r\n	at org.apache.catalina.authenticator.AuthenticatorBase.invoke(AuthenticatorBase.java:541)\r\n	at org.apache.catalina.core.StandardHostValve.invoke(StandardHostValve.java:135)\r\n	at org.apache.catalina.valves.ErrorReportValve.invoke(ErrorReportValve.java:92)\r\n	at org.apache.catalina.core.StandardEngineValve.invoke(StandardEngineValve.java:78)\r\n	at org.apache.catalina.connector.CoyoteAdapter.service(CoyoteAdapter.java:360)\r\n	at org.apache.coyote.http11.Http11Processor.service(Http11Processor.java:399)\r\n	at org.apache.coyote.AbstractProcessorLight.process(AbstractProcessorLight.java:65)\r\n	at org.apache.coyote.AbstractProtocol$ConnectionHandler.process(AbstractProtocol.java:893)\r\n	at org.apache.tomcat.util.net.NioEndpoint$SocketProcessor.doRun(NioEndpoint.java:1789)\r\n	at org.apache.tomcat.util.net.SocketProcessorBase.run(SocketProcessorBase.java:49)\r\n	at org.apache.tomcat.util.threads.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1191)\r\n	at org.apache.tomcat.util.threads.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:659)\r\n	at org.apache.tomcat.util.threads.TaskThread$WrappingRunnable.run(TaskThread.java:61)\r\n	at java.lang.Thread.run(Thread.java:745)\r\nCaused by: java.lang.NumberFormatException: For input string: \"pageinfo\"\r\n	at java.lang.NumberFormatException.forInputString(NumberFormatException.java:65)\r\n	at java.lang.Long.parseLong(Long.java:589)\r\n	at java.lang.Long.valueOf(Long.java:803)\r\n	at org.springframework.util.NumberUtils.parseNumber(NumberUtils.java:214)\r\n	at org.springframework.beans.propertyeditors.CustomNumberEditor.setAsText(CustomNumberEditor.java:115)\r\n	at org.springframework.beans.TypeConverterDelegate.doConvertTextValue(TypeConverterDelegate.java:429)\r\n	at org.springframework.beans.TypeConverterDelegate.doConvertValue(TypeConverterDelegate.java:402)\r\n	at org.springframework.beans.TypeConverterDelegate.convertIfNecessary(TypeConverterDelegate.java:155)\r\n	at org.springframework.beans.TypeConverterSupport.convertIfNecessary(TypeConverterSupport.java:73)\r\n	at org.springframework.beans.TypeConverterSupport.convertIfNecessary(TypeConverterSupport.java:53)\r\n	at org.springframework.validation.DataBinder.convertIfNecessary(DataBinder.java:729)\r\n	at org.springframework.web.method.annotation.AbstractNamedValueMethodArgumentResolver.resolveArgument(AbstractNamedValueMethodArgumentResolver.java:125)\r\n	... 88 more\r\n', 1067246875800000001, '2022-11-16 14:46:52');
INSERT INTO `sys_log_error` VALUES (1592772112127401986, '/renren-admin/newfilm/pageinfo', 'GET', '{\"input\":\"2\",\"pageNo\":\"1\",\"pageSize\":\"10\"}', 'PostmanRuntime/7.28.0', '0:0:0:0:0:0:0:1', 'org.springframework.web.method.annotation.MethodArgumentTypeMismatchException: Failed to convert value of type \'java.lang.String\' to required type \'java.lang.Long\'; nested exception is java.lang.NumberFormatException: For input string: \"pageinfo\"\r\n	at org.springframework.web.method.annotation.AbstractNamedValueMethodArgumentResolver.resolveArgument(AbstractNamedValueMethodArgumentResolver.java:133)\r\n	at org.springframework.web.method.support.HandlerMethodArgumentResolverComposite.resolveArgument(HandlerMethodArgumentResolverComposite.java:122)\r\n	at org.springframework.web.method.support.InvocableHandlerMethod.getMethodArgumentValues(InvocableHandlerMethod.java:179)\r\n	at org.springframework.web.method.support.InvocableHandlerMethod.invokeForRequest(InvocableHandlerMethod.java:146)\r\n	at org.springframework.web.servlet.mvc.method.annotation.ServletInvocableHandlerMethod.invokeAndHandle(ServletInvocableHandlerMethod.java:117)\r\n	at org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter.invokeHandlerMethod(RequestMappingHandlerAdapter.java:895)\r\n	at org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter.handleInternal(RequestMappingHandlerAdapter.java:808)\r\n	at org.springframework.web.servlet.mvc.method.AbstractHandlerMethodAdapter.handle(AbstractHandlerMethodAdapter.java:87)\r\n	at org.springframework.web.servlet.DispatcherServlet.doDispatch(DispatcherServlet.java:1071)\r\n	at org.springframework.web.servlet.DispatcherServlet.doService(DispatcherServlet.java:964)\r\n	at org.springframework.web.servlet.FrameworkServlet.processRequest(FrameworkServlet.java:1006)\r\n	at org.springframework.web.servlet.FrameworkServlet.doGet(FrameworkServlet.java:898)\r\n	at javax.servlet.http.HttpServlet.service(HttpServlet.java:670)\r\n	at org.springframework.web.servlet.FrameworkServlet.service(FrameworkServlet.java:883)\r\n	at javax.servlet.http.HttpServlet.service(HttpServlet.java:779)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:227)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:162)\r\n	at org.apache.tomcat.websocket.server.WsFilter.doFilter(WsFilter.java:53)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:189)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:162)\r\n	at org.apache.shiro.web.servlet.ProxiedFilterChain.doFilter(ProxiedFilterChain.java:61)\r\n	at org.apache.shiro.web.servlet.AdviceFilter.executeChain(AdviceFilter.java:108)\r\n	at org.apache.shiro.web.servlet.AdviceFilter.doFilterInternal(AdviceFilter.java:137)\r\n	at org.apache.shiro.web.servlet.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:154)\r\n	at org.apache.shiro.web.servlet.ProxiedFilterChain.doFilter(ProxiedFilterChain.java:66)\r\n	at org.apache.shiro.web.servlet.AdviceFilter.executeChain(AdviceFilter.java:108)\r\n	at org.apache.shiro.web.servlet.AdviceFilter.doFilterInternal(AdviceFilter.java:137)\r\n	at org.apache.shiro.web.servlet.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:154)\r\n	at org.apache.shiro.web.servlet.ProxiedFilterChain.doFilter(ProxiedFilterChain.java:66)\r\n	at org.apache.shiro.web.servlet.AbstractShiroFilter.executeChain(AbstractShiroFilter.java:458)\r\n	at org.apache.shiro.web.servlet.AbstractShiroFilter$1.call(AbstractShiroFilter.java:373)\r\n	at org.apache.shiro.subject.support.SubjectCallable.doCall(SubjectCallable.java:90)\r\n	at org.apache.shiro.subject.support.SubjectCallable.call(SubjectCallable.java:83)\r\n	at org.apache.shiro.subject.support.DelegatingSubject.execute(DelegatingSubject.java:387)\r\n	at org.apache.shiro.web.servlet.AbstractShiroFilter.doFilterInternal(AbstractShiroFilter.java:370)\r\n	at org.apache.shiro.web.servlet.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:154)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:189)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:162)\r\n	at io.renren.common.xss.XssFilter.doFilter(XssFilter.java:30)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:189)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:162)\r\n	at org.apache.shiro.web.servlet.ProxiedFilterChain.doFilter(ProxiedFilterChain.java:61)\r\n	at org.apache.shiro.web.servlet.AdviceFilter.executeChain(AdviceFilter.java:108)\r\n	at org.apache.shiro.web.servlet.AdviceFilter.doFilterInternal(AdviceFilter.java:137)\r\n	at org.apache.shiro.web.servlet.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:154)\r\n	at org.apache.shiro.web.servlet.ProxiedFilterChain.doFilter(ProxiedFilterChain.java:66)\r\n	at org.apache.shiro.web.servlet.AdviceFilter.executeChain(AdviceFilter.java:108)\r\n	at org.apache.shiro.web.servlet.AdviceFilter.doFilterInternal(AdviceFilter.java:137)\r\n	at org.apache.shiro.web.servlet.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:154)\r\n	at org.apache.shiro.web.servlet.ProxiedFilterChain.doFilter(ProxiedFilterChain.java:66)\r\n	at org.apache.shiro.web.servlet.AbstractShiroFilter.executeChain(AbstractShiroFilter.java:458)\r\n	at org.apache.shiro.web.servlet.AbstractShiroFilter$1.call(AbstractShiroFilter.java:373)\r\n	at org.apache.shiro.subject.support.SubjectCallable.doCall(SubjectCallable.java:90)\r\n	at org.apache.shiro.subject.support.SubjectCallable.call(SubjectCallable.java:83)\r\n	at org.apache.shiro.subject.support.DelegatingSubject.execute(DelegatingSubject.java:387)\r\n	at org.apache.shiro.web.servlet.AbstractShiroFilter.doFilterInternal(AbstractShiroFilter.java:370)\r\n	at org.apache.shiro.web.servlet.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:154)\r\n	at org.springframework.web.filter.DelegatingFilterProxy.invokeDelegate(DelegatingFilterProxy.java:354)\r\n	at org.springframework.web.filter.DelegatingFilterProxy.doFilter(DelegatingFilterProxy.java:267)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:189)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:162)\r\n	at org.springframework.web.filter.RequestContextFilter.doFilterInternal(RequestContextFilter.java:100)\r\n	at org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:117)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:189)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:162)\r\n	at org.springframework.web.filter.FormContentFilter.doFilterInternal(FormContentFilter.java:93)\r\n	at org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:117)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:189)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:162)\r\n	at org.springframework.web.filter.CharacterEncodingFilter.doFilterInternal(CharacterEncodingFilter.java:201)\r\n	at org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:117)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:189)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:162)\r\n	at org.apache.catalina.core.StandardWrapperValve.invoke(StandardWrapperValve.java:197)\r\n	at org.apache.catalina.core.StandardContextValve.invoke(StandardContextValve.java:97)\r\n	at org.apache.catalina.authenticator.AuthenticatorBase.invoke(AuthenticatorBase.java:541)\r\n	at org.apache.catalina.core.StandardHostValve.invoke(StandardHostValve.java:135)\r\n	at org.apache.catalina.valves.ErrorReportValve.invoke(ErrorReportValve.java:92)\r\n	at org.apache.catalina.core.StandardEngineValve.invoke(StandardEngineValve.java:78)\r\n	at org.apache.catalina.connector.CoyoteAdapter.service(CoyoteAdapter.java:360)\r\n	at org.apache.coyote.http11.Http11Processor.service(Http11Processor.java:399)\r\n	at org.apache.coyote.AbstractProcessorLight.process(AbstractProcessorLight.java:65)\r\n	at org.apache.coyote.AbstractProtocol$ConnectionHandler.process(AbstractProtocol.java:893)\r\n	at org.apache.tomcat.util.net.NioEndpoint$SocketProcessor.doRun(NioEndpoint.java:1789)\r\n	at org.apache.tomcat.util.net.SocketProcessorBase.run(SocketProcessorBase.java:49)\r\n	at org.apache.tomcat.util.threads.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1191)\r\n	at org.apache.tomcat.util.threads.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:659)\r\n	at org.apache.tomcat.util.threads.TaskThread$WrappingRunnable.run(TaskThread.java:61)\r\n	at java.lang.Thread.run(Thread.java:745)\r\nCaused by: java.lang.NumberFormatException: For input string: \"pageinfo\"\r\n	at java.lang.NumberFormatException.forInputString(NumberFormatException.java:65)\r\n	at java.lang.Long.parseLong(Long.java:589)\r\n	at java.lang.Long.valueOf(Long.java:803)\r\n	at org.springframework.util.NumberUtils.parseNumber(NumberUtils.java:214)\r\n	at org.springframework.beans.propertyeditors.CustomNumberEditor.setAsText(CustomNumberEditor.java:115)\r\n	at org.springframework.beans.TypeConverterDelegate.doConvertTextValue(TypeConverterDelegate.java:429)\r\n	at org.springframework.beans.TypeConverterDelegate.doConvertValue(TypeConverterDelegate.java:402)\r\n	at org.springframework.beans.TypeConverterDelegate.convertIfNecessary(TypeConverterDelegate.java:155)\r\n	at org.springframework.beans.TypeConverterSupport.convertIfNecessary(TypeConverterSupport.java:73)\r\n	at org.springframework.beans.TypeConverterSupport.convertIfNecessary(TypeConverterSupport.java:53)\r\n	at org.springframework.validation.DataBinder.convertIfNecessary(DataBinder.java:729)\r\n	at org.springframework.web.method.annotation.AbstractNamedValueMethodArgumentResolver.resolveArgument(AbstractNamedValueMethodArgumentResolver.java:125)\r\n	... 88 more\r\n', 1067246875800000001, '2022-11-16 14:50:38');
INSERT INTO `sys_log_error` VALUES (1593418512120999937, '/renren-admin/newcomment/pageInfo', 'GET', '{\"_t\":\"1668735552112\",\"pageNo\":\"1\",\"pageSize\":\"20\"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', '0:0:0:0:0:0:0:1', 'java.lang.NullPointerException\r\n	at io.renren.modules.newcomment.service.impl.NewcommentsServiceImpl.pageInfo(NewcommentsServiceImpl.java:58)\r\n	at io.renren.modules.newcomment.service.impl.NewcommentsServiceImpl$$FastClassBySpringCGLIB$$4a6a1694.invoke(<generated>)\r\n	at org.springframework.cglib.proxy.MethodProxy.invoke(MethodProxy.java:218)\r\n	at org.springframework.aop.framework.CglibAopProxy.invokeMethod(CglibAopProxy.java:386)\r\n	at org.springframework.aop.framework.CglibAopProxy.access$000(CglibAopProxy.java:85)\r\n	at org.springframework.aop.framework.CglibAopProxy$DynamicAdvisedInterceptor.intercept(CglibAopProxy.java:704)\r\n	at io.renren.modules.newcomment.service.impl.NewcommentsServiceImpl$$EnhancerBySpringCGLIB$$be0502ac.pageInfo(<generated>)\r\n	at io.renren.modules.newcomment.controller.NewcommentsController.pageInfo(NewcommentsController.java:47)\r\n	at io.renren.modules.newcomment.controller.NewcommentsController$$FastClassBySpringCGLIB$$577361c0.invoke(<generated>)\r\n	at org.springframework.cglib.proxy.MethodProxy.invoke(MethodProxy.java:218)\r\n	at org.springframework.aop.framework.CglibAopProxy$CglibMethodInvocation.invokeJoinpoint(CglibAopProxy.java:793)\r\n	at org.springframework.aop.framework.ReflectiveMethodInvocation.proceed(ReflectiveMethodInvocation.java:163)\r\n	at org.springframework.aop.framework.CglibAopProxy$CglibMethodInvocation.proceed(CglibAopProxy.java:763)\r\n	at org.springframework.aop.interceptor.ExposeInvocationInterceptor.invoke(ExposeInvocationInterceptor.java:97)\r\n	at org.springframework.aop.framework.ReflectiveMethodInvocation.proceed(ReflectiveMethodInvocation.java:186)\r\n	at org.springframework.aop.framework.CglibAopProxy$CglibMethodInvocation.proceed(CglibAopProxy.java:763)\r\n	at org.springframework.aop.framework.CglibAopProxy$DynamicAdvisedInterceptor.intercept(CglibAopProxy.java:708)\r\n	at io.renren.modules.newcomment.controller.NewcommentsController$$EnhancerBySpringCGLIB$$15cbba5c.pageInfo(<generated>)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n	at java.lang.reflect.Method.invoke(Method.java:483)\r\n	at org.springframework.web.method.support.InvocableHandlerMethod.doInvoke(InvocableHandlerMethod.java:205)\r\n	at org.springframework.web.method.support.InvocableHandlerMethod.invokeForRequest(InvocableHandlerMethod.java:150)\r\n	at org.springframework.web.servlet.mvc.method.annotation.ServletInvocableHandlerMethod.invokeAndHandle(ServletInvocableHandlerMethod.java:117)\r\n	at org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter.invokeHandlerMethod(RequestMappingHandlerAdapter.java:895)\r\n	at org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter.handleInternal(RequestMappingHandlerAdapter.java:808)\r\n	at org.springframework.web.servlet.mvc.method.AbstractHandlerMethodAdapter.handle(AbstractHandlerMethodAdapter.java:87)\r\n	at org.springframework.web.servlet.DispatcherServlet.doDispatch(DispatcherServlet.java:1071)\r\n	at org.springframework.web.servlet.DispatcherServlet.doService(DispatcherServlet.java:964)\r\n	at org.springframework.web.servlet.FrameworkServlet.processRequest(FrameworkServlet.java:1006)\r\n	at org.springframework.web.servlet.FrameworkServlet.doGet(FrameworkServlet.java:898)\r\n	at javax.servlet.http.HttpServlet.service(HttpServlet.java:670)\r\n	at org.springframework.web.servlet.FrameworkServlet.service(FrameworkServlet.java:883)\r\n	at javax.servlet.http.HttpServlet.service(HttpServlet.java:779)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:227)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:162)\r\n	at org.apache.tomcat.websocket.server.WsFilter.doFilter(WsFilter.java:53)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:189)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:162)\r\n	at org.apache.shiro.web.servlet.ProxiedFilterChain.doFilter(ProxiedFilterChain.java:61)\r\n	at org.apache.shiro.web.servlet.AdviceFilter.executeChain(AdviceFilter.java:108)\r\n	at org.apache.shiro.web.servlet.AdviceFilter.doFilterInternal(AdviceFilter.java:137)\r\n	at org.apache.shiro.web.servlet.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:154)\r\n	at org.apache.shiro.web.servlet.ProxiedFilterChain.doFilter(ProxiedFilterChain.java:66)\r\n	at org.apache.shiro.web.servlet.AdviceFilter.executeChain(AdviceFilter.java:108)\r\n	at org.apache.shiro.web.servlet.AdviceFilter.doFilterInternal(AdviceFilter.java:137)\r\n	at org.apache.shiro.web.servlet.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:154)\r\n	at org.apache.shiro.web.servlet.ProxiedFilterChain.doFilter(ProxiedFilterChain.java:66)\r\n	at org.apache.shiro.web.servlet.AbstractShiroFilter.executeChain(AbstractShiroFilter.java:458)\r\n	at org.apache.shiro.web.servlet.AbstractShiroFilter$1.call(AbstractShiroFilter.java:373)\r\n	at org.apache.shiro.subject.support.SubjectCallable.doCall(SubjectCallable.java:90)\r\n	at org.apache.shiro.subject.support.SubjectCallable.call(SubjectCallable.java:83)\r\n	at org.apache.shiro.subject.support.DelegatingSubject.execute(DelegatingSubject.java:387)\r\n	at org.apache.shiro.web.servlet.AbstractShiroFilter.doFilterInternal(AbstractShiroFilter.java:370)\r\n	at org.apache.shiro.web.servlet.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:154)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:189)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:162)\r\n	at io.renren.common.xss.XssFilter.doFilter(XssFilter.java:30)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:189)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:162)\r\n	at org.apache.shiro.web.servlet.ProxiedFilterChain.doFilter(ProxiedFilterChain.java:61)\r\n	at org.apache.shiro.web.servlet.AdviceFilter.executeChain(AdviceFilter.java:108)\r\n	at org.apache.shiro.web.servlet.AdviceFilter.doFilterInternal(AdviceFilter.java:137)\r\n	at org.apache.shiro.web.servlet.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:154)\r\n	at org.apache.shiro.web.servlet.ProxiedFilterChain.doFilter(ProxiedFilterChain.java:66)\r\n	at org.apache.shiro.web.servlet.AdviceFilter.executeChain(AdviceFilter.java:108)\r\n	at org.apache.shiro.web.servlet.AdviceFilter.doFilterInternal(AdviceFilter.java:137)\r\n	at org.apache.shiro.web.servlet.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:154)\r\n	at org.apache.shiro.web.servlet.ProxiedFilterChain.doFilter(ProxiedFilterChain.java:66)\r\n	at org.apache.shiro.web.servlet.AbstractShiroFilter.executeChain(AbstractShiroFilter.java:458)\r\n	at org.apache.shiro.web.servlet.AbstractShiroFilter$1.call(AbstractShiroFilter.java:373)\r\n	at org.apache.shiro.subject.support.SubjectCallable.doCall(SubjectCallable.java:90)\r\n	at org.apache.shiro.subject.support.SubjectCallable.call(SubjectCallable.java:83)\r\n	at org.apache.shiro.subject.support.DelegatingSubject.execute(DelegatingSubject.java:387)\r\n	at org.apache.shiro.web.servlet.AbstractShiroFilter.doFilterInternal(AbstractShiroFilter.java:370)\r\n	at org.apache.shiro.web.servlet.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:154)\r\n	at org.springframework.web.filter.DelegatingFilterProxy.invokeDelegate(DelegatingFilterProxy.java:354)\r\n	at org.springframework.web.filter.DelegatingFilterProxy.doFilter(DelegatingFilterProxy.java:267)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:189)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:162)\r\n	at org.springframework.web.filter.RequestContextFilter.doFilterInternal(RequestContextFilter.java:100)\r\n	at org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:117)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:189)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:162)\r\n	at org.springframework.web.filter.FormContentFilter.doFilterInternal(FormContentFilter.java:93)\r\n	at org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:117)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:189)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:162)\r\n	at org.springframework.web.filter.CharacterEncodingFilter.doFilterInternal(CharacterEncodingFilter.java:201)\r\n	at org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:117)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:189)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:162)\r\n	at org.apache.catalina.core.StandardWrapperValve.invoke(StandardWrapperValve.java:197)\r\n	at org.apache.catalina.core.StandardContextValve.invoke(StandardContextValve.java:97)\r\n	at org.apache.catalina.authenticator.AuthenticatorBase.invoke(AuthenticatorBase.java:541)\r\n	at org.apache.catalina.core.StandardHostValve.invoke(StandardHostValve.java:135)\r\n	at org.apache.catalina.valves.ErrorReportValve.invoke(ErrorReportValve.java:92)\r\n	at org.apache.catalina.core.StandardEngineValve.invoke(StandardEngineValve.java:78)\r\n	at org.apache.catalina.connector.CoyoteAdapter.service(CoyoteAdapter.java:360)\r\n	at org.apache.coyote.http11.Http11Processor.service(Http11Processor.java:399)\r\n	at org.apache.coyote.AbstractProcessorLight.process(AbstractProcessorLight.java:65)\r\n	at org.apache.coyote.AbstractProtocol$ConnectionHandler.process(AbstractProtocol.java:893)\r\n	at org.apache.tomcat.util.net.NioEndpoint$SocketProcessor.doRun(NioEndpoint.java:1789)\r\n	at org.apache.tomcat.util.net.SocketProcessorBase.run(SocketProcessorBase.java:49)\r\n	at org.apache.tomcat.util.threads.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1191)\r\n	at org.apache.tomcat.util.threads.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:659)\r\n	at org.apache.tomcat.util.threads.TaskThread$WrappingRunnable.run(TaskThread.java:61)\r\n	at java.lang.Thread.run(Thread.java:745)\r\n', 1067246875800000001, NULL);
INSERT INTO `sys_log_error` VALUES (1593418962094272513, '/renren-admin/newcomment/pageInfo', 'GET', '{\"_t\":\"1668735659360\",\"pageNo\":\"1\",\"pageSize\":\"20\"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', '0:0:0:0:0:0:0:1', 'java.lang.NullPointerException\r\n	at io.renren.modules.newcomment.service.impl.NewcommentsServiceImpl.pageInfo(NewcommentsServiceImpl.java:58)\r\n	at io.renren.modules.newcomment.service.impl.NewcommentsServiceImpl$$FastClassBySpringCGLIB$$4a6a1694.invoke(<generated>)\r\n	at org.springframework.cglib.proxy.MethodProxy.invoke(MethodProxy.java:218)\r\n	at org.springframework.aop.framework.CglibAopProxy.invokeMethod(CglibAopProxy.java:386)\r\n	at org.springframework.aop.framework.CglibAopProxy.access$000(CglibAopProxy.java:85)\r\n	at org.springframework.aop.framework.CglibAopProxy$DynamicAdvisedInterceptor.intercept(CglibAopProxy.java:704)\r\n	at io.renren.modules.newcomment.service.impl.NewcommentsServiceImpl$$EnhancerBySpringCGLIB$$db7a5c9e.pageInfo(<generated>)\r\n	at io.renren.modules.newcomment.controller.NewcommentsController.pageInfo(NewcommentsController.java:47)\r\n	at io.renren.modules.newcomment.controller.NewcommentsController$$FastClassBySpringCGLIB$$577361c0.invoke(<generated>)\r\n	at org.springframework.cglib.proxy.MethodProxy.invoke(MethodProxy.java:218)\r\n	at org.springframework.aop.framework.CglibAopProxy$CglibMethodInvocation.invokeJoinpoint(CglibAopProxy.java:793)\r\n	at org.springframework.aop.framework.ReflectiveMethodInvocation.proceed(ReflectiveMethodInvocation.java:163)\r\n	at org.springframework.aop.framework.CglibAopProxy$CglibMethodInvocation.proceed(CglibAopProxy.java:763)\r\n	at org.springframework.aop.interceptor.ExposeInvocationInterceptor.invoke(ExposeInvocationInterceptor.java:97)\r\n	at org.springframework.aop.framework.ReflectiveMethodInvocation.proceed(ReflectiveMethodInvocation.java:186)\r\n	at org.springframework.aop.framework.CglibAopProxy$CglibMethodInvocation.proceed(CglibAopProxy.java:763)\r\n	at org.springframework.aop.framework.CglibAopProxy$DynamicAdvisedInterceptor.intercept(CglibAopProxy.java:708)\r\n	at io.renren.modules.newcomment.controller.NewcommentsController$$EnhancerBySpringCGLIB$$15cbba5c.pageInfo(<generated>)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n	at java.lang.reflect.Method.invoke(Method.java:483)\r\n	at org.springframework.web.method.support.InvocableHandlerMethod.doInvoke(InvocableHandlerMethod.java:205)\r\n	at org.springframework.web.method.support.InvocableHandlerMethod.invokeForRequest(InvocableHandlerMethod.java:150)\r\n	at org.springframework.web.servlet.mvc.method.annotation.ServletInvocableHandlerMethod.invokeAndHandle(ServletInvocableHandlerMethod.java:117)\r\n	at org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter.invokeHandlerMethod(RequestMappingHandlerAdapter.java:895)\r\n	at org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter.handleInternal(RequestMappingHandlerAdapter.java:808)\r\n	at org.springframework.web.servlet.mvc.method.AbstractHandlerMethodAdapter.handle(AbstractHandlerMethodAdapter.java:87)\r\n	at org.springframework.web.servlet.DispatcherServlet.doDispatch(DispatcherServlet.java:1071)\r\n	at org.springframework.web.servlet.DispatcherServlet.doService(DispatcherServlet.java:964)\r\n	at org.springframework.web.servlet.FrameworkServlet.processRequest(FrameworkServlet.java:1006)\r\n	at org.springframework.web.servlet.FrameworkServlet.doGet(FrameworkServlet.java:898)\r\n	at javax.servlet.http.HttpServlet.service(HttpServlet.java:670)\r\n	at org.springframework.web.servlet.FrameworkServlet.service(FrameworkServlet.java:883)\r\n	at javax.servlet.http.HttpServlet.service(HttpServlet.java:779)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:227)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:162)\r\n	at org.apache.tomcat.websocket.server.WsFilter.doFilter(WsFilter.java:53)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:189)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:162)\r\n	at org.apache.shiro.web.servlet.ProxiedFilterChain.doFilter(ProxiedFilterChain.java:61)\r\n	at org.apache.shiro.web.servlet.AdviceFilter.executeChain(AdviceFilter.java:108)\r\n	at org.apache.shiro.web.servlet.AdviceFilter.doFilterInternal(AdviceFilter.java:137)\r\n	at org.apache.shiro.web.servlet.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:154)\r\n	at org.apache.shiro.web.servlet.ProxiedFilterChain.doFilter(ProxiedFilterChain.java:66)\r\n	at org.apache.shiro.web.servlet.AdviceFilter.executeChain(AdviceFilter.java:108)\r\n	at org.apache.shiro.web.servlet.AdviceFilter.doFilterInternal(AdviceFilter.java:137)\r\n	at org.apache.shiro.web.servlet.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:154)\r\n	at org.apache.shiro.web.servlet.ProxiedFilterChain.doFilter(ProxiedFilterChain.java:66)\r\n	at org.apache.shiro.web.servlet.AbstractShiroFilter.executeChain(AbstractShiroFilter.java:458)\r\n	at org.apache.shiro.web.servlet.AbstractShiroFilter$1.call(AbstractShiroFilter.java:373)\r\n	at org.apache.shiro.subject.support.SubjectCallable.doCall(SubjectCallable.java:90)\r\n	at org.apache.shiro.subject.support.SubjectCallable.call(SubjectCallable.java:83)\r\n	at org.apache.shiro.subject.support.DelegatingSubject.execute(DelegatingSubject.java:387)\r\n	at org.apache.shiro.web.servlet.AbstractShiroFilter.doFilterInternal(AbstractShiroFilter.java:370)\r\n	at org.apache.shiro.web.servlet.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:154)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:189)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:162)\r\n	at io.renren.common.xss.XssFilter.doFilter(XssFilter.java:30)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:189)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:162)\r\n	at org.apache.shiro.web.servlet.ProxiedFilterChain.doFilter(ProxiedFilterChain.java:61)\r\n	at org.apache.shiro.web.servlet.AdviceFilter.executeChain(AdviceFilter.java:108)\r\n	at org.apache.shiro.web.servlet.AdviceFilter.doFilterInternal(AdviceFilter.java:137)\r\n	at org.apache.shiro.web.servlet.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:154)\r\n	at org.apache.shiro.web.servlet.ProxiedFilterChain.doFilter(ProxiedFilterChain.java:66)\r\n	at org.apache.shiro.web.servlet.AdviceFilter.executeChain(AdviceFilter.java:108)\r\n	at org.apache.shiro.web.servlet.AdviceFilter.doFilterInternal(AdviceFilter.java:137)\r\n	at org.apache.shiro.web.servlet.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:154)\r\n	at org.apache.shiro.web.servlet.ProxiedFilterChain.doFilter(ProxiedFilterChain.java:66)\r\n	at org.apache.shiro.web.servlet.AbstractShiroFilter.executeChain(AbstractShiroFilter.java:458)\r\n	at org.apache.shiro.web.servlet.AbstractShiroFilter$1.call(AbstractShiroFilter.java:373)\r\n	at org.apache.shiro.subject.support.SubjectCallable.doCall(SubjectCallable.java:90)\r\n	at org.apache.shiro.subject.support.SubjectCallable.call(SubjectCallable.java:83)\r\n	at org.apache.shiro.subject.support.DelegatingSubject.execute(DelegatingSubject.java:387)\r\n	at org.apache.shiro.web.servlet.AbstractShiroFilter.doFilterInternal(AbstractShiroFilter.java:370)\r\n	at org.apache.shiro.web.servlet.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:154)\r\n	at org.springframework.web.filter.DelegatingFilterProxy.invokeDelegate(DelegatingFilterProxy.java:354)\r\n	at org.springframework.web.filter.DelegatingFilterProxy.doFilter(DelegatingFilterProxy.java:267)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:189)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:162)\r\n	at org.springframework.web.filter.RequestContextFilter.doFilterInternal(RequestContextFilter.java:100)\r\n	at org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:117)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:189)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:162)\r\n	at org.springframework.web.filter.FormContentFilter.doFilterInternal(FormContentFilter.java:93)\r\n	at org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:117)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:189)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:162)\r\n	at org.springframework.web.filter.CharacterEncodingFilter.doFilterInternal(CharacterEncodingFilter.java:201)\r\n	at org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:117)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:189)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:162)\r\n	at org.apache.catalina.core.StandardWrapperValve.invoke(StandardWrapperValve.java:197)\r\n	at org.apache.catalina.core.StandardContextValve.invoke(StandardContextValve.java:97)\r\n	at org.apache.catalina.authenticator.AuthenticatorBase.invoke(AuthenticatorBase.java:541)\r\n	at org.apache.catalina.core.StandardHostValve.invoke(StandardHostValve.java:135)\r\n	at org.apache.catalina.valves.ErrorReportValve.invoke(ErrorReportValve.java:92)\r\n	at org.apache.catalina.core.StandardEngineValve.invoke(StandardEngineValve.java:78)\r\n	at org.apache.catalina.connector.CoyoteAdapter.service(CoyoteAdapter.java:360)\r\n	at org.apache.coyote.http11.Http11Processor.service(Http11Processor.java:399)\r\n	at org.apache.coyote.AbstractProcessorLight.process(AbstractProcessorLight.java:65)\r\n	at org.apache.coyote.AbstractProtocol$ConnectionHandler.process(AbstractProtocol.java:893)\r\n	at org.apache.tomcat.util.net.NioEndpoint$SocketProcessor.doRun(NioEndpoint.java:1789)\r\n	at org.apache.tomcat.util.net.SocketProcessorBase.run(SocketProcessorBase.java:49)\r\n	at org.apache.tomcat.util.threads.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1191)\r\n	at org.apache.tomcat.util.threads.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:659)\r\n	at org.apache.tomcat.util.threads.TaskThread$WrappingRunnable.run(TaskThread.java:61)\r\n	at java.lang.Thread.run(Thread.java:745)\r\n', 1067246875800000001, NULL);
INSERT INTO `sys_log_error` VALUES (1593442796834959361, '/renren-admin/newcomment/numberlike', 'GET', '{\"_t\":\"1668741341981\",\"pageNo\":\"1\",\"filmid\":\"1\",\"pageSize\":\"20\"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', '0:0:0:0:0:0:0:1', 'org.springframework.web.method.annotation.MethodArgumentTypeMismatchException: Failed to convert value of type \'java.lang.String\' to required type \'java.lang.Long\'; nested exception is java.lang.NumberFormatException: For input string: \"numberlike\"\r\n	at org.springframework.web.method.annotation.AbstractNamedValueMethodArgumentResolver.resolveArgument(AbstractNamedValueMethodArgumentResolver.java:133)\r\n	at org.springframework.web.method.support.HandlerMethodArgumentResolverComposite.resolveArgument(HandlerMethodArgumentResolverComposite.java:122)\r\n	at org.springframework.web.method.support.InvocableHandlerMethod.getMethodArgumentValues(InvocableHandlerMethod.java:179)\r\n	at org.springframework.web.method.support.InvocableHandlerMethod.invokeForRequest(InvocableHandlerMethod.java:146)\r\n	at org.springframework.web.servlet.mvc.method.annotation.ServletInvocableHandlerMethod.invokeAndHandle(ServletInvocableHandlerMethod.java:117)\r\n	at org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter.invokeHandlerMethod(RequestMappingHandlerAdapter.java:895)\r\n	at org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter.handleInternal(RequestMappingHandlerAdapter.java:808)\r\n	at org.springframework.web.servlet.mvc.method.AbstractHandlerMethodAdapter.handle(AbstractHandlerMethodAdapter.java:87)\r\n	at org.springframework.web.servlet.DispatcherServlet.doDispatch(DispatcherServlet.java:1071)\r\n	at org.springframework.web.servlet.DispatcherServlet.doService(DispatcherServlet.java:964)\r\n	at org.springframework.web.servlet.FrameworkServlet.processRequest(FrameworkServlet.java:1006)\r\n	at org.springframework.web.servlet.FrameworkServlet.doGet(FrameworkServlet.java:898)\r\n	at javax.servlet.http.HttpServlet.service(HttpServlet.java:670)\r\n	at org.springframework.web.servlet.FrameworkServlet.service(FrameworkServlet.java:883)\r\n	at javax.servlet.http.HttpServlet.service(HttpServlet.java:779)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:227)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:162)\r\n	at org.apache.tomcat.websocket.server.WsFilter.doFilter(WsFilter.java:53)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:189)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:162)\r\n	at org.apache.shiro.web.servlet.ProxiedFilterChain.doFilter(ProxiedFilterChain.java:61)\r\n	at org.apache.shiro.web.servlet.AdviceFilter.executeChain(AdviceFilter.java:108)\r\n	at org.apache.shiro.web.servlet.AdviceFilter.doFilterInternal(AdviceFilter.java:137)\r\n	at org.apache.shiro.web.servlet.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:154)\r\n	at org.apache.shiro.web.servlet.ProxiedFilterChain.doFilter(ProxiedFilterChain.java:66)\r\n	at org.apache.shiro.web.servlet.AdviceFilter.executeChain(AdviceFilter.java:108)\r\n	at org.apache.shiro.web.servlet.AdviceFilter.doFilterInternal(AdviceFilter.java:137)\r\n	at org.apache.shiro.web.servlet.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:154)\r\n	at org.apache.shiro.web.servlet.ProxiedFilterChain.doFilter(ProxiedFilterChain.java:66)\r\n	at org.apache.shiro.web.servlet.AbstractShiroFilter.executeChain(AbstractShiroFilter.java:458)\r\n	at org.apache.shiro.web.servlet.AbstractShiroFilter$1.call(AbstractShiroFilter.java:373)\r\n	at org.apache.shiro.subject.support.SubjectCallable.doCall(SubjectCallable.java:90)\r\n	at org.apache.shiro.subject.support.SubjectCallable.call(SubjectCallable.java:83)\r\n	at org.apache.shiro.subject.support.DelegatingSubject.execute(DelegatingSubject.java:387)\r\n	at org.apache.shiro.web.servlet.AbstractShiroFilter.doFilterInternal(AbstractShiroFilter.java:370)\r\n	at org.apache.shiro.web.servlet.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:154)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:189)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:162)\r\n	at io.renren.common.xss.XssFilter.doFilter(XssFilter.java:30)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:189)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:162)\r\n	at org.apache.shiro.web.servlet.ProxiedFilterChain.doFilter(ProxiedFilterChain.java:61)\r\n	at org.apache.shiro.web.servlet.AdviceFilter.executeChain(AdviceFilter.java:108)\r\n	at org.apache.shiro.web.servlet.AdviceFilter.doFilterInternal(AdviceFilter.java:137)\r\n	at org.apache.shiro.web.servlet.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:154)\r\n	at org.apache.shiro.web.servlet.ProxiedFilterChain.doFilter(ProxiedFilterChain.java:66)\r\n	at org.apache.shiro.web.servlet.AdviceFilter.executeChain(AdviceFilter.java:108)\r\n	at org.apache.shiro.web.servlet.AdviceFilter.doFilterInternal(AdviceFilter.java:137)\r\n	at org.apache.shiro.web.servlet.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:154)\r\n	at org.apache.shiro.web.servlet.ProxiedFilterChain.doFilter(ProxiedFilterChain.java:66)\r\n	at org.apache.shiro.web.servlet.AbstractShiroFilter.executeChain(AbstractShiroFilter.java:458)\r\n	at org.apache.shiro.web.servlet.AbstractShiroFilter$1.call(AbstractShiroFilter.java:373)\r\n	at org.apache.shiro.subject.support.SubjectCallable.doCall(SubjectCallable.java:90)\r\n	at org.apache.shiro.subject.support.SubjectCallable.call(SubjectCallable.java:83)\r\n	at org.apache.shiro.subject.support.DelegatingSubject.execute(DelegatingSubject.java:387)\r\n	at org.apache.shiro.web.servlet.AbstractShiroFilter.doFilterInternal(AbstractShiroFilter.java:370)\r\n	at org.apache.shiro.web.servlet.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:154)\r\n	at org.springframework.web.filter.DelegatingFilterProxy.invokeDelegate(DelegatingFilterProxy.java:354)\r\n	at org.springframework.web.filter.DelegatingFilterProxy.doFilter(DelegatingFilterProxy.java:267)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:189)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:162)\r\n	at org.springframework.web.filter.RequestContextFilter.doFilterInternal(RequestContextFilter.java:100)\r\n	at org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:117)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:189)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:162)\r\n	at org.springframework.web.filter.FormContentFilter.doFilterInternal(FormContentFilter.java:93)\r\n	at org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:117)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:189)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:162)\r\n	at org.springframework.web.filter.CharacterEncodingFilter.doFilterInternal(CharacterEncodingFilter.java:201)\r\n	at org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:117)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:189)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:162)\r\n	at org.apache.catalina.core.StandardWrapperValve.invoke(StandardWrapperValve.java:197)\r\n	at org.apache.catalina.core.StandardContextValve.invoke(StandardContextValve.java:97)\r\n	at org.apache.catalina.authenticator.AuthenticatorBase.invoke(AuthenticatorBase.java:541)\r\n	at org.apache.catalina.core.StandardHostValve.invoke(StandardHostValve.java:135)\r\n	at org.apache.catalina.valves.ErrorReportValve.invoke(ErrorReportValve.java:92)\r\n	at org.apache.catalina.core.StandardEngineValve.invoke(StandardEngineValve.java:78)\r\n	at org.apache.catalina.connector.CoyoteAdapter.service(CoyoteAdapter.java:360)\r\n	at org.apache.coyote.http11.Http11Processor.service(Http11Processor.java:399)\r\n	at org.apache.coyote.AbstractProcessorLight.process(AbstractProcessorLight.java:65)\r\n	at org.apache.coyote.AbstractProtocol$ConnectionHandler.process(AbstractProtocol.java:893)\r\n	at org.apache.tomcat.util.net.NioEndpoint$SocketProcessor.doRun(NioEndpoint.java:1789)\r\n	at org.apache.tomcat.util.net.SocketProcessorBase.run(SocketProcessorBase.java:49)\r\n	at org.apache.tomcat.util.threads.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1191)\r\n	at org.apache.tomcat.util.threads.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:659)\r\n	at org.apache.tomcat.util.threads.TaskThread$WrappingRunnable.run(TaskThread.java:61)\r\n	at java.lang.Thread.run(Thread.java:745)\r\nCaused by: java.lang.NumberFormatException: For input string: \"numberlike\"\r\n	at java.lang.NumberFormatException.forInputString(NumberFormatException.java:65)\r\n	at java.lang.Long.parseLong(Long.java:589)\r\n	at java.lang.Long.valueOf(Long.java:803)\r\n	at org.springframework.util.NumberUtils.parseNumber(NumberUtils.java:214)\r\n	at org.springframework.beans.propertyeditors.CustomNumberEditor.setAsText(CustomNumberEditor.java:115)\r\n	at org.springframework.beans.TypeConverterDelegate.doConvertTextValue(TypeConverterDelegate.java:429)\r\n	at org.springframework.beans.TypeConverterDelegate.doConvertValue(TypeConverterDelegate.java:402)\r\n	at org.springframework.beans.TypeConverterDelegate.convertIfNecessary(TypeConverterDelegate.java:155)\r\n	at org.springframework.beans.TypeConverterSupport.convertIfNecessary(TypeConverterSupport.java:73)\r\n	at org.springframework.beans.TypeConverterSupport.convertIfNecessary(TypeConverterSupport.java:53)\r\n	at org.springframework.validation.DataBinder.convertIfNecessary(DataBinder.java:729)\r\n	at org.springframework.web.method.annotation.AbstractNamedValueMethodArgumentResolver.resolveArgument(AbstractNamedValueMethodArgumentResolver.java:125)\r\n	... 88 more\r\n', 1067246875800000001, NULL);
INSERT INTO `sys_log_error` VALUES (1593451836134715393, '/renren-admin/newcomment/numberlike', 'PUT', NULL, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', '0:0:0:0:0:0:0:1', 'org.mybatis.spring.MyBatisSystemException: nested exception is org.apache.ibatis.exceptions.PersistenceException: \r\n### Error updating database.  Cause: com.baomidou.mybatisplus.core.exceptions.MybatisPlusException: Prohibition of table update operation\r\n### The error may exist in io/renren/modules/newcomment/dao/NewcommentsDao.java (best guess)\r\n### The error may involve io.renren.modules.newcomment.dao.NewcommentsDao.update\r\n### The error occurred while executing an update\r\n### Cause: com.baomidou.mybatisplus.core.exceptions.MybatisPlusException: Prohibition of table update operation\r\n	at org.mybatis.spring.MyBatisExceptionTranslator.translateExceptionIfPossible(MyBatisExceptionTranslator.java:96)\r\n	at org.mybatis.spring.SqlSessionTemplate$SqlSessionInterceptor.invoke(SqlSessionTemplate.java:441)\r\n	at com.sun.proxy.$Proxy85.update(Unknown Source)\r\n	at org.mybatis.spring.SqlSessionTemplate.update(SqlSessionTemplate.java:288)\r\n	at com.baomidou.mybatisplus.core.override.MybatisMapperMethod.execute(MybatisMapperMethod.java:64)\r\n	at com.baomidou.mybatisplus.core.override.MybatisMapperProxy$PlainMethodInvoker.invoke(MybatisMapperProxy.java:148)\r\n	at com.baomidou.mybatisplus.core.override.MybatisMapperProxy.invoke(MybatisMapperProxy.java:89)\r\n	at com.sun.proxy.$Proxy115.update(Unknown Source)\r\n	at io.renren.common.service.impl.BaseServiceImpl.update(BaseServiceImpl.java:185)\r\n	at io.renren.common.service.impl.BaseServiceImpl$$FastClassBySpringCGLIB$$24e956ac.invoke(<generated>)\r\n	at org.springframework.cglib.proxy.MethodProxy.invoke(MethodProxy.java:218)\r\n	at org.springframework.aop.framework.CglibAopProxy.invokeMethod(CglibAopProxy.java:386)\r\n	at org.springframework.aop.framework.CglibAopProxy.access$000(CglibAopProxy.java:85)\r\n	at org.springframework.aop.framework.CglibAopProxy$DynamicAdvisedInterceptor.intercept(CglibAopProxy.java:704)\r\n	at io.renren.modules.newcomment.service.impl.NewcommentsServiceImpl$$EnhancerBySpringCGLIB$$c75f2900.update(<generated>)\r\n	at io.renren.modules.newcomment.controller.NewcommentsController.numberlike(NewcommentsController.java:67)\r\n	at io.renren.modules.newcomment.controller.NewcommentsController$$FastClassBySpringCGLIB$$577361c0.invoke(<generated>)\r\n	at org.springframework.cglib.proxy.MethodProxy.invoke(MethodProxy.java:218)\r\n	at org.springframework.aop.framework.CglibAopProxy$CglibMethodInvocation.invokeJoinpoint(CglibAopProxy.java:793)\r\n	at org.springframework.aop.framework.ReflectiveMethodInvocation.proceed(ReflectiveMethodInvocation.java:163)\r\n	at org.springframework.aop.framework.CglibAopProxy$CglibMethodInvocation.proceed(CglibAopProxy.java:763)\r\n	at org.springframework.aop.interceptor.ExposeInvocationInterceptor.invoke(ExposeInvocationInterceptor.java:97)\r\n	at org.springframework.aop.framework.ReflectiveMethodInvocation.proceed(ReflectiveMethodInvocation.java:186)\r\n	at org.springframework.aop.framework.CglibAopProxy$CglibMethodInvocation.proceed(CglibAopProxy.java:763)\r\n	at org.springframework.aop.framework.CglibAopProxy$DynamicAdvisedInterceptor.intercept(CglibAopProxy.java:708)\r\n	at io.renren.modules.newcomment.controller.NewcommentsController$$EnhancerBySpringCGLIB$$904eb62c.numberlike(<generated>)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n	at java.lang.reflect.Method.invoke(Method.java:483)\r\n	at org.springframework.web.method.support.InvocableHandlerMethod.doInvoke(InvocableHandlerMethod.java:205)\r\n	at org.springframework.web.method.support.InvocableHandlerMethod.invokeForRequest(InvocableHandlerMethod.java:150)\r\n	at org.springframework.web.servlet.mvc.method.annotation.ServletInvocableHandlerMethod.invokeAndHandle(ServletInvocableHandlerMethod.java:117)\r\n	at org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter.invokeHandlerMethod(RequestMappingHandlerAdapter.java:895)\r\n	at org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter.handleInternal(RequestMappingHandlerAdapter.java:808)\r\n	at org.springframework.web.servlet.mvc.method.AbstractHandlerMethodAdapter.handle(AbstractHandlerMethodAdapter.java:87)\r\n	at org.springframework.web.servlet.DispatcherServlet.doDispatch(DispatcherServlet.java:1071)\r\n	at org.springframework.web.servlet.DispatcherServlet.doService(DispatcherServlet.java:964)\r\n	at org.springframework.web.servlet.FrameworkServlet.processRequest(FrameworkServlet.java:1006)\r\n	at org.springframework.web.servlet.FrameworkServlet.doPut(FrameworkServlet.java:920)\r\n	at javax.servlet.http.HttpServlet.service(HttpServlet.java:699)\r\n	at org.springframework.web.servlet.FrameworkServlet.service(FrameworkServlet.java:883)\r\n	at javax.servlet.http.HttpServlet.service(HttpServlet.java:779)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:227)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:162)\r\n	at org.apache.tomcat.websocket.server.WsFilter.doFilter(WsFilter.java:53)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:189)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:162)\r\n	at org.apache.shiro.web.servlet.ProxiedFilterChain.doFilter(ProxiedFilterChain.java:61)\r\n	at org.apache.shiro.web.servlet.AdviceFilter.executeChain(AdviceFilter.java:108)\r\n	at org.apache.shiro.web.servlet.AdviceFilter.doFilterInternal(AdviceFilter.java:137)\r\n	at org.apache.shiro.web.servlet.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:154)\r\n	at org.apache.shiro.web.servlet.ProxiedFilterChain.doFilter(ProxiedFilterChain.java:66)\r\n	at org.apache.shiro.web.servlet.AdviceFilter.executeChain(AdviceFilter.java:108)\r\n	at org.apache.shiro.web.servlet.AdviceFilter.doFilterInternal(AdviceFilter.java:137)\r\n	at org.apache.shiro.web.servlet.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:154)\r\n	at org.apache.shiro.web.servlet.ProxiedFilterChain.doFilter(ProxiedFilterChain.java:66)\r\n	at org.apache.shiro.web.servlet.AbstractShiroFilter.executeChain(AbstractShiroFilter.java:458)\r\n	at org.apache.shiro.web.servlet.AbstractShiroFilter$1.call(AbstractShiroFilter.java:373)\r\n	at org.apache.shiro.subject.support.SubjectCallable.doCall(SubjectCallable.java:90)\r\n	at org.apache.shiro.subject.support.SubjectCallable.call(SubjectCallable.java:83)\r\n	at org.apache.shiro.subject.support.DelegatingSubject.execute(DelegatingSubject.java:387)\r\n	at org.apache.shiro.web.servlet.AbstractShiroFilter.doFilterInternal(AbstractShiroFilter.java:370)\r\n	at org.apache.shiro.web.servlet.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:154)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:189)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:162)\r\n	at io.renren.common.xss.XssFilter.doFilter(XssFilter.java:30)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:189)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:162)\r\n	at org.apache.shiro.web.servlet.ProxiedFilterChain.doFilter(ProxiedFilterChain.java:61)\r\n	at org.apache.shiro.web.servlet.AdviceFilter.executeChain(AdviceFilter.java:108)\r\n	at org.apache.shiro.web.servlet.AdviceFilter.doFilterInternal(AdviceFilter.java:137)\r\n	at org.apache.shiro.web.servlet.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:154)\r\n	at org.apache.shiro.web.servlet.ProxiedFilterChain.doFilter(ProxiedFilterChain.java:66)\r\n	at org.apache.shiro.web.servlet.AdviceFilter.executeChain(AdviceFilter.java:108)\r\n	at org.apache.shiro.web.servlet.AdviceFilter.doFilterInternal(AdviceFilter.java:137)\r\n	at org.apache.shiro.web.servlet.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:154)\r\n	at org.apache.shiro.web.servlet.ProxiedFilterChain.doFilter(ProxiedFilterChain.java:66)\r\n	at org.apache.shiro.web.servlet.AbstractShiroFilter.executeChain(AbstractShiroFilter.java:458)\r\n	at org.apache.shiro.web.servlet.AbstractShiroFilter$1.call(AbstractShiroFilter.java:373)\r\n	at org.apache.shiro.subject.support.SubjectCallable.doCall(SubjectCallable.java:90)\r\n	at org.apache.shiro.subject.support.SubjectCallable.call(SubjectCallable.java:83)\r\n	at org.apache.shiro.subject.support.DelegatingSubject.execute(DelegatingSubject.java:387)\r\n	at org.apache.shiro.web.servlet.AbstractShiroFilter.doFilterInternal(AbstractShiroFilter.java:370)\r\n	at org.apache.shiro.web.servlet.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:154)\r\n	at org.springframework.web.filter.DelegatingFilterProxy.invokeDelegate(DelegatingFilterProxy.java:354)\r\n	at org.springframework.web.filter.DelegatingFilterProxy.doFilter(DelegatingFilterProxy.java:267)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:189)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:162)\r\n	at org.springframework.web.filter.RequestContextFilter.doFilterInternal(RequestContextFilter.java:100)\r\n	at org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:117)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:189)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:162)\r\n	at org.springframework.web.filter.FormContentFilter.doFilterInternal(FormContentFilter.java:93)\r\n	at org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:117)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:189)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:162)\r\n	at org.springframework.web.filter.CharacterEncodingFilter.doFilterInternal(CharacterEncodingFilter.java:201)\r\n	at org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:117)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:189)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:162)\r\n	at org.apache.catalina.core.StandardWrapperValve.invoke(StandardWrapperValve.java:197)\r\n	at org.apache.catalina.core.StandardContextValve.invoke(StandardContextValve.java:97)\r\n	at org.apache.catalina.authenticator.AuthenticatorBase.invoke(AuthenticatorBase.java:541)\r\n	at org.apache.catalina.core.StandardHostValve.invoke(StandardHostValve.java:135)\r\n	at org.apache.catalina.valves.ErrorReportValve.invoke(ErrorReportValve.java:92)\r\n	at org.apache.catalina.core.StandardEngineValve.invoke(StandardEngineValve.java:78)\r\n	at org.apache.catalina.connector.CoyoteAdapter.service(CoyoteAdapter.java:360)\r\n	at org.apache.coyote.http11.Http11Processor.service(Http11Processor.java:399)\r\n	at org.apache.coyote.AbstractProcessorLight.process(AbstractProcessorLight.java:65)\r\n	at org.apache.coyote.AbstractProtocol$ConnectionHandler.process(AbstractProtocol.java:893)\r\n	at org.apache.tomcat.util.net.NioEndpoint$SocketProcessor.doRun(NioEndpoint.java:1789)\r\n	at org.apache.tomcat.util.net.SocketProcessorBase.run(SocketProcessorBase.java:49)\r\n	at org.apache.tomcat.util.threads.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1191)\r\n	at org.apache.tomcat.util.threads.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:659)\r\n	at org.apache.tomcat.util.threads.TaskThread$WrappingRunnable.run(TaskThread.java:61)\r\n	at java.lang.Thread.run(Thread.java:745)\r\nCaused by: org.apache.ibatis.exceptions.PersistenceException: \r\n### Error updating database.  Cause: com.baomidou.mybatisplus.core.exceptions.MybatisPlusException: Prohibition of table update operation\r\n### The error may exist in io/renren/modules/newcomment/dao/NewcommentsDao.java (best guess)\r\n### The error may involve io.renren.modules.newcomment.dao.NewcommentsDao.update\r\n### The error occurred while executing an update\r\n### Cause: com.baomidou.mybatisplus.core.exceptions.MybatisPlusException: Prohibition of table update operation\r\n	at org.apache.ibatis.exceptions.ExceptionFactory.wrapException(ExceptionFactory.java:30)\r\n	at org.apache.ibatis.session.defaults.DefaultSqlSession.update(DefaultSqlSession.java:196)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n	at java.lang.reflect.Method.invoke(Method.java:483)\r\n	at org.mybatis.spring.SqlSessionTemplate$SqlSessionInterceptor.invoke(SqlSessionTemplate.java:427)\r\n	... 115 more\r\nCaused by: com.baomidou.mybatisplus.core.exceptions.MybatisPlusException: Prohibition of table update operation\r\n	at com.baomidou.mybatisplus.core.toolkit.ExceptionUtils.mpe(ExceptionUtils.java:49)\r\n	at com.baomidou.mybatisplus.core.toolkit.Assert.isTrue(Assert.java:38)\r\n	at com.baomidou.mybatisplus.core.toolkit.Assert.isFalse(Assert.java:50)\r\n	at com.baomidou.mybatisplus.extension.plugins.inner.BlockAttackInnerInterceptor.checkWhere(BlockAttackInnerInterceptor.java:74)\r\n	at com.baomidou.mybatisplus.extension.plugins.inner.BlockAttackInnerInterceptor.processUpdate(BlockAttackInnerInterceptor.java:70)\r\n	at com.baomidou.mybatisplus.extension.parser.JsqlParserSupport.processParser(JsqlParserSupport.java:93)\r\n	at com.baomidou.mybatisplus.extension.parser.JsqlParserSupport.parserMulti(JsqlParserSupport.java:69)\r\n	at com.baomidou.mybatisplus.extension.plugins.inner.BlockAttackInnerInterceptor.beforePrepare(BlockAttackInnerInterceptor.java:59)\r\n	at com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor.intercept(MybatisPlusInterceptor.java:102)\r\n	at org.apache.ibatis.plugin.Plugin.invoke(Plugin.java:62)\r\n	at com.sun.proxy.$Proxy188.prepare(Unknown Source)\r\n	at org.apache.ibatis.executor.SimpleExecutor.prepareStatement(SimpleExecutor.java:87)\r\n	at org.apache.ibatis.executor.SimpleExecutor.doUpdate(SimpleExecutor.java:49)\r\n	at org.apache.ibatis.executor.BaseExecutor.update(BaseExecutor.java:117)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n	at java.lang.reflect.Method.invoke(Method.java:483)\r\n	at org.apache.ibatis.plugin.Invocation.proceed(Invocation.java:49)\r\n	at com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor.intercept(MybatisPlusInterceptor.java:106)\r\n	at org.apache.ibatis.plugin.Plugin.invoke(Plugin.java:62)\r\n	at com.sun.proxy.$Proxy187.update(Unknown Source)\r\n	at org.apache.ibatis.session.defaults.DefaultSqlSession.update(DefaultSqlSession.java:194)\r\n	... 120 more\r\n', 1067246875800000001, NULL);

-- ----------------------------
-- Table structure for sys_log_login
-- ----------------------------
DROP TABLE IF EXISTS `sys_log_login`;
CREATE TABLE `sys_log_login`  (
  `id` bigint NOT NULL COMMENT 'id',
  `operation` tinyint UNSIGNED NULL DEFAULT NULL COMMENT '用户操作   0：用户登录   1：用户退出',
  `status` tinyint UNSIGNED NOT NULL COMMENT '状态  0：失败    1：成功    2：账号已锁定',
  `user_agent` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '用户代理',
  `ip` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '操作IP',
  `creator_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '用户名',
  `creator` bigint NULL DEFAULT NULL COMMENT '创建者',
  `create_date` datetime NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_status`(`status` ASC) USING BTREE,
  INDEX `idx_create_date`(`create_date` ASC) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '登录日志' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_log_login
-- ----------------------------
INSERT INTO `sys_log_login` VALUES (1592147002894856193, 0, 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36 Edg/106.0.1370.34', '0:0:0:0:0:0:0:1', 'admin', 1067246875800000001, '2022-11-14 21:26:41');
INSERT INTO `sys_log_login` VALUES (1592147128380043265, 0, 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', '0:0:0:0:0:0:0:1', 'admin', 1067246875800000001, '2022-11-14 21:27:11');
INSERT INTO `sys_log_login` VALUES (1592413473147547649, 0, 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:106.0) Gecko/20100101 Firefox/106.0', '127.0.0.1', 'admin', 1067246875800000001, '2022-11-15 15:05:32');
INSERT INTO `sys_log_login` VALUES (1592437661157634049, 0, 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', '0:0:0:0:0:0:0:1', 'admin', 1067246875800000001, '2022-11-15 16:41:39');
INSERT INTO `sys_log_login` VALUES (1592689327823941633, 0, 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:106.0) Gecko/20100101 Firefox/106.0', '127.0.0.1', 'admin', 1067246875800000001, '2022-11-16 09:21:41');
INSERT INTO `sys_log_login` VALUES (1592693233954091010, 0, 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', '0:0:0:0:0:0:0:1', 'admin', 1067246875800000001, '2022-11-16 09:37:12');
INSERT INTO `sys_log_login` VALUES (1592794076275568642, 0, 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', '0:0:0:0:0:0:0:1', 'admin', 1067246875800000001, '2022-11-16 16:17:55');
INSERT INTO `sys_log_login` VALUES (1593048810030968834, 0, 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', '0:0:0:0:0:0:0:1', 'admin', 1067246875800000001, '2022-11-17 09:10:08');
INSERT INTO `sys_log_login` VALUES (1593412953124691970, 0, 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', '0:0:0:0:0:0:0:1', 'admin', 1067246875800000001, '2022-11-18 09:17:07');
INSERT INTO `sys_log_login` VALUES (1593421307226140673, 0, 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', '0:0:0:0:0:0:0:1', 'admin', 1067246875800000001, '2022-11-18 09:50:19');
INSERT INTO `sys_log_login` VALUES (1593447806914326529, 0, 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', '0:0:0:0:0:0:0:1', 'admin', 1067246875800000001, '2022-11-18 11:35:37');
INSERT INTO `sys_log_login` VALUES (1594931368037568514, 0, 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:107.0) Gecko/20100101 Firefox/107.0', '127.0.0.1', 'admin', 1067246875800000001, '2022-11-22 13:50:45');

-- ----------------------------
-- Table structure for sys_log_operation
-- ----------------------------
DROP TABLE IF EXISTS `sys_log_operation`;
CREATE TABLE `sys_log_operation`  (
  `id` bigint NOT NULL COMMENT 'id',
  `operation` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '用户操作',
  `request_uri` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '请求URI',
  `request_method` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '请求方式',
  `request_params` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '请求参数',
  `request_time` int UNSIGNED NOT NULL COMMENT '请求时长(毫秒)',
  `user_agent` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '用户代理',
  `ip` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '操作IP',
  `status` tinyint UNSIGNED NOT NULL COMMENT '状态  0：失败   1：成功',
  `creator_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '用户名',
  `creator` bigint NULL DEFAULT NULL COMMENT '创建者',
  `create_date` datetime NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_create_date`(`create_date` ASC) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '操作日志' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_log_operation
-- ----------------------------
INSERT INTO `sys_log_operation` VALUES (1592438489788526593, '保存', '/renren-admin/sys/menu', 'POST', '{\"id\":null,\"pid\":0,\"children\":[],\"name\":\"影视评论\",\"url\":\"\",\"menuType\":0,\"icon\":\"icon-play-circle\",\"permissions\":\"\",\"sort\":0,\"createDate\":null,\"parentName\":\"一级菜单\"}', 48, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', '0:0:0:0:0:0:0:1', 1, 'admin', 1067246875800000001, '2022-11-15 16:44:57');
INSERT INTO `sys_log_operation` VALUES (1592438529672163330, '修改', '/renren-admin/sys/menu', 'PUT', '{\"id\":1067246875800000002,\"pid\":0,\"children\":[],\"name\":\"权限管理\",\"url\":null,\"menuType\":0,\"icon\":\"icon-safetycertificate\",\"permissions\":null,\"sort\":4,\"createDate\":null,\"parentName\":\"一级菜单\"}', 364, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', '0:0:0:0:0:0:0:1', 1, 'admin', 1067246875800000001, '2022-11-15 16:45:06');
INSERT INTO `sys_log_operation` VALUES (1592439052299218945, '保存', '/renren-admin/sys/menu', 'POST', '{\"id\":null,\"pid\":1592438489641725954,\"children\":[],\"name\":\"影视评论\",\"url\":\"comment/comment\",\"menuType\":0,\"icon\":\"icon-edit-square\",\"permissions\":\"\",\"sort\":0,\"createDate\":null,\"parentName\":\"影视评论\"}', 38, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', '0:0:0:0:0:0:0:1', 1, 'admin', 1067246875800000001, '2022-11-15 16:47:11');
INSERT INTO `sys_log_operation` VALUES (1592439307619086338, '修改', '/renren-admin/sys/menu', 'PUT', '{\"id\":1592438489641725954,\"pid\":0,\"children\":[],\"name\":\"影视评论\",\"url\":\"\",\"menuType\":0,\"icon\":\"icon-play-circle\",\"permissions\":\"\",\"sort\":5,\"createDate\":null,\"parentName\":\"一级菜单\"}', 48, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', '0:0:0:0:0:0:0:1', 1, 'admin', 1067246875800000001, '2022-11-15 16:48:12');

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
  `id` bigint NOT NULL COMMENT 'id',
  `pid` bigint NULL DEFAULT NULL COMMENT '上级ID，一级菜单为0',
  `name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '名称',
  `url` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '菜单URL',
  `permissions` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '授权(多个用逗号分隔，如：sys:user:list,sys:user:save)',
  `menu_type` tinyint UNSIGNED NULL DEFAULT NULL COMMENT '类型   0：菜单   1：按钮',
  `icon` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '菜单图标',
  `sort` int NULL DEFAULT NULL COMMENT '排序',
  `creator` bigint NULL DEFAULT NULL COMMENT '创建者',
  `create_date` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `updater` bigint NULL DEFAULT NULL COMMENT '更新者',
  `update_date` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_pid`(`pid` ASC) USING BTREE,
  INDEX `idx_sort`(`sort` ASC) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '菜单管理' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES (1067246875800000002, 0, '权限管理', NULL, NULL, 0, 'icon-safetycertificate', 4, 1067246875800000001, '2022-11-14 21:13:57', 1067246875800000001, '2022-11-15 16:45:06');
INSERT INTO `sys_menu` VALUES (1067246875800000003, 1067246875800000055, '新增', NULL, 'sys:user:save,sys:dept:list,sys:role:list', 1, NULL, 1, 1067246875800000001, '2022-11-14 21:13:57', 1067246875800000001, '2022-11-14 21:13:57');
INSERT INTO `sys_menu` VALUES (1067246875800000004, 1067246875800000055, '修改', NULL, 'sys:user:update,sys:dept:list,sys:role:list', 1, NULL, 2, 1067246875800000001, '2022-11-14 21:13:57', 1067246875800000001, '2022-11-14 21:13:57');
INSERT INTO `sys_menu` VALUES (1067246875800000005, 1067246875800000055, '删除', NULL, 'sys:user:delete', 1, NULL, 3, 1067246875800000001, '2022-11-14 21:13:57', 1067246875800000001, '2022-11-14 21:13:57');
INSERT INTO `sys_menu` VALUES (1067246875800000006, 1067246875800000055, '导出', NULL, 'sys:user:export', 1, NULL, 4, 1067246875800000001, '2022-11-14 21:13:57', 1067246875800000001, '2022-11-14 21:13:57');
INSERT INTO `sys_menu` VALUES (1067246875800000007, 1067246875800000002, '角色管理', 'sys/role', NULL, 0, 'icon-team', 2, 1067246875800000001, '2022-11-14 21:13:57', 1067246875800000001, '2022-11-14 21:13:57');
INSERT INTO `sys_menu` VALUES (1067246875800000008, 1067246875800000007, '查看', NULL, 'sys:role:page,sys:role:info', 1, NULL, 0, 1067246875800000001, '2022-11-14 21:13:57', 1067246875800000001, '2022-11-14 21:13:57');
INSERT INTO `sys_menu` VALUES (1067246875800000009, 1067246875800000007, '新增', NULL, 'sys:role:save,sys:menu:select,sys:dept:list', 1, NULL, 1, 1067246875800000001, '2022-11-14 21:13:57', 1067246875800000001, '2022-11-14 21:13:57');
INSERT INTO `sys_menu` VALUES (1067246875800000010, 1067246875800000007, '修改', NULL, 'sys:role:update,sys:menu:select,sys:dept:list', 1, NULL, 2, 1067246875800000001, '2022-11-14 21:13:57', 1067246875800000001, '2022-11-14 21:13:57');
INSERT INTO `sys_menu` VALUES (1067246875800000011, 1067246875800000007, '删除', NULL, 'sys:role:delete', 1, NULL, 3, 1067246875800000001, '2022-11-14 21:13:57', 1067246875800000001, '2022-11-14 21:13:57');
INSERT INTO `sys_menu` VALUES (1067246875800000012, 1067246875800000002, '部门管理', 'sys/dept', NULL, 0, 'icon-apartment', 1, 1067246875800000001, '2022-11-14 21:13:57', 1067246875800000001, '2022-11-14 21:13:57');
INSERT INTO `sys_menu` VALUES (1067246875800000014, 1067246875800000012, '查看', NULL, 'sys:dept:list,sys:dept:info', 1, NULL, 0, 1067246875800000001, '2022-11-14 21:13:57', 1067246875800000001, '2022-11-14 21:13:57');
INSERT INTO `sys_menu` VALUES (1067246875800000015, 1067246875800000012, '新增', NULL, 'sys:dept:save', 1, NULL, 1, 1067246875800000001, '2022-11-14 21:13:57', 1067246875800000001, '2022-11-14 21:13:57');
INSERT INTO `sys_menu` VALUES (1067246875800000016, 1067246875800000012, '修改', NULL, 'sys:dept:update', 1, NULL, 2, 1067246875800000001, '2022-11-14 21:13:57', 1067246875800000001, '2022-11-14 21:13:57');
INSERT INTO `sys_menu` VALUES (1067246875800000017, 1067246875800000012, '删除', NULL, 'sys:dept:delete', 1, NULL, 3, 1067246875800000001, '2022-11-14 21:13:57', 1067246875800000001, '2022-11-14 21:13:57');
INSERT INTO `sys_menu` VALUES (1067246875800000025, 1067246875800000035, '菜单管理', 'sys/menu', NULL, 0, 'icon-unorderedlist', 0, 1067246875800000001, '2022-11-14 21:13:57', 1067246875800000001, '2022-11-14 21:13:57');
INSERT INTO `sys_menu` VALUES (1067246875800000026, 1067246875800000025, '查看', NULL, 'sys:menu:list,sys:menu:info', 1, NULL, 0, 1067246875800000001, '2022-11-14 21:13:57', 1067246875800000001, '2022-11-14 21:13:57');
INSERT INTO `sys_menu` VALUES (1067246875800000027, 1067246875800000025, '新增', NULL, 'sys:menu:save', 1, NULL, 1, 1067246875800000001, '2022-11-14 21:13:57', 1067246875800000001, '2022-11-14 21:13:57');
INSERT INTO `sys_menu` VALUES (1067246875800000028, 1067246875800000025, '修改', NULL, 'sys:menu:update', 1, NULL, 2, 1067246875800000001, '2022-11-14 21:13:57', 1067246875800000001, '2022-11-14 21:13:57');
INSERT INTO `sys_menu` VALUES (1067246875800000029, 1067246875800000025, '删除', NULL, 'sys:menu:delete', 1, NULL, 3, 1067246875800000001, '2022-11-14 21:13:57', 1067246875800000001, '2022-11-14 21:13:57');
INSERT INTO `sys_menu` VALUES (1067246875800000030, 1067246875800000035, '定时任务', 'job/schedule', NULL, 0, 'icon-dashboard', 3, 1067246875800000001, '2022-11-14 21:13:57', 1067246875800000001, '2022-11-14 21:13:57');
INSERT INTO `sys_menu` VALUES (1067246875800000031, 1067246875800000030, '查看', NULL, 'sys:schedule:page,sys:schedule:info', 1, NULL, 0, 1067246875800000001, '2022-11-14 21:13:57', 1067246875800000001, '2022-11-14 21:13:57');
INSERT INTO `sys_menu` VALUES (1067246875800000032, 1067246875800000030, '新增', NULL, 'sys:schedule:save', 1, NULL, 1, 1067246875800000001, '2022-11-14 21:13:57', 1067246875800000001, '2022-11-14 21:13:57');
INSERT INTO `sys_menu` VALUES (1067246875800000033, 1067246875800000030, '修改', NULL, 'sys:schedule:update', 1, NULL, 2, 1067246875800000001, '2022-11-14 21:13:57', 1067246875800000001, '2022-11-14 21:13:57');
INSERT INTO `sys_menu` VALUES (1067246875800000034, 1067246875800000030, '删除', NULL, 'sys:schedule:delete', 1, NULL, 3, 1067246875800000001, '2022-11-14 21:13:57', 1067246875800000001, '2022-11-14 21:13:57');
INSERT INTO `sys_menu` VALUES (1067246875800000035, 0, '系统设置', NULL, NULL, 0, 'icon-setting', 1, 1067246875800000001, '2022-11-14 21:13:57', 1067246875800000001, '2022-11-14 21:13:57');
INSERT INTO `sys_menu` VALUES (1067246875800000036, 1067246875800000030, '暂停', NULL, 'sys:schedule:pause', 1, NULL, 4, 1067246875800000001, '2022-11-14 21:13:57', 1067246875800000001, '2022-11-14 21:13:57');
INSERT INTO `sys_menu` VALUES (1067246875800000037, 1067246875800000030, '恢复', NULL, 'sys:schedule:resume', 1, NULL, 5, 1067246875800000001, '2022-11-14 21:13:57', 1067246875800000001, '2022-11-14 21:13:57');
INSERT INTO `sys_menu` VALUES (1067246875800000038, 1067246875800000030, '立即执行', NULL, 'sys:schedule:run', 1, NULL, 6, 1067246875800000001, '2022-11-14 21:13:57', 1067246875800000001, '2022-11-14 21:13:57');
INSERT INTO `sys_menu` VALUES (1067246875800000039, 1067246875800000030, '日志列表', NULL, 'sys:schedule:log', 1, NULL, 7, 1067246875800000001, '2022-11-14 21:13:57', 1067246875800000001, '2022-11-14 21:13:57');
INSERT INTO `sys_menu` VALUES (1067246875800000040, 1067246875800000035, '参数管理', 'sys/params', '', 0, 'icon-fileprotect', 1, 1067246875800000001, '2022-11-14 21:13:57', 1067246875800000001, '2022-11-14 21:13:57');
INSERT INTO `sys_menu` VALUES (1067246875800000041, 1067246875800000035, '字典管理', 'sys/dict-type', NULL, 0, 'icon-golden-fill', 2, 1067246875800000001, '2022-11-14 21:13:57', 1067246875800000001, '2022-11-14 21:13:57');
INSERT INTO `sys_menu` VALUES (1067246875800000042, 1067246875800000041, '查看', NULL, 'sys:dict:page,sys:dict:info', 1, NULL, 0, 1067246875800000001, '2022-11-14 21:13:57', 1067246875800000001, '2022-11-14 21:13:57');
INSERT INTO `sys_menu` VALUES (1067246875800000043, 1067246875800000041, '新增', NULL, 'sys:dict:save', 1, NULL, 1, 1067246875800000001, '2022-11-14 21:13:57', 1067246875800000001, '2022-11-14 21:13:57');
INSERT INTO `sys_menu` VALUES (1067246875800000044, 1067246875800000041, '修改', NULL, 'sys:dict:update', 1, NULL, 2, 1067246875800000001, '2022-11-14 21:13:57', 1067246875800000001, '2022-11-14 21:13:57');
INSERT INTO `sys_menu` VALUES (1067246875800000045, 1067246875800000041, '删除', NULL, 'sys:dict:delete', 1, NULL, 3, 1067246875800000001, '2022-11-14 21:13:57', 1067246875800000001, '2022-11-14 21:13:57');
INSERT INTO `sys_menu` VALUES (1067246875800000046, 0, '日志管理', NULL, NULL, 0, 'icon-container', 2, 1067246875800000001, '2022-11-14 21:13:57', 1067246875800000001, '2022-11-14 21:13:57');
INSERT INTO `sys_menu` VALUES (1067246875800000047, 1067246875800000035, '文件上传', 'oss/oss', 'sys:oss:all', 0, 'icon-upload', 4, 1067246875800000001, '2022-11-14 21:13:57', 1067246875800000001, '2022-11-14 21:13:57');
INSERT INTO `sys_menu` VALUES (1067246875800000048, 1067246875800000046, '登录日志', 'sys/log-login', 'sys:log:login', 0, 'icon-filedone', 0, 1067246875800000001, '2022-11-14 21:13:57', 1067246875800000001, '2022-11-14 21:13:57');
INSERT INTO `sys_menu` VALUES (1067246875800000049, 1067246875800000046, '操作日志', 'sys/log-operation', 'sys:log:operation', 0, 'icon-solution', 1, 1067246875800000001, '2022-11-14 21:13:57', 1067246875800000001, '2022-11-14 21:13:57');
INSERT INTO `sys_menu` VALUES (1067246875800000050, 1067246875800000046, '异常日志', 'sys/log-error', 'sys:log:error', 0, 'icon-file-exception', 2, 1067246875800000001, '2022-11-14 21:13:57', 1067246875800000001, '2022-11-14 21:13:57');
INSERT INTO `sys_menu` VALUES (1067246875800000051, 1067246875800000053, 'SQL监控', '{{ window.SITE_CONFIG[\"apiURL\"] }}/druid/sql.html', NULL, 0, 'icon-database', 0, 1067246875800000001, '2022-11-14 21:13:57', 1067246875800000001, '2022-11-14 21:13:57');
INSERT INTO `sys_menu` VALUES (1067246875800000053, 0, '系统监控', NULL, NULL, 0, 'icon-desktop', 3, 1067246875800000001, '2022-11-14 21:13:57', 1067246875800000001, '2022-11-14 21:13:57');
INSERT INTO `sys_menu` VALUES (1067246875800000055, 1067246875800000002, '用户管理', 'sys/user', NULL, 0, 'icon-user', 0, 1067246875800000001, '2022-11-14 21:13:57', 1067246875800000001, '2022-11-14 21:13:57');
INSERT INTO `sys_menu` VALUES (1067246875800000056, 1067246875800000055, '查看', NULL, 'sys:user:page,sys:user:info', 1, NULL, 0, 1067246875800000001, '2022-11-14 21:13:57', 1067246875800000001, '2022-11-14 21:13:57');
INSERT INTO `sys_menu` VALUES (1067246875800000057, 1067246875800000040, '新增', NULL, 'sys:params:save', 1, NULL, 1, 1067246875800000001, '2022-11-14 21:13:57', 1067246875800000001, '2022-11-14 21:13:57');
INSERT INTO `sys_menu` VALUES (1067246875800000058, 1067246875800000040, '导出', NULL, 'sys:params:export', 1, NULL, 4, 1067246875800000001, '2022-11-14 21:13:57', 1067246875800000001, '2022-11-14 21:13:57');
INSERT INTO `sys_menu` VALUES (1067246875800000059, 1067246875800000040, '查看', '', 'sys:params:page,sys:params:info', 1, NULL, 0, 1067246875800000001, '2022-11-14 21:13:57', 1067246875800000001, '2022-11-14 21:13:57');
INSERT INTO `sys_menu` VALUES (1067246875800000060, 1067246875800000040, '修改', NULL, 'sys:params:update', 1, NULL, 2, 1067246875800000001, '2022-11-14 21:13:57', 1067246875800000001, '2022-11-14 21:13:57');
INSERT INTO `sys_menu` VALUES (1067246875800000061, 1067246875800000040, '删除', '', 'sys:params:delete', 1, '', 3, 1067246875800000001, '2022-11-14 21:13:57', 1067246875800000001, '2022-11-14 21:13:57');
INSERT INTO `sys_menu` VALUES (1156748733921165314, 1067246875800000053, '接口文档', '{{ window.SITE_CONFIG[\"apiURL\"] }}/doc.html', '', 0, 'icon-file-word', 1, 1067246875800000001, '2022-11-14 21:13:57', 1067246875800000001, '2022-11-14 21:13:57');
INSERT INTO `sys_menu` VALUES (1592438489641725954, 0, '影视评论', '', '', 0, 'icon-play-circle', 5, 1067246875800000001, '2022-11-15 16:44:57', 1067246875800000001, '2022-11-15 16:48:12');
INSERT INTO `sys_menu` VALUES (1592439052185972737, 1592438489641725954, '影视评论', 'comment/comment', '', 0, 'icon-edit-square', 0, 1067246875800000001, '2022-11-15 16:47:11', 1067246875800000001, '2022-11-15 16:47:11');

-- ----------------------------
-- Table structure for sys_oss
-- ----------------------------
DROP TABLE IF EXISTS `sys_oss`;
CREATE TABLE `sys_oss`  (
  `id` bigint NOT NULL COMMENT 'id',
  `url` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT 'URL地址',
  `creator` bigint NULL DEFAULT NULL COMMENT '创建者',
  `create_date` datetime NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_create_date`(`create_date` ASC) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '文件上传' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_oss
-- ----------------------------

-- ----------------------------
-- Table structure for sys_params
-- ----------------------------
DROP TABLE IF EXISTS `sys_params`;
CREATE TABLE `sys_params`  (
  `id` bigint NOT NULL COMMENT 'id',
  `param_code` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '参数编码',
  `param_value` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '参数值',
  `param_type` tinyint UNSIGNED NULL DEFAULT 1 COMMENT '类型   0：系统参数   1：非系统参数',
  `remark` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
  `creator` bigint NULL DEFAULT NULL COMMENT '创建者',
  `create_date` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `updater` bigint NULL DEFAULT NULL COMMENT '更新者',
  `update_date` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_param_code`(`param_code` ASC) USING BTREE,
  INDEX `idx_create_date`(`create_date` ASC) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '参数管理' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_params
-- ----------------------------
INSERT INTO `sys_params` VALUES (1067246875800000073, 'CLOUD_STORAGE_CONFIG_KEY', '{\"type\":1,\"qiniuDomain\":\"http://test.oss.renren.io\",\"qiniuPrefix\":\"upload\",\"qiniuAccessKey\":\"NrgMfABZxWLo5B-YYSjoE8-AZ1EISdi1Z3ubLOeZ\",\"qiniuSecretKey\":\"uIwJHevMRWU0VLxFvgy0tAcOdGqasdtVlJkdy6vV\",\"qiniuBucketName\":\"renren-oss\",\"aliyunDomain\":\"\",\"aliyunPrefix\":\"\",\"aliyunEndPoint\":\"\",\"aliyunAccessKeyId\":\"\",\"aliyunAccessKeySecret\":\"\",\"aliyunBucketName\":\"\",\"qcloudDomain\":\"\",\"qcloudPrefix\":\"\",\"qcloudSecretId\":\"\",\"qcloudSecretKey\":\"\",\"qcloudBucketName\":\"\"}', 0, '云存储配置信息', 1067246875800000001, '2022-11-14 21:13:57', 1067246875800000001, '2022-11-14 21:13:57');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `id` bigint NOT NULL COMMENT 'id',
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '角色名称',
  `remark` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
  `dept_id` bigint NULL DEFAULT NULL COMMENT '部门ID',
  `creator` bigint NULL DEFAULT NULL COMMENT '创建者',
  `create_date` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `updater` bigint NULL DEFAULT NULL COMMENT '更新者',
  `update_date` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_dept_id`(`dept_id` ASC) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '角色管理' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_role
-- ----------------------------

-- ----------------------------
-- Table structure for sys_role_data_scope
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_data_scope`;
CREATE TABLE `sys_role_data_scope`  (
  `id` bigint NOT NULL COMMENT 'id',
  `role_id` bigint NULL DEFAULT NULL COMMENT '角色ID',
  `dept_id` bigint NULL DEFAULT NULL COMMENT '部门ID',
  `creator` bigint NULL DEFAULT NULL COMMENT '创建者',
  `create_date` datetime NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_role_id`(`role_id` ASC) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '角色数据权限' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_role_data_scope
-- ----------------------------

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu`  (
  `id` bigint NOT NULL COMMENT 'id',
  `role_id` bigint NULL DEFAULT NULL COMMENT '角色ID',
  `menu_id` bigint NULL DEFAULT NULL COMMENT '菜单ID',
  `creator` bigint NULL DEFAULT NULL COMMENT '创建者',
  `create_date` datetime NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_role_id`(`role_id` ASC) USING BTREE,
  INDEX `idx_menu_id`(`menu_id` ASC) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '角色菜单关系' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------

-- ----------------------------
-- Table structure for sys_role_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_user`;
CREATE TABLE `sys_role_user`  (
  `id` bigint NOT NULL COMMENT 'id',
  `role_id` bigint NULL DEFAULT NULL COMMENT '角色ID',
  `user_id` bigint NULL DEFAULT NULL COMMENT '用户ID',
  `creator` bigint NULL DEFAULT NULL COMMENT '创建者',
  `create_date` datetime NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_role_id`(`role_id` ASC) USING BTREE,
  INDEX `idx_user_id`(`user_id` ASC) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '角色用户关系' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_role_user
-- ----------------------------

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `id` bigint NOT NULL COMMENT 'id',
  `username` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '用户名',
  `password` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '密码',
  `real_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '姓名',
  `head_url` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '头像',
  `gender` tinyint UNSIGNED NULL DEFAULT NULL COMMENT '性别   0：男   1：女    2：保密',
  `email` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '邮箱',
  `mobile` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '手机号',
  `dept_id` bigint NULL DEFAULT NULL COMMENT '部门ID',
  `super_admin` tinyint UNSIGNED NULL DEFAULT NULL COMMENT '超级管理员   0：否   1：是',
  `status` tinyint NULL DEFAULT NULL COMMENT '状态  0：停用   1：正常',
  `creator` bigint NULL DEFAULT NULL COMMENT '创建者',
  `create_date` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `updater` bigint NULL DEFAULT NULL COMMENT '更新者',
  `update_date` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_username`(`username` ASC) USING BTREE,
  INDEX `idx_create_date`(`create_date` ASC) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '系统用户' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1067246875800000001, 'admin', '$2a$10$012Kx2ba5jzqr9gLlG4MX.bnQJTD9UWqF57XDo2N3.fPtLne02u/m', '管理员', NULL, 0, 'root@renren.io', '13612345678', NULL, 1, 1, 1067246875800000001, '2022-11-14 21:13:57', 1067246875800000001, '2022-11-14 21:13:57');

-- ----------------------------
-- Table structure for sys_user_token
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_token`;
CREATE TABLE `sys_user_token`  (
  `id` bigint NOT NULL COMMENT 'id',
  `user_id` bigint NOT NULL COMMENT '用户id',
  `token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '用户token',
  `expire_date` datetime NULL DEFAULT NULL COMMENT '过期时间',
  `update_date` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `create_date` datetime NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `user_id`(`user_id` ASC) USING BTREE,
  UNIQUE INDEX `token`(`token` ASC) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '系统用户Token' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_user_token
-- ----------------------------
INSERT INTO `sys_user_token` VALUES (1592147002961965057, 1067246875800000001, '1210fca50ee93bddc4e56e3d29198b03', '2022-11-23 01:50:45', '2022-11-22 13:50:45', '2022-11-14 21:26:41');

SET FOREIGN_KEY_CHECKS = 1;
