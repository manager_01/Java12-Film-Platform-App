package io.renren.modules.squestion.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.renren.common.service.impl.CrudServiceImpl;
import io.renren.modules.squestion.dao.SquestionDao;
import io.renren.modules.squestion.dto.SquestionDTO;
import io.renren.modules.squestion.entity.SquestionEntity;
import io.renren.modules.squestion.service.SquestionService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * 
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2022-11-15
 */
@Service
public class SquestionServiceImpl extends CrudServiceImpl<SquestionDao, SquestionEntity, SquestionDTO> implements SquestionService {

    @Override
    public QueryWrapper<SquestionEntity> getWrapper(Map<String, Object> params){
        String id = (String)params.get("id");

        QueryWrapper<SquestionEntity> wrapper = new QueryWrapper<>();
        wrapper.eq(StringUtils.isNotBlank(id), "id", id);

        return wrapper;
    }


}