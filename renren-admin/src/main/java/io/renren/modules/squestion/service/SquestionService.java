package io.renren.modules.squestion.service;

import io.renren.common.service.CrudService;
import io.renren.modules.squestion.dto.SquestionDTO;
import io.renren.modules.squestion.entity.SquestionEntity;

/**
 * 
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2022-11-15
 */
public interface SquestionService extends CrudService<SquestionEntity, SquestionDTO> {

}