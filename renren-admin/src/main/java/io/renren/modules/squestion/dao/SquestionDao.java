package io.renren.modules.squestion.dao;

import io.renren.common.dao.BaseDao;
import io.renren.modules.squestion.entity.SquestionEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2022-11-15
 */
@Mapper
public interface SquestionDao extends BaseDao<SquestionEntity> {
	
}