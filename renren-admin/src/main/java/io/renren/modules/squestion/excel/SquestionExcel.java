package io.renren.modules.squestion.excel;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;

import java.util.Date;

/**
 * 
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2022-11-15
 */
@Data
public class SquestionExcel {
    @Excel(name = "")
    private Integer qid;
    @Excel(name = "")
    private Integer uid;
    @Excel(name = "")
    private String title;
    @Excel(name = "")
    private String content;
    @Excel(name = "")
    private Integer bonus;
    @Excel(name = "")
    private Date time;
    @Excel(name = "")
    private Integer readnum;
    @Excel(name = "")
    private Integer dstate;
    @Excel(name = "")
    private Integer state;
    @Excel(name = "")
    private String reason;
    @Excel(name = "")
    private Integer deltag;

}