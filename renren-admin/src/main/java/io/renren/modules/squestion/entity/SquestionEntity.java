package io.renren.modules.squestion.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.github.classgraph.json.Id;
import lombok.Data;

import java.util.Date;

/**
 * 
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2022-11-15
 */
@Data
@TableName("squestion")
public class SquestionEntity {

    /**
     * 
     */
    @TableId(type =IdType.AUTO)
	private Integer qid;
    /**
     * 
     */
	private Integer uid;
    /**
     * 
     */
	private String title;
    /**
     * 
     */
	private String content;
    /**
     * 
     */
	private Integer bonus;
    /**
     * 
     */
	private Date time;
    /**
     * 
     */
	private Integer readnum;
    /**
     * 
     */
	private Integer dstate;
    /**
     * 
     */
	private Integer state;
    /**
     * 
     */
	private String reason;
    /**
     * 
     */
	private Integer deltag;
}