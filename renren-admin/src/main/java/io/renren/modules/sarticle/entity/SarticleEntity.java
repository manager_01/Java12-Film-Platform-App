package io.renren.modules.sarticle.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

/**
 * 
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2022-11-15
 */
@Data
@TableName("sarticle")
public class SarticleEntity {

    /**
     * 
     */
    @TableId(type = IdType.AUTO)
	private Integer arid;
    /**
     * 
     */
	private Integer uid;
    /**
     * 
     */
	private String title;
    /**
     * 
     */
	private String url;
    /**
     * 
     */
	private Integer type;
    /**
     * 
     */
	private Date ptime;
    /**
     * 
     */
	private Integer readnum;
    /**
     * 
     */
	private Integer dznum;
    /**
     * 
     */
	private Integer plnum;
    /**
     * 
     */
	private Integer state;
    /**
     * 
     */
	private String reason;
    /**
     * 
     */
	private Integer deltag;
}