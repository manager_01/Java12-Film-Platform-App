package io.renren.modules.sarticle.service;

import io.renren.common.service.CrudService;
import io.renren.modules.sarticle.dto.SarticleDTO;
import io.renren.modules.sarticle.entity.SarticleEntity;

/**
 * 
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2022-11-15
 */
public interface SarticleService extends CrudService<SarticleEntity, SarticleDTO> {

}