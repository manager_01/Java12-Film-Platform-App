package io.renren.modules.sarticle.dao;

import io.renren.common.dao.BaseDao;
import io.renren.modules.sarticle.entity.SarticleEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2022-11-15
 */
@Mapper
public interface SarticleDao extends BaseDao<SarticleEntity> {
	
}