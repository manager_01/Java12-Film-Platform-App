package io.renren.modules.sarticle.excel;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;

import java.util.Date;

/**
 * 
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2022-11-15
 */
@Data
public class SarticleExcel {
    @Excel(name = "")
    private Integer arid;
    @Excel(name = "")
    private Integer uid;
    @Excel(name = "")
    private String title;
    @Excel(name = "")
    private String url;
    @Excel(name = "")
    private Integer type;
    @Excel(name = "")
    private Date ptime;
    @Excel(name = "")
    private Integer readnum;
    @Excel(name = "")
    private Integer dznum;
    @Excel(name = "")
    private Integer plnum;
    @Excel(name = "")
    private Integer state;
    @Excel(name = "")
    private String reason;
    @Excel(name = "")
    private Integer deltag;

}