package io.renren.modules.sarticle.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;


/**
 * 
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2022-11-15
 */
@Data
@ApiModel(value = "")
public class SarticleDTO implements Serializable {
    private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "")
	private Integer arid;

	@ApiModelProperty(value = "")
	private Integer uid;

	@ApiModelProperty(value = "")
	private String title;

	@ApiModelProperty(value = "")
	private String url;

	@ApiModelProperty(value = "")
	private Integer type;

	@ApiModelProperty(value = "")
	private Date ptime;

	@ApiModelProperty(value = "")
	private Integer readnum;

	@ApiModelProperty(value = "")
	private Integer dznum;

	@ApiModelProperty(value = "")
	private Integer plnum;

	@ApiModelProperty(value = "")
	private Integer state;

	@ApiModelProperty(value = "")
	private String reason;

	@ApiModelProperty(value = "")
	private Integer deltag;


}