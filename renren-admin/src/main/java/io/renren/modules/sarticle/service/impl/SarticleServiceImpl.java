package io.renren.modules.sarticle.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.renren.common.service.impl.CrudServiceImpl;
import io.renren.modules.sarticle.dao.SarticleDao;
import io.renren.modules.sarticle.dto.SarticleDTO;
import io.renren.modules.sarticle.entity.SarticleEntity;
import io.renren.modules.sarticle.service.SarticleService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * 
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2022-11-15
 */
@Service
public class SarticleServiceImpl extends CrudServiceImpl<SarticleDao, SarticleEntity, SarticleDTO> implements SarticleService {

    @Override
    public QueryWrapper<SarticleEntity> getWrapper(Map<String, Object> params){
        String id = (String)params.get("id");

        QueryWrapper<SarticleEntity> wrapper = new QueryWrapper<>();
        wrapper.eq(StringUtils.isNotBlank(id), "id", id);

        return wrapper;
    }


}