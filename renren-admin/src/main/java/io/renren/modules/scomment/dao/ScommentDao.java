package io.renren.modules.scomment.dao;

import io.renren.common.dao.BaseDao;
import io.renren.modules.scomment.entity.ScommentEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2022-11-15
 */
@Mapper
public interface ScommentDao extends BaseDao<ScommentEntity> {
	
}