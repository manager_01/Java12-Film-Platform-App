package io.renren.modules.scomment.service;

import io.renren.common.service.CrudService;
import io.renren.modules.scomment.dto.ScommentDTO;
import io.renren.modules.scomment.entity.ScommentEntity;

/**
 * 
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2022-11-15
 */
public interface ScommentService extends CrudService<ScommentEntity, ScommentDTO> {

}