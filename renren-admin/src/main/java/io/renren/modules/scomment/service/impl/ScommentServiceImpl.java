package io.renren.modules.scomment.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.renren.common.service.impl.CrudServiceImpl;
import io.renren.modules.scomment.dao.ScommentDao;
import io.renren.modules.scomment.dto.ScommentDTO;
import io.renren.modules.scomment.entity.ScommentEntity;
import io.renren.modules.scomment.service.ScommentService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * 
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2022-11-15
 */
@Service
public class ScommentServiceImpl extends CrudServiceImpl<ScommentDao, ScommentEntity, ScommentDTO> implements ScommentService {

    @Override
    public QueryWrapper<ScommentEntity> getWrapper(Map<String, Object> params){
        String id = (String)params.get("id");

        QueryWrapper<ScommentEntity> wrapper = new QueryWrapper<>();
        wrapper.eq(StringUtils.isNotBlank(id), "id", id);

        return wrapper;
    }


}