package io.renren.modules.scomment.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

/**
 * 
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2022-11-15
 */
@Data
@TableName("scomment")
public class ScommentEntity {

    /**
     * 
     */
    @TableId(type = IdType.AUTO)
	private Integer cid;
    /**
     * 
     */
	private Integer arid;
    /**
     * 
     */
	private Integer uid;
    /**
     * 
     */
	private String content;
    /**
     * 
     */
	private Integer dznum;
    /**
     * 
     */
	private Date time;
}