package io.renren.modules.scomment.excel;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;

import java.util.Date;

/**
 * 
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2022-11-15
 */
@Data
public class ScommentExcel {
    @Excel(name = "")
    private Integer cid;
    @Excel(name = "")
    private Integer arid;
    @Excel(name = "")
    private Integer uid;
    @Excel(name = "")
    private String content;
    @Excel(name = "")
    private Integer dznum;
    @Excel(name = "")
    private Date time;

}