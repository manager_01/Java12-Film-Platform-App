package io.renren.modules.suser.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.renren.common.service.impl.CrudServiceImpl;
import io.renren.modules.suser.dao.SuserDao;
import io.renren.modules.suser.dto.SuserDTO;
import io.renren.modules.suser.entity.SuserEntity;
import io.renren.modules.suser.service.SuserService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * 
 *
 * @author ${author} 1072692036@qq.com
 * @since 1.0.0 2022-11-15
 */
@Service
public class SuserServiceImpl extends CrudServiceImpl<SuserDao, SuserEntity, SuserDTO> implements SuserService {

    @Override
    public QueryWrapper<SuserEntity> getWrapper(Map<String, Object> params){
        String id = (String)params.get("id");

        QueryWrapper<SuserEntity> wrapper = new QueryWrapper<>();
        wrapper.eq(StringUtils.isNotBlank(id), "id", id);

        return wrapper;
    }


}