package io.renren.modules.suser.service;

import io.renren.common.service.CrudService;
import io.renren.modules.suser.dto.SuserDTO;
import io.renren.modules.suser.entity.SuserEntity;

/**
 * 
 *
 * @author ${author} 1072692036@qq.com
 * @since 1.0.0 2022-11-15
 */
public interface SuserService extends CrudService<SuserEntity, SuserDTO> {

}