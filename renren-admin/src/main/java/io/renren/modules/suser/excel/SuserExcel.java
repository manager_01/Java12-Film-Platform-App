package io.renren.modules.suser.excel;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;

import java.util.Date;

/**
 * 
 *
 * @author ${author} 1072692036@qq.com
 * @since 1.0.0 2022-11-15
 */
@Data
public class SuserExcel {
    @Excel(name = "")
    private Integer uid;
    @Excel(name = "")
    private String user;
    @Excel(name = "")
    private String password;
    @Excel(name = "")
    private Integer type;

}