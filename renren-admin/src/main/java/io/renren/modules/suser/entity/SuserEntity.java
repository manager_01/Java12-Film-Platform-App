package io.renren.modules.suser.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

/**
 * 
 *
 * @author ${author} 1072692036@qq.com
 * @since 1.0.0 2022-11-15
 */
@Data
@TableName("suser")
public class SuserEntity {

    /**
     * 
     */
    @TableId(type = IdType.AUTO)
	private Integer uid;
    /**
     * 
     */
	private String user;
    /**
     * 
     */
	private String password;
    /**
     * 
     */
	private Integer type;
}