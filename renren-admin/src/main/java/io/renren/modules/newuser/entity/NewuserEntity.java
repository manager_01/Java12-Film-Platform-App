package io.renren.modules.newuser.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

/**
 * 
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2022-11-17
 */
@Data
@TableName("newuser")
public class NewuserEntity {

    /**
     * 
     */
    @TableId(type = IdType.AUTO)
	private Integer uid;
    /**
     * 
     */
	private String uname;
}