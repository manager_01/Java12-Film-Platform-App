package io.renren.modules.newuser.service;

import io.renren.common.service.CrudService;
import io.renren.modules.newuser.dto.NewuserDTO;
import io.renren.modules.newuser.entity.NewuserEntity;

/**
 * 
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2022-11-17
 */
public interface NewuserService extends CrudService<NewuserEntity, NewuserDTO> {

}