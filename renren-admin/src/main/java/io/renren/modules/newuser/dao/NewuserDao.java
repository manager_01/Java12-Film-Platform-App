package io.renren.modules.newuser.dao;

import io.renren.common.dao.BaseDao;
import io.renren.modules.newuser.entity.NewuserEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2022-11-17
 */
@Mapper
public interface NewuserDao extends BaseDao<NewuserEntity> {
	
}