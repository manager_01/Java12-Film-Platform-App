package io.renren.modules.newuser.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.renren.common.service.impl.CrudServiceImpl;
import io.renren.modules.newuser.dao.NewuserDao;
import io.renren.modules.newuser.dto.NewuserDTO;
import io.renren.modules.newuser.entity.NewuserEntity;
import io.renren.modules.newuser.service.NewuserService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * 
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2022-11-17
 */
@Service
public class NewuserServiceImpl extends CrudServiceImpl<NewuserDao, NewuserEntity, NewuserDTO> implements NewuserService {

    @Override
    public QueryWrapper<NewuserEntity> getWrapper(Map<String, Object> params){
        String id = (String)params.get("id");

        QueryWrapper<NewuserEntity> wrapper = new QueryWrapper<>();
        wrapper.eq(StringUtils.isNotBlank(id), "id", id);

        return wrapper;
    }


}