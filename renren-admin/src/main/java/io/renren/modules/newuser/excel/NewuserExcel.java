package io.renren.modules.newuser.excel;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;

import java.util.Date;

/**
 * 
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2022-11-17
 */
@Data
public class NewuserExcel {
    @Excel(name = "")
    private Integer uid;
    @Excel(name = "")
    private String uname;

}