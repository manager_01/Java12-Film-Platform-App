package io.renren.modules.sanswer.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.renren.common.service.impl.CrudServiceImpl;
import io.renren.modules.sanswer.dao.SanswerDao;
import io.renren.modules.sanswer.dto.SanswerDTO;
import io.renren.modules.sanswer.entity.SanswerEntity;
import io.renren.modules.sanswer.service.SanswerService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * 
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2022-11-15
 */
@Service
public class SanswerServiceImpl extends CrudServiceImpl<SanswerDao, SanswerEntity, SanswerDTO> implements SanswerService {

    @Override
    public QueryWrapper<SanswerEntity> getWrapper(Map<String, Object> params){
        String id = (String)params.get("id");

        QueryWrapper<SanswerEntity> wrapper = new QueryWrapper<>();
        wrapper.eq(StringUtils.isNotBlank(id), "id", id);

        return wrapper;
    }


}