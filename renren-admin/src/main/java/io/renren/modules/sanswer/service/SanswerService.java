package io.renren.modules.sanswer.service;

import io.renren.common.service.CrudService;
import io.renren.modules.sanswer.dto.SanswerDTO;
import io.renren.modules.sanswer.entity.SanswerEntity;

/**
 * 
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2022-11-15
 */
public interface SanswerService extends CrudService<SanswerEntity, SanswerDTO> {

}