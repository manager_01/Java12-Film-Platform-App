package io.renren.modules.sanswer.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

/**
 * 
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2022-11-15
 */
@Data
@TableName("sanswer")
public class SanswerEntity {

    /**
     * 
     */
    @TableId(type = IdType.AUTO)
	private Integer ansid;
    /**
     * 
     */
	private Integer qid;
    /**
     * 
     */
	private Integer uid;
    /**
     * 
     */
	private String content;
    /**
     * 
     */
	private Integer dznum;
    /**
     * 
     */
	private Date time;
}