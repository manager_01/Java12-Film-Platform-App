package io.renren.modules.sanswer.excel;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;

import java.util.Date;

/**
 * 
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2022-11-15
 */
@Data
public class SanswerExcel {
    @Excel(name = "")
    private Integer ansid;
    @Excel(name = "")
    private Integer qid;
    @Excel(name = "")
    private Integer uid;
    @Excel(name = "")
    private String content;
    @Excel(name = "")
    private Integer dznum;
    @Excel(name = "")
    private Date time;

}