package io.renren.modules.sanswer.dao;

import io.renren.common.dao.BaseDao;
import io.renren.modules.sanswer.entity.SanswerEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2022-11-15
 */
@Mapper
public interface SanswerDao extends BaseDao<SanswerEntity> {
	
}