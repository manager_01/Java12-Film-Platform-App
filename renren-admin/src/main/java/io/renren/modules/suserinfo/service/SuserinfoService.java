package io.renren.modules.suserinfo.service;

import io.renren.common.page.PageData;
import io.renren.common.service.CrudService;
import io.renren.modules.suserinfo.dto.SuserinfoDTO;
import io.renren.modules.suserinfo.entity.SuserinfoEntity;

/**
 * 
 *
 * @author ${author} 1072692036@qq.com
 * @since 1.0.0 2022-11-15
 */
public interface SuserinfoService extends CrudService<SuserinfoEntity, SuserinfoDTO> {


    PageData<SuserinfoDTO> pageInfo(Integer pageNo, Integer pageSize);
    public Integer editUser(SuserinfoDTO suserinfoDTO);

    SuserinfoDTO viewPoint(Integer uid);

}