package io.renren.modules.suserinfo.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;


/**
 * 
 *
 * @author ${author} 1072692036@qq.com
 * @since 1.0.0 2022-11-15
 */
@Data
@ApiModel(value = "")
public class SuserinfoDTO implements Serializable {
    private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "")
	private Integer pid;

	@ApiModelProperty(value = "")
	private Integer uid;

	@ApiModelProperty(value = "")
	private String nickname;

	@ApiModelProperty(value = "")
	private Integer sex;

	@ApiModelProperty(value = "")
	private Integer age;

	@ApiModelProperty(value = "")
	private String email;

	@ApiModelProperty(value = "")
	private Date birthday;

	@ApiModelProperty(value = "")
	private String descr;

	@ApiModelProperty(value = "")
	private String picurl;

	@ApiModelProperty(value = "")
	private Integer pbonus;

	@ApiModelProperty(value = "")
	private Integer vip;

	@ApiModelProperty(value = "")
	private Integer t;


}