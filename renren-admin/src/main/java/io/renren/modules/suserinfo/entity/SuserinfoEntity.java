package io.renren.modules.suserinfo.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

/**
 * 
 *
 * @author ${author} 1072692036@qq.com
 * @since 1.0.0 2022-11-15
 */
@Data
@TableName("suserinfo")
public class SuserinfoEntity {

    /**
     * 
     */
    @TableId(type = IdType.AUTO)
	private Integer pid;
    /**
     * 
     */
	private Integer uid;
    /**
     * 
     */
	private String nickname;
    /**
     * 
     */
	private Integer sex;
    /**
     * 
     */
	private Integer age;
    /**
     * 
     */
	private String email;
    /**
     * 
     */
	private Date birthday;
    /**
     * 
     */
	private String descr;
    /**
     * 
     */
	private String picurl;
    /**
     * 
     */
	private Integer pbonus;
    /**
     * 
     */
	private Integer vip;
    /**
     * 
     */
	private Integer t;
}