package io.renren.modules.suserinfo.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.renren.common.page.PageData;
import io.renren.common.service.impl.CrudServiceImpl;
import io.renren.common.utils.ConvertUtils;
import io.renren.modules.suserinfo.dao.SuserinfoDao;
import io.renren.modules.suserinfo.dto.SuserinfoDTO;
import io.renren.modules.suserinfo.entity.SuserinfoEntity;
import io.renren.modules.suserinfo.service.SuserinfoService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author ${author} 1072692036@qq.com
 * @since 1.0.0 2022-11-15
 */
@Service
public class SuserinfoServiceImpl extends CrudServiceImpl<SuserinfoDao, SuserinfoEntity, SuserinfoDTO> implements SuserinfoService {

    @Override
    public QueryWrapper<SuserinfoEntity> getWrapper(Map<String, Object> params){
        String id = (String)params.get("id");

        QueryWrapper<SuserinfoEntity> wrapper = new QueryWrapper<>();
        wrapper.eq(StringUtils.isNotBlank(id), "id", id);

        return wrapper;
    }


    @Override
    public PageData<SuserinfoDTO> pageInfo(Integer pageNo, Integer pageSize) {
        IPage<SuserinfoEntity> page = new Page<>(pageNo,pageSize);
        IPage<SuserinfoEntity> result = baseDao.selectPage(page, null);
        List<SuserinfoEntity> drugInfoEntityList = result.getRecords();
        List<SuserinfoDTO> drugInfoDTOList = ConvertUtils.sourceToTarget(drugInfoEntityList, SuserinfoDTO.class);
        PageData<SuserinfoDTO> pageData = new PageData<>(drugInfoDTOList, result.getTotal());
        return pageData;
    }

    @Override
    public Integer editUser(SuserinfoDTO suserinfoDTO) {

        SuserinfoEntity suserinfoEntity = ConvertUtils.sourceToTarget(suserinfoDTO, SuserinfoEntity.class);
        int num = baseDao.updateById(suserinfoEntity);
        return num;
    }

    @Override
    public SuserinfoDTO viewPoint(Integer uid) {

        SuserinfoEntity suserinfoEntity = baseDao.selectById(uid);
        SuserinfoDTO suserinfoDTO = ConvertUtils.sourceToTarget(suserinfoEntity, SuserinfoDTO.class);
        return suserinfoDTO;
    }
}