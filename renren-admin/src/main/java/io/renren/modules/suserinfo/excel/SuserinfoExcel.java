package io.renren.modules.suserinfo.excel;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;

import java.util.Date;

/**
 * 
 *
 * @author ${author} 1072692036@qq.com
 * @since 1.0.0 2022-11-15
 */
@Data
public class SuserinfoExcel {
    @Excel(name = "")
    private Integer pid;
    @Excel(name = "")
    private Integer uid;
    @Excel(name = "")
    private String nickname;
    @Excel(name = "")
    private Integer sex;
    @Excel(name = "")
    private Integer age;
    @Excel(name = "")
    private String email;
    @Excel(name = "")
    private Date birthday;
    @Excel(name = "")
    private String descr;
    @Excel(name = "")
    private String picurl;
    @Excel(name = "")
    private Integer pbonus;
    @Excel(name = "")
    private Integer vip;
    @Excel(name = "")
    private Integer t;

}