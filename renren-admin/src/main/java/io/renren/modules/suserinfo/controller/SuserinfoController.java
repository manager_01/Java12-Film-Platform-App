package io.renren.modules.suserinfo.controller;

import io.renren.common.annotation.LogOperation;
import io.renren.common.constant.Constant;
import io.renren.common.page.PageData;
import io.renren.common.utils.ExcelUtils;
import io.renren.common.utils.Result;
import io.renren.common.validator.AssertUtils;
import io.renren.common.validator.ValidatorUtils;
import io.renren.common.validator.group.AddGroup;
import io.renren.common.validator.group.DefaultGroup;
import io.renren.common.validator.group.UpdateGroup;
import io.renren.modules.suserinfo.dto.SuserinfoDTO;
import io.renren.modules.suserinfo.excel.SuserinfoExcel;
import io.renren.modules.suserinfo.service.SuserinfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;


/**
 * 
 *
 * @author ${author} 1072692036@qq.com
 * @since 1.0.0 2022-11-15
 */
@RestController
@RequestMapping("suserinfo")
@Api(tags="")
public class SuserinfoController {
    @Autowired
    private SuserinfoService suserinfoService;

    /**
     * <p>查询用户</p >
     *
     * @param pageNo
 * @param pageSize
     * @return io.renren.common.utils.Result<io.renren.common.page.PageData<io.renren.modules.suserinfo.dto.SuserinfoDTO>>
     * @methodName pageInfo
     * @author liuqinhe
     * @time 2022-11-21 16:09:59
     */
    @GetMapping("pageInfo")
    public Result<PageData<SuserinfoDTO>> pageInfo(Integer pageNo,Integer pageSize){
        PageData<SuserinfoDTO> pageData = suserinfoService.pageInfo(pageNo, pageSize);
        return new Result<PageData<SuserinfoDTO>>().ok(pageData);
    }


    /**
     * <p>修改用户</p >
     *
     * @param suserinfoDTO
     * @return io.renren.common.utils.Result<java.lang.Integer>
     * @methodName editUser
     * @author liuqinhe
     * @time 2022-11-21 16:11:00
     */
    @PutMapping("{suserinfoDTO}")
    public Result<Integer> editUser(SuserinfoDTO suserinfoDTO){
        Integer integer = suserinfoService.editUser(suserinfoDTO);
        return new Result<Integer>().ok(integer);
    }

    @GetMapping("byId/{uid}")
    public Result<SuserinfoDTO> viewPoint(@PathVariable Integer uid){
        SuserinfoDTO suserinfoDTO = suserinfoService.viewPoint(uid);
        return new Result<SuserinfoDTO>().ok(suserinfoDTO);
    }
    @GetMapping("page")
    @ApiOperation("分页")
    @ApiImplicitParams({
        @ApiImplicitParam(name = Constant.PAGE, value = "当前页码，从1开始", paramType = "query", required = true, dataType="int") ,
        @ApiImplicitParam(name = Constant.LIMIT, value = "每页显示记录数", paramType = "query",required = true, dataType="int") ,
        @ApiImplicitParam(name = Constant.ORDER_FIELD, value = "排序字段", paramType = "query", dataType="String") ,
        @ApiImplicitParam(name = Constant.ORDER, value = "排序方式，可选值(asc、desc)", paramType = "query", dataType="String")
    })
    @RequiresPermissions("suserinfo:suserinfo:page")
    public Result<PageData<SuserinfoDTO>> page(@ApiIgnore @RequestParam Map<String, Object> params){
        PageData<SuserinfoDTO> page = suserinfoService.page(params);

        return new Result<PageData<SuserinfoDTO>>().ok(page);
    }

    @GetMapping("{id}")
    @ApiOperation("信息")
    @RequiresPermissions("suserinfo:suserinfo:info")
    public Result<SuserinfoDTO> get(@PathVariable("id") Long id){
        SuserinfoDTO data = suserinfoService.get(id);

        return new Result<SuserinfoDTO>().ok(data);
    }

    @PostMapping
    @ApiOperation("保存")
    @LogOperation("保存")
    @RequiresPermissions("suserinfo:suserinfo:save")
    public Result save(@RequestBody SuserinfoDTO dto){
        //效验数据
        ValidatorUtils.validateEntity(dto, AddGroup.class, DefaultGroup.class);

        suserinfoService.save(dto);

        return new Result();
    }

    @PutMapping
    @ApiOperation("修改")
    @LogOperation("修改")
    @RequiresPermissions("suserinfo:suserinfo:update")
    public Result update(@RequestBody SuserinfoDTO dto){
        //效验数据
        ValidatorUtils.validateEntity(dto, UpdateGroup.class, DefaultGroup.class);

        suserinfoService.update(dto);

        return new Result();
    }

    @DeleteMapping
    @ApiOperation("删除")
    @LogOperation("删除")
    @RequiresPermissions("suserinfo:suserinfo:delete")
    public Result delete(@RequestBody Long[] ids){
        //效验数据
        AssertUtils.isArrayEmpty(ids, "id");

        suserinfoService.delete(ids);

        return new Result();
    }

    @GetMapping("export")
    @ApiOperation("导出")
    @LogOperation("导出")
    @RequiresPermissions("suserinfo:suserinfo:export")
    public void export(@ApiIgnore @RequestParam Map<String, Object> params, HttpServletResponse response) throws Exception {
        List<SuserinfoDTO> list = suserinfoService.list(params);

        ExcelUtils.exportExcelToTarget(response, null, list, SuserinfoExcel.class);
    }

}