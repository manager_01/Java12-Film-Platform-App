package io.renren.modules.stcoin.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.renren.common.service.impl.CrudServiceImpl;
import io.renren.modules.stcoin.dao.StcoinDao;
import io.renren.modules.stcoin.dto.StcoinDTO;
import io.renren.modules.stcoin.entity.StcoinEntity;
import io.renren.modules.stcoin.service.StcoinService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * 
 *
 * @author ${author} 1072692036@qq.com
 * @since 1.0.0 2022-11-15
 */
@Service
public class StcoinServiceImpl extends CrudServiceImpl<StcoinDao, StcoinEntity, StcoinDTO> implements StcoinService {

    @Override
    public QueryWrapper<StcoinEntity> getWrapper(Map<String, Object> params){
        String id = (String)params.get("id");

        QueryWrapper<StcoinEntity> wrapper = new QueryWrapper<>();
        wrapper.eq(StringUtils.isNotBlank(id), "id", id);

        return wrapper;
    }


}