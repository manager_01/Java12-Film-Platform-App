package io.renren.modules.stcoin.service;

import io.renren.common.service.CrudService;
import io.renren.modules.stcoin.dto.StcoinDTO;
import io.renren.modules.stcoin.entity.StcoinEntity;

/**
 * 
 *
 * @author ${author} 1072692036@qq.com
 * @since 1.0.0 2022-11-15
 */
public interface StcoinService extends CrudService<StcoinEntity, StcoinDTO> {

}