package io.renren.modules.stcoin.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

/**
 * 
 *
 * @author ${author} 1072692036@qq.com
 * @since 1.0.0 2022-11-15
 */
@Data
@TableName("stcoin")
public class StcoinEntity {

    /**
     * 
     */
    @TableId(type = IdType.AUTO)
	private Integer tid;
    /**
     * 
     */
	private Integer pid;
    /**
     * 
     */
	private Date time;
    /**
     * 
     */
	private Integer money;
}