package io.renren.modules.stcoin.dao;

import io.renren.common.dao.BaseDao;
import io.renren.modules.stcoin.entity.StcoinEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 *
 * @author ${author} 1072692036@qq.com
 * @since 1.0.0 2022-11-15
 */
@Mapper
public interface StcoinDao extends BaseDao<StcoinEntity> {
	
}