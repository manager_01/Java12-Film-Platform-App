package io.renren.modules.stcoin.excel;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;

import java.util.Date;

/**
 * 
 *
 * @author ${author} 1072692036@qq.com
 * @since 1.0.0 2022-11-15
 */
@Data
public class StcoinExcel {
    @Excel(name = "")
    private Integer tid;
    @Excel(name = "")
    private Integer pid;
    @Excel(name = "")
    private Date time;
    @Excel(name = "")
    private Integer money;

}