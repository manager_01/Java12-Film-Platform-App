package io.renren.modules.stcoin.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;


/**
 * 
 *
 * @author ${author} 1072692036@qq.com
 * @since 1.0.0 2022-11-15
 */
@Data
@ApiModel(value = "")
public class StcoinDTO implements Serializable {
    private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "")
	private Integer tid;

	@ApiModelProperty(value = "")
	private Integer pid;

	@ApiModelProperty(value = "")
	private Date time;

	@ApiModelProperty(value = "")
	private Integer money;


}