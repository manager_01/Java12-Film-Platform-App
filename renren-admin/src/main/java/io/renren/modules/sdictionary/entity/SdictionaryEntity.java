package io.renren.modules.sdictionary.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

/**
 * 
 *
 * @author ${author} 1072692036@qq.com
 * @since 1.0.0 2022-11-15
 */
@Data
@TableName("sdictionary")
public class SdictionaryEntity {

    /**
     * 
     */
    @TableId(type = IdType.AUTO)
	private Integer fid;
    /**
     * 
     */
	private String fname;
    /**
     * 
     */
	private Integer fcode;
    /**
     * 
     */
	private String fvalue;
    /**
     * 
     */
	private Integer pcode;
}