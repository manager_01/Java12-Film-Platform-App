package io.renren.modules.sdictionary.service;

import io.renren.common.service.CrudService;
import io.renren.modules.sdictionary.dto.SdictionaryDTO;
import io.renren.modules.sdictionary.entity.SdictionaryEntity;

/**
 * 
 *
 * @author ${author} 1072692036@qq.com
 * @since 1.0.0 2022-11-15
 */
public interface SdictionaryService extends CrudService<SdictionaryEntity, SdictionaryDTO> {

}