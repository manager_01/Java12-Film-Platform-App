package io.renren.modules.sdictionary.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.renren.common.service.impl.CrudServiceImpl;
import io.renren.modules.sdictionary.dao.SdictionaryDao;
import io.renren.modules.sdictionary.dto.SdictionaryDTO;
import io.renren.modules.sdictionary.entity.SdictionaryEntity;
import io.renren.modules.sdictionary.service.SdictionaryService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * 
 *
 * @author ${author} 1072692036@qq.com
 * @since 1.0.0 2022-11-15
 */
@Service
public class SdictionaryServiceImpl extends CrudServiceImpl<SdictionaryDao, SdictionaryEntity, SdictionaryDTO> implements SdictionaryService {

    @Override
    public QueryWrapper<SdictionaryEntity> getWrapper(Map<String, Object> params){
        String id = (String)params.get("id");

        QueryWrapper<SdictionaryEntity> wrapper = new QueryWrapper<>();
        wrapper.eq(StringUtils.isNotBlank(id), "id", id);

        return wrapper;
    }


}