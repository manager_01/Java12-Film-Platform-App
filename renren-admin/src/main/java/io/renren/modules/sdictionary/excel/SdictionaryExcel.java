package io.renren.modules.sdictionary.excel;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;

import java.util.Date;

/**
 * 
 *
 * @author ${author} 1072692036@qq.com
 * @since 1.0.0 2022-11-15
 */
@Data
public class SdictionaryExcel {
    @Excel(name = "")
    private Integer fid;
    @Excel(name = "")
    private String fname;
    @Excel(name = "")
    private Integer fcode;
    @Excel(name = "")
    private String fvalue;
    @Excel(name = "")
    private Integer pcode;

}