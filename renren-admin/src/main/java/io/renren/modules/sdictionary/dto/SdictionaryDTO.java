package io.renren.modules.sdictionary.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;


/**
 * 
 *
 * @author ${author} 1072692036@qq.com
 * @since 1.0.0 2022-11-15
 */
@Data
@ApiModel(value = "")
public class SdictionaryDTO implements Serializable {
    private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "")
	private Integer fid;

	@ApiModelProperty(value = "")
	private String fname;

	@ApiModelProperty(value = "")
	private Integer fcode;

	@ApiModelProperty(value = "")
	private String fvalue;

	@ApiModelProperty(value = "")
	private Integer pcode;


}