package io.renren.modules.scollection.excel;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;

import java.util.Date;

/**
 * 
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2022-11-15
 */
@Data
public class ScollectionExcel {
    @Excel(name = "")
    private Integer coid;
    @Excel(name = "")
    private Integer uid;
    @Excel(name = "")
    private Integer arid;
    @Excel(name = "")
    private Date time;
    @Excel(name = "")
    private Integer deltag;

}