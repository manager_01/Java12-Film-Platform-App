package io.renren.modules.scollection.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.renren.common.service.impl.CrudServiceImpl;
import io.renren.modules.scollection.dao.ScollectionDao;
import io.renren.modules.scollection.dto.ScollectionDTO;
import io.renren.modules.scollection.entity.ScollectionEntity;
import io.renren.modules.scollection.service.ScollectionService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * 
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2022-11-15
 */
@Service
public class ScollectionServiceImpl extends CrudServiceImpl<ScollectionDao, ScollectionEntity, ScollectionDTO> implements ScollectionService {

    @Override
    public QueryWrapper<ScollectionEntity> getWrapper(Map<String, Object> params){
        String id = (String)params.get("id");

        QueryWrapper<ScollectionEntity> wrapper = new QueryWrapper<>();
        wrapper.eq(StringUtils.isNotBlank(id), "id", id);

        return wrapper;
    }


}