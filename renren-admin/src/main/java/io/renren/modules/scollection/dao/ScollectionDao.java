package io.renren.modules.scollection.dao;

import io.renren.common.dao.BaseDao;
import io.renren.modules.scollection.entity.ScollectionEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2022-11-15
 */
@Mapper
public interface ScollectionDao extends BaseDao<ScollectionEntity> {
	
}