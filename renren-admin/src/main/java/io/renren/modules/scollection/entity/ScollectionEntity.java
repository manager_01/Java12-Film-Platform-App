package io.renren.modules.scollection.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

/**
 * 
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2022-11-15
 */
@Data
@TableName("scollection")
public class ScollectionEntity {

    /**
     * 
     */
    @TableId(type = IdType.AUTO)
	private Integer coid;
    /**
     * 
     */
	private Integer uid;
    /**
     * 
     */
	private Integer arid;
    /**
     * 
     */
	private Date time;
    /**
     * 
     */
	private Integer deltag;
}