package io.renren.modules.scollection.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;


/**
 * 
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2022-11-15
 */
@Data
@ApiModel(value = "")
public class ScollectionDTO implements Serializable {
    private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "")
	private Integer coid;

	@ApiModelProperty(value = "")
	private Integer uid;

	@ApiModelProperty(value = "")
	private Integer arid;

	@ApiModelProperty(value = "")
	private Date time;

	@ApiModelProperty(value = "")
	private Integer deltag;


}