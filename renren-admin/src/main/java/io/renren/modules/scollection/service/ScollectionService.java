package io.renren.modules.scollection.service;

import io.renren.common.service.CrudService;
import io.renren.modules.scollection.dto.ScollectionDTO;
import io.renren.modules.scollection.entity.ScollectionEntity;

/**
 * 
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2022-11-15
 */
public interface ScollectionService extends CrudService<ScollectionEntity, ScollectionDTO> {

}