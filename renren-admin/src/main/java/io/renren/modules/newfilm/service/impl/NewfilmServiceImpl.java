package io.renren.modules.newfilm.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.renren.common.page.PageData;
import io.renren.common.service.impl.CrudServiceImpl;
import io.renren.common.utils.ConvertUtils;
import io.renren.modules.newfilm.dao.NewfilmDao;
import io.renren.modules.newfilm.dto.NewfilmDTO;
import io.renren.modules.newfilm.entity.NewfilmEntity;
import io.renren.modules.newfilm.service.NewfilmService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2022-11-16
 */
@Service
public class NewfilmServiceImpl extends CrudServiceImpl<NewfilmDao, NewfilmEntity, NewfilmDTO> implements NewfilmService {

    @Override
    public QueryWrapper<NewfilmEntity> getWrapper(Map<String, Object> params) {
        String id = (String) params.get("id");

        QueryWrapper<NewfilmEntity> wrapper = new QueryWrapper<>();
        wrapper.eq(StringUtils.isNotBlank(id), "id", id);

        return wrapper;
    }


    @Override
    public PageData<NewfilmDTO> pageInfo(Integer pageNo, Integer pageSize, String input) {

        Page<NewfilmEntity> page = new Page<>(pageNo, pageSize);

        LambdaQueryWrapper<NewfilmEntity> lqw = new LambdaQueryWrapper<>();

        lqw.like(StringUtils.isNotBlank(input) ,NewfilmEntity::getFilmname, input);

        Page<NewfilmEntity> result= baseDao.selectPage(page, lqw);

        List<NewfilmEntity> records = result.getRecords();

        List<NewfilmDTO> newfilmDTOS = ConvertUtils.sourceToTarget(records, NewfilmDTO.class);
        PageData<NewfilmDTO>  newfilmDTOPageData = new PageData<>(newfilmDTOS, result.getTotal());

        return  newfilmDTOPageData;




    }
}