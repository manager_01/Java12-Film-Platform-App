package io.renren.modules.newfilm.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;


/**
 * 
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2022-11-16
 */
@Data
@ApiModel(value = "")
public class NewfilmDTO implements Serializable {
    private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "")
	private Integer filmid;

	@ApiModelProperty(value = "")
	private String filmname;

	@ApiModelProperty(value = "")
	private String filmupdate;

	@ApiModelProperty(value = "")
	private String filmstar;

	@ApiModelProperty(value = "")
	private String filmpictures;

	@ApiModelProperty(value = "")
	private Integer filmscore;


}