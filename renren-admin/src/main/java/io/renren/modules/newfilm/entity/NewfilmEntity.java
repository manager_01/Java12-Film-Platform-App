package io.renren.modules.newfilm.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

/**
 * 
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2022-11-16
 */
@Data
@TableName("newfilm")
public class NewfilmEntity {

    /**
     * 
     */
    @TableId(type = IdType.AUTO)
	private Integer filmid;
    /**
     * 
     */
	private String filmname;
    /**
     * 
     */
	private String filmupdate;
    /**
     * 
     */
	private String filmstar;
    /**
     * 
     */
	private String filmpictures;
    /**
     * 
     */

	private Integer filmscore;
}