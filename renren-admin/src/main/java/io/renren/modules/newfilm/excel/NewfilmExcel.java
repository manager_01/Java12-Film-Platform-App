package io.renren.modules.newfilm.excel;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;

import java.util.Date;

/**
 * 
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2022-11-16
 */
@Data
public class NewfilmExcel {
    @Excel(name = "")
    private Integer filmid;
    @Excel(name = "")
    private String filmname;
    @Excel(name = "")
    private String filmupdate;
    @Excel(name = "")
    private String filmstar;
    @Excel(name = "")
    private String filmpictures;
    @Excel(name = "")
    private Integer filmscore;

}