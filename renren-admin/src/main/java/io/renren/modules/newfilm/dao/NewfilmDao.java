package io.renren.modules.newfilm.dao;

import io.renren.common.dao.BaseDao;
import io.renren.modules.newfilm.entity.NewfilmEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2022-11-16
 */
@Mapper
public interface NewfilmDao extends BaseDao<NewfilmEntity> {
	
}