package io.renren.modules.newfilm.service;

import io.renren.common.page.PageData;
import io.renren.common.service.CrudService;
import io.renren.modules.newfilm.dto.NewfilmDTO;
import io.renren.modules.newfilm.entity.NewfilmEntity;

/**
 * 
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2022-11-16
 */
public interface NewfilmService extends CrudService<NewfilmEntity, NewfilmDTO> {

    PageData<NewfilmDTO> pageInfo(Integer pageNo, Integer pageSize, String input);
}