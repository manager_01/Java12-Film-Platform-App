package io.renren.modules.newfilm.controller;

import io.renren.common.annotation.LogOperation;
import io.renren.common.constant.Constant;
import io.renren.common.page.PageData;
import io.renren.common.utils.ExcelUtils;
import io.renren.common.utils.Result;
import io.renren.common.validator.AssertUtils;
import io.renren.common.validator.ValidatorUtils;
import io.renren.common.validator.group.AddGroup;
import io.renren.common.validator.group.DefaultGroup;
import io.renren.common.validator.group.UpdateGroup;
import io.renren.modules.newfilm.dto.NewfilmDTO;
import io.renren.modules.newfilm.excel.NewfilmExcel;
import io.renren.modules.newfilm.service.NewfilmService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;


/**
 * 
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2022-11-16
 */
@RestController
@RequestMapping("newfilm")
@Api(tags="")
public class NewfilmController {
    @Autowired
    private NewfilmService newfilmService;


    @GetMapping("pageInfo")
    public Result<PageData<NewfilmDTO>> pageInfo(Integer pageNo,Integer pageSize,String input){


        PageData<NewfilmDTO> pageInfo=newfilmService.pageInfo(pageNo,pageSize,input);

        return new Result<PageData<NewfilmDTO>>().ok(pageInfo);



    }


    @GetMapping("page")
    @ApiOperation("分页")
    @ApiImplicitParams({
        @ApiImplicitParam(name = Constant.PAGE, value = "当前页码，从1开始", paramType = "query", required = true, dataType="int") ,
        @ApiImplicitParam(name = Constant.LIMIT, value = "每页显示记录数", paramType = "query",required = true, dataType="int") ,
        @ApiImplicitParam(name = Constant.ORDER_FIELD, value = "排序字段", paramType = "query", dataType="String") ,
        @ApiImplicitParam(name = Constant.ORDER, value = "排序方式，可选值(asc、desc)", paramType = "query", dataType="String")
    })
    @RequiresPermissions("newfilm:newfilm:page")
    public Result<PageData<NewfilmDTO>> page(@ApiIgnore @RequestParam Map<String, Object> params){
        PageData<NewfilmDTO> page = newfilmService.page(params);

        return new Result<PageData<NewfilmDTO>>().ok(page);
    }

    @GetMapping("{id}")
    @ApiOperation("信息")
    @RequiresPermissions("newfilm:newfilm:info")
    public Result<NewfilmDTO> get(@PathVariable("id") Long id){
        NewfilmDTO data = newfilmService.get(id);

        return new Result<NewfilmDTO>().ok(data);
    }

    @PostMapping
    @ApiOperation("保存")
    @LogOperation("保存")
    @RequiresPermissions("newfilm:newfilm:save")
    public Result save(@RequestBody NewfilmDTO dto){
        //效验数据
        ValidatorUtils.validateEntity(dto, AddGroup.class, DefaultGroup.class);

        newfilmService.save(dto);

        return new Result();
    }

    @PutMapping
    @ApiOperation("修改")
    @LogOperation("修改")
    @RequiresPermissions("newfilm:newfilm:update")
    public Result update(@RequestBody NewfilmDTO dto){
        //效验数据
        ValidatorUtils.validateEntity(dto, UpdateGroup.class, DefaultGroup.class);

        newfilmService.update(dto);

        return new Result();
    }

    @DeleteMapping
    @ApiOperation("删除")
    @LogOperation("删除")
    @RequiresPermissions("newfilm:newfilm:delete")
    public Result delete(@RequestBody Long[] ids){
        //效验数据
        AssertUtils.isArrayEmpty(ids, "id");

        newfilmService.delete(ids);

        return new Result();
    }

    @GetMapping("export")
    @ApiOperation("导出")
    @LogOperation("导出")
    @RequiresPermissions("newfilm:newfilm:export")
    public void export(@ApiIgnore @RequestParam Map<String, Object> params, HttpServletResponse response) throws Exception {
        List<NewfilmDTO> list = newfilmService.list(params);

        ExcelUtils.exportExcelToTarget(response, null, list, NewfilmExcel.class);
    }

}