package io.renren.modules.sdelrecord.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

/**
 * 
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2022-11-15
 */
@Data
@TableName("sdelrecord")
public class SdelrecordEntity {

    /**
     * 
     */
    @TableId(type = IdType.AUTO)
	private Integer deid;
    /**
     * 
     */
	private Integer type;
    /**
     * 
     */
	private Integer keyid;
    /**
     * 
     */
	private String reason;
    /**
     * 
     */
	private Date time;
    /**
     * 
     */
	private String descr;
}