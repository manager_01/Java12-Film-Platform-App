package io.renren.modules.sdelrecord.service;

import io.renren.common.service.CrudService;
import io.renren.modules.sdelrecord.dto.SdelrecordDTO;
import io.renren.modules.sdelrecord.entity.SdelrecordEntity;

/**
 * 
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2022-11-15
 */
public interface SdelrecordService extends CrudService<SdelrecordEntity, SdelrecordDTO> {

}