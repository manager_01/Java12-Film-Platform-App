package io.renren.modules.sdelrecord.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.renren.common.service.impl.CrudServiceImpl;
import io.renren.modules.sdelrecord.dao.SdelrecordDao;
import io.renren.modules.sdelrecord.dto.SdelrecordDTO;
import io.renren.modules.sdelrecord.entity.SdelrecordEntity;
import io.renren.modules.sdelrecord.service.SdelrecordService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * 
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2022-11-15
 */
@Service
public class SdelrecordServiceImpl extends CrudServiceImpl<SdelrecordDao, SdelrecordEntity, SdelrecordDTO> implements SdelrecordService {

    @Override
    public QueryWrapper<SdelrecordEntity> getWrapper(Map<String, Object> params){
        String id = (String)params.get("id");

        QueryWrapper<SdelrecordEntity> wrapper = new QueryWrapper<>();
        wrapper.eq(StringUtils.isNotBlank(id), "id", id);

        return wrapper;
    }


}