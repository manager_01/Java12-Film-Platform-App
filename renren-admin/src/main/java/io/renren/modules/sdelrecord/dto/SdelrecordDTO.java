package io.renren.modules.sdelrecord.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;


/**
 * 
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2022-11-15
 */
@Data
@ApiModel(value = "")
public class SdelrecordDTO implements Serializable {
    private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "")
	private Integer deid;

	@ApiModelProperty(value = "")
	private Integer type;

	@ApiModelProperty(value = "")
	private Integer keyid;

	@ApiModelProperty(value = "")
	private String reason;

	@ApiModelProperty(value = "")
	private Date time;

	@ApiModelProperty(value = "")
	private String descr;


}