package io.renren.modules.sdelrecord.dao;

import io.renren.common.dao.BaseDao;
import io.renren.modules.sdelrecord.entity.SdelrecordEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2022-11-15
 */
@Mapper
public interface SdelrecordDao extends BaseDao<SdelrecordEntity> {
	
}