package io.renren.modules.sdelrecord.excel;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;

import java.util.Date;

/**
 * 
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2022-11-15
 */
@Data
public class SdelrecordExcel {
    @Excel(name = "")
    private Integer deid;
    @Excel(name = "")
    private Integer type;
    @Excel(name = "")
    private Integer keyid;
    @Excel(name = "")
    private String reason;
    @Excel(name = "")
    private Date time;
    @Excel(name = "")
    private String descr;

}