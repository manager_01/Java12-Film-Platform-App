package io.renren.modules.newcomment.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;


/**
 * 
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2022-11-17
 */
@Data
@ApiModel(value = "")
public class NewcommentsDTO implements Serializable {
    private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "")
	private Integer cid;

	@ApiModelProperty(value = "")
	private Integer fid;

	@ApiModelProperty(value = "")
	private Integer uid;

	@ApiModelProperty(value = "")
	private String content;

	@ApiModelProperty(value = "")
	private Date createtime;

	@ApiModelProperty(value = "")
	private Integer favoritecomment;

	@ApiModelProperty(value = "")
	private Integer numberlike;

	@ApiModelProperty(value = "")
	private String uname;


}