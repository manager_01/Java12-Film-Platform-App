package io.renren.modules.newcomment.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.util.Date;

/**
 * 
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2022-11-17
 */
@Data
@TableName("newcomments")
public class NewcommentsEntity {

    /**
     * 
     */
    @TableId(type = IdType.AUTO)
	private Integer cid;
    /**
     * 
     */
	private Integer fid;
    /**
     * 
     */
	private Integer uid;
    /**
     * 
     */
	private String content;
    /**
     * 
     */
    @TableField(fill = FieldFill.INSERT)
	private Date createtime;
    /**
     * 
     */
	private Integer favoritecomment;
    /**
     * 
     */
	private Integer numberlike;
}