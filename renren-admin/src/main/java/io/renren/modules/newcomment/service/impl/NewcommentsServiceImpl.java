package io.renren.modules.newcomment.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.renren.common.page.PageData;
import io.renren.common.service.impl.CrudServiceImpl;
import io.renren.common.utils.ConvertUtils;
import io.renren.modules.newcomment.dao.NewcommentsDao;
import io.renren.modules.newcomment.dto.NewcommentsDTO;
import io.renren.modules.newcomment.entity.NewcommentsEntity;
import io.renren.modules.newcomment.service.NewcommentsService;
import io.renren.modules.newfilm.dto.NewfilmDTO;
import io.renren.modules.newfilm.entity.NewfilmEntity;
import io.renren.modules.newuser.dto.NewuserDTO;
import io.renren.modules.newuser.service.NewuserService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2022-11-17
 */
@Service
public class NewcommentsServiceImpl extends CrudServiceImpl<NewcommentsDao, NewcommentsEntity, NewcommentsDTO> implements NewcommentsService {


    @Autowired
    private NewuserService newuserService;


    @Override
    public QueryWrapper<NewcommentsEntity> getWrapper(Map<String, Object> params) {
        String id = (String) params.get("id");

        QueryWrapper<NewcommentsEntity> wrapper = new QueryWrapper<>();
        wrapper.eq(StringUtils.isNotBlank(id), "id", id);

        return wrapper;
    }


    @Override
    public PageData<NewcommentsDTO> pageInfo(Integer pageNo, Integer pageSize, Integer filmid) {


        Page<NewcommentsEntity> page = new Page<>(pageNo, pageSize);

        Page<NewcommentsDTO> page1 = new Page<>();


        LambdaQueryWrapper<NewcommentsEntity> lqw = new LambdaQueryWrapper<>();
        lqw.eq(NewcommentsEntity::getFid, filmid);
        baseDao.selectPage(page, lqw);


        BeanUtils.copyProperties(page, page1, "records");

        List<NewcommentsDTO> records1 = page1.getRecords();

        List<NewcommentsEntity> records = page.getRecords();

        records1 = records.stream().map((item) -> {
            NewcommentsDTO newcommentsDTO = new NewcommentsDTO();
            BeanUtils.copyProperties(item, newcommentsDTO);

            Integer uid = item.getUid();

            NewuserDTO newuserDTO = newuserService.get(Long.valueOf(uid));

            String uname = newuserDTO.getUname();

            newcommentsDTO.setUname(uname);

            return newcommentsDTO;
        }).collect(Collectors.toList());

        page1.setRecords(records1);

        PageData<NewcommentsDTO> newcommentsDTOPageData = new PageData<>(records1, page1.getTotal());

        return newcommentsDTOPageData;

    }
}