package io.renren.modules.newcomment.excel;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;

import java.util.Date;

/**
 * 
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2022-11-17
 */
@Data
public class NewcommentsExcel {
    @Excel(name = "")
    private Integer cid;
    @Excel(name = "")
    private Integer fid;
    @Excel(name = "")
    private Integer uid;
    @Excel(name = "")
    private String content;
    @Excel(name = "")
    private Date createtime;
    @Excel(name = "")
    private Integer favoritecomment;
    @Excel(name = "")
    private Integer numberlike;

}