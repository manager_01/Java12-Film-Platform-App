package io.renren.modules.newcomment.service;

import io.renren.common.page.PageData;
import io.renren.common.service.CrudService;
import io.renren.modules.newcomment.dto.NewcommentsDTO;
import io.renren.modules.newcomment.entity.NewcommentsEntity;

/**
 * 
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2022-11-17
 */
public interface NewcommentsService extends CrudService<NewcommentsEntity, NewcommentsDTO> {

    PageData<NewcommentsDTO> pageInfo(Integer pageNo, Integer pageSize,Integer filmid);
}