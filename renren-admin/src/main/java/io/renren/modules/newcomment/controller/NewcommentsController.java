package io.renren.modules.newcomment.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import io.renren.common.annotation.LogOperation;
import io.renren.common.constant.Constant;
import io.renren.common.page.PageData;
import io.renren.common.utils.ExcelUtils;
import io.renren.common.utils.Result;
import io.renren.common.validator.AssertUtils;
import io.renren.common.validator.ValidatorUtils;
import io.renren.common.validator.group.AddGroup;
import io.renren.common.validator.group.DefaultGroup;
import io.renren.common.validator.group.UpdateGroup;
import io.renren.modules.newcomment.dto.NewcommentsDTO;
import io.renren.modules.newcomment.entity.NewcommentsEntity;
import io.renren.modules.newcomment.excel.NewcommentsExcel;
import io.renren.modules.newcomment.service.NewcommentsService;
import io.renren.modules.newfilm.dto.NewfilmDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;


/**
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2022-11-17
 */
@RestController
@RequestMapping("newcomment")
@Api(tags = "")
@Slf4j
public class NewcommentsController {
    @Autowired
    private NewcommentsService newcommentsService;


    @GetMapping("pageInfo")
    public Result<PageData<NewcommentsDTO>> pageInfo(Integer pageNo, Integer pageSize, Integer filmid) {


        PageData<NewcommentsDTO> pageInfo = newcommentsService.pageInfo(pageNo, pageSize, filmid);

        return new Result<PageData<NewcommentsDTO>>().ok(pageInfo);


    }


    @PutMapping("numberlike")
    @Transactional(rollbackFor = Exception.class)
    public Result<String> numberlike(@RequestBody NewcommentsEntity newcommentsEntity) {

//        log.info("{}",newcommentsEntity);

        Integer numberlike = 0;

        numberlike = newcommentsEntity.getNumberlike() + 1;

        newcommentsEntity.setNumberlike(numberlike);


        LambdaQueryWrapper<NewcommentsEntity> lqw=new LambdaQueryWrapper<>();
        lqw.eq(NewcommentsEntity::getCid,newcommentsEntity.getCid());



        boolean update = newcommentsService.update(newcommentsEntity, lqw);

        if(update==true){
            return new Result();
        }else {
            return new Result().error();
        }

//        log.info("{}", update);

    }


    @GetMapping("page")
    @ApiOperation("分页")
    @ApiImplicitParams({
            @ApiImplicitParam(name = Constant.PAGE, value = "当前页码，从1开始", paramType = "query", required = true, dataType = "int"),
            @ApiImplicitParam(name = Constant.LIMIT, value = "每页显示记录数", paramType = "query", required = true, dataType = "int"),
            @ApiImplicitParam(name = Constant.ORDER_FIELD, value = "排序字段", paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = Constant.ORDER, value = "排序方式，可选值(asc、desc)", paramType = "query", dataType = "String")
    })
    @RequiresPermissions("newcomment:newcomments:page")
    public Result<PageData<NewcommentsDTO>> page(@ApiIgnore @RequestParam Map<String, Object> params) {
        PageData<NewcommentsDTO> page = newcommentsService.page(params);

        return new Result<PageData<NewcommentsDTO>>().ok(page);
    }

    @GetMapping("{id}")
    @ApiOperation("信息")
    @RequiresPermissions("newcomment:newcomments:info")
    public Result<NewcommentsDTO> get(@PathVariable("id") Long id) {
        NewcommentsDTO data = newcommentsService.get(id);

        return new Result<NewcommentsDTO>().ok(data);
    }

    @PostMapping
    @ApiOperation("保存")
    @LogOperation("保存")
    @RequiresPermissions("newcomment:newcomments:save")
    public Result save(@RequestBody NewcommentsDTO dto) {
        //效验数据
        ValidatorUtils.validateEntity(dto, AddGroup.class, DefaultGroup.class);

        newcommentsService.save(dto);

        return new Result();
    }

    @PutMapping
    @ApiOperation("修改")
    @LogOperation("修改")
    @RequiresPermissions("newcomment:newcomments:update")
    public Result update(@RequestBody NewcommentsDTO dto) {
        //效验数据
        ValidatorUtils.validateEntity(dto, UpdateGroup.class, DefaultGroup.class);

        newcommentsService.update(dto);

        return new Result();
    }

    @DeleteMapping
    @ApiOperation("删除")
    @LogOperation("删除")
    @RequiresPermissions("newcomment:newcomments:delete")
    public Result delete(@RequestBody Long[] ids) {
        //效验数据
        AssertUtils.isArrayEmpty(ids, "id");

        newcommentsService.delete(ids);

        return new Result();
    }

    @GetMapping("export")
    @ApiOperation("导出")
    @LogOperation("导出")
    @RequiresPermissions("newcomment:newcomments:export")
    public void export(@ApiIgnore @RequestParam Map<String, Object> params, HttpServletResponse response) throws Exception {
        List<NewcommentsDTO> list = newcommentsService.list(params);

        ExcelUtils.exportExcelToTarget(response, null, list, NewcommentsExcel.class);
    }

}