package io.renren.modules.newcomment.dao;

import io.renren.common.dao.BaseDao;
import io.renren.modules.newcomment.entity.NewcommentsEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2022-11-17
 */
@Mapper
public interface NewcommentsDao extends BaseDao<NewcommentsEntity> {
	
}