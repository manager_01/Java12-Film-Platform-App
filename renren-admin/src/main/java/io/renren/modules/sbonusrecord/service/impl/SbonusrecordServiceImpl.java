package io.renren.modules.sbonusrecord.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.renren.common.service.impl.CrudServiceImpl;
import io.renren.modules.sbonusrecord.dao.SbonusrecordDao;
import io.renren.modules.sbonusrecord.dto.SbonusrecordDTO;
import io.renren.modules.sbonusrecord.entity.SbonusrecordEntity;
import io.renren.modules.sbonusrecord.service.SbonusrecordService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * 
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2022-11-15
 */
@Service
public class SbonusrecordServiceImpl extends CrudServiceImpl<SbonusrecordDao, SbonusrecordEntity, SbonusrecordDTO> implements SbonusrecordService {

    @Override
    public QueryWrapper<SbonusrecordEntity> getWrapper(Map<String, Object> params){
        String id = (String)params.get("id");

        QueryWrapper<SbonusrecordEntity> wrapper = new QueryWrapper<>();
        wrapper.eq(StringUtils.isNotBlank(id), "id", id);

        return wrapper;
    }


}