package io.renren.modules.sbonusrecord.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;


/**
 * 
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2022-11-15
 */
@Data
@ApiModel(value = "")
public class SbonusrecordDTO implements Serializable {
    private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "")
	private Integer brid;

	@ApiModelProperty(value = "")
	private Integer uid;

	@ApiModelProperty(value = "")
	private Integer sourceid;

	@ApiModelProperty(value = "")
	private Integer type;

	@ApiModelProperty(value = "")
	private Integer state;

	@ApiModelProperty(value = "")
	private Integer bonus;

	@ApiModelProperty(value = "")
	private String descr;

	@ApiModelProperty(value = "")
	private Date time;


}