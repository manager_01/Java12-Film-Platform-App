package io.renren.modules.sbonusrecord.excel;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;

import java.util.Date;

/**
 * 
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2022-11-15
 */
@Data
public class SbonusrecordExcel {
    @Excel(name = "")
    private Integer brid;
    @Excel(name = "")
    private Integer uid;
    @Excel(name = "")
    private Integer sourceid;
    @Excel(name = "")
    private Integer type;
    @Excel(name = "")
    private Integer state;
    @Excel(name = "")
    private Integer bonus;
    @Excel(name = "")
    private String descr;
    @Excel(name = "")
    private Date time;

}