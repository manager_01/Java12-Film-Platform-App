package io.renren.modules.sbonusrecord.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

/**
 * 
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2022-11-15
 */
@Data
@TableName("sbonusrecord")
public class SbonusrecordEntity {

    /**
     * 
     */
    @TableId(type = IdType.AUTO)
	private Integer brid;
    /**
     * 
     */
	private Integer uid;
    /**
     * 
     */
	private Integer sourceid;
    /**
     * 
     */
	private Integer type;
    /**
     * 
     */
	private Integer state;
    /**
     * 
     */
	private Integer bonus;
    /**
     * 
     */
	private String descr;
    /**
     * 
     */
	private Date time;
}