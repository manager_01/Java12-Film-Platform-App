package io.renren.modules.sbonusrecord.dao;

import io.renren.common.dao.BaseDao;
import io.renren.modules.sbonusrecord.entity.SbonusrecordEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2022-11-15
 */
@Mapper
public interface SbonusrecordDao extends BaseDao<SbonusrecordEntity> {
	
}