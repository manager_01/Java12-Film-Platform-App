package io.renren.modules.sbonusrecord.service;

import io.renren.common.service.CrudService;
import io.renren.modules.sbonusrecord.dto.SbonusrecordDTO;
import io.renren.modules.sbonusrecord.entity.SbonusrecordEntity;

/**
 * 
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2022-11-15
 */
public interface SbonusrecordService extends CrudService<SbonusrecordEntity, SbonusrecordDTO> {

}