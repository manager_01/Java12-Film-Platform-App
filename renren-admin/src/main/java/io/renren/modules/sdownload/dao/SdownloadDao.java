package io.renren.modules.sdownload.dao;

import io.renren.common.dao.BaseDao;
import io.renren.modules.sdownload.entity.SdownloadEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 *
 * @author ${author} 1072692036@qq.com
 * @since 1.0.0 2022-11-15
 */
@Mapper
public interface SdownloadDao extends BaseDao<SdownloadEntity> {
	
}