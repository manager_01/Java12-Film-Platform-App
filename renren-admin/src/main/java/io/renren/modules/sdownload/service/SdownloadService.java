package io.renren.modules.sdownload.service;

import io.renren.common.service.CrudService;
import io.renren.modules.sdownload.dto.SdownloadDTO;
import io.renren.modules.sdownload.entity.SdownloadEntity;

/**
 * 
 *
 * @author ${author} 1072692036@qq.com
 * @since 1.0.0 2022-11-15
 */
public interface SdownloadService extends CrudService<SdownloadEntity, SdownloadDTO> {

}