package io.renren.modules.sdownload.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

/**
 * 
 *
 * @author ${author} 1072692036@qq.com
 * @since 1.0.0 2022-11-15
 */
@Data
@TableName("sdownload")
public class SdownloadEntity {

    /**
     * 
     */
    @TableId(type = IdType.AUTO)
	private Integer dlid;
    /**
     * 
     */
	private Integer uid;
    /**
     * 
     */
	private String filename;
    /**
     * 
     */
	private Date time;
    /**
     * 
     */
	private Integer bonus;
    /**
     * 
     */
	private Integer type;
    /**
     * 
     */
	private String descr;
    /**
     * 
     */
	private Integer dstate;
    /**
     * 
     */
	private String url;
    /**
     * 
     */
	private Integer state;
    /**
     * 
     */
	private String reason;
    /**
     * 
     */
	private Integer dnum;
    /**
     * 
     */
	private Integer deltag;
}